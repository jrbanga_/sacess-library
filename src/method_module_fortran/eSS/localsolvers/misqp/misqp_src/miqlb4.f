C************************************************************************
C
C   FILE NAME:  MIQLB4.FOR
C
C************************************************************************
C
C
C
C  M I X E D   I N T E G E R   Q U A D R A T I C   P R O G R A M M I N G 
C
C                  B Y   B R A N C H   A N D   B O U N D
C
C
C
C
C   Problem:
C   -------
C
C   The code solves the strictly convex mixed integer quadratic program
C
C      minimize 1/2 x^ C x + c^x
C
C      subject to    a_j^x + b_j  =  0  ,  j=1,...,m_e
C
C                    a_j^x + b_j  >= 0  ,  j=m+1,...,m
C
C                    x_l <= x <= x_u
C
C
C   with an n by n positive matrix C, an n-dimensional vector c, an m by n 
C   matrix A=(a_1,...,a_m)^, and an m-vector b. It is assumed that 
C   x = (x_1,...,x_n) consists of n_i integer and n - n_i continuous 
C   variables.
C
C
C
C   Method:
C   ------
C
C   The mixed integer problem is solved by a branch and bound algorithm. 
C   Proceeding from the solution of the relaxed, i.e., continuous problem, 
C   an integer variable is selected from where two different subproblems 
C   are generated. They are obtained by rounding the continuous value of 
C   the integer variable, say x, to get two separate subproblems, one with 
C   upper bound floor(x), another one with lower bound floor(x)+1. Each 
C   subproblem determines a node in binary search tree, which is internally  
C   created step by step. Several alternatives are implemented with different 
C   branching rules and variable selection strategies. 
C
C
C
C   Example: 
C   -------
C
C            minimize 1/2(x_1^2 + x_2^2 + x_3^2 + x_4^2 + x_5^2)  
C                         - 21.98 x_1 - 1.26 x_2 + 61.39 x_3 + 5.3 x_4 + 101.3 x_5
C
C            subject to    -7.56 x_1 + 0.5 x_5 + 39.1  >=  0 
C                          -100 <= x_1 <= 6   , -150 <= x_2 <= 10,  -100 <= x_3 <= 100,
C                          -100 <= x_4 <= 100 , -100 <= x_5 <= 100
C                           x_1,x_2 real, x_3,x_4,x_5 integer
C
C
C   Usage:
C   -----
C      SUBROUTINE MIQLB4(M,ME,MMAX,N,NMAX,MNN,NI,IND,C,D,A,B,XL,XU,X,U,
C     /                F,ACC,EPS,IOPTNS,IOUT,IFAIL,IPRINT,
C     /                RW,LRW,IW,LIW,LW,LLW)
C
C
C
C   Definition of the parameters:
C
C   M :           Number of constraints.
C   ME :          Number of equality constraints.
C   MMAX :        Row dimension of array A containing linear constraints.
C                 MMAX must be at least one and greater or equal to M.
C   N :           Number of optimization variables.
C   NMAX :        Row dimension of C. NMAX must be at least one and greater
C                 or equal to N.
C   MNN :         Must be equal to M+N+N when calling MIQL, dimension of U.
C   NI :          Number of integer variables.
C   IND(NMAX) :   Integer array identifying integer variables. 
C   C(NMAX,N):    Objective function matrix which should be symmetric 
C                 and positive definite. 
C   D(N) :        Constant vector of the quadratic objective function.
C   A(MMAX,N):    Matrix of the linear constraints, first ME rows for
C                 equality, then M-ME rows for inequality constraints.
C   B(M) :        Constant values of linear constraints in the same order.
C   XL(N),XU(N) : On input, the one-dimensional arrays XL and XU must
C                 contain the upper and lower bounds of the variables.
C   X(N) :        On return, X contains the optimal solution.
C   U(MNN) :      On return, U contains the multipliers subject to the 
C                 linear constraints and bounds. The first M locations 
C                 contain the multipliers of the M linear constraints, the 
C                 subsequent N locations the multipliers of the lower 
C                 bounds, and the final N locations the multipliers of the
C                 upper bounds. At the optimal solution, all multipliers 
C                 with respect to inequality constraints should be 
C                 nonnegative.
C   F :           On successful return (IFAIL=0/1), F contains the optimal
C                 objective function value.
C   ACC :         Accuracy to identify integer values for integer variables.
C                 If ACC is less than machine precision, e.g., ACC=0, then ACC 
C                 is set to machine precision.
C   EPS :         Termination accuracy for solving relaxed quadratic programs by 
C                 subroutine QL (e.g. 1.0D-12). The value should not be smaller 
C                 than the underlying machine precision.
C                 If EPS is less than machine precision, e.g., EPS=0, then EPS 
C                 is set to machine precision.
C   IOPTNS(20):   Integer Option Array
C   IOPTNS(1):    BR, i.e. Branching rule:
C                 1 : Maximal fractional branching, selection strategy 'best of all'.
C                 2 : Minimal fractional branching, selection strategy 'best of all'.
C                 3 : User defined Priorities
C   IOPTNS(2):    SK, i.e. Node Selection stragegy:
C                 1 : Best of all
C                 2 : Best of two
C                 3 : Depth first
C   IOPTNS(3):    MAXNODE, i.e Maximal number of subproblems or tree nodes, respectively, 
C                 that can be generated.
C   IOPTNS(4):    SUCQP, Maximal number of successive QPs solved using warmstarts, big numbers
C                 may cause numerical problems, especially for Lagrange Multipliers.
C   IOPTNS(5):    BBNDS, Indicates wether improved bounds according to Leyffer should be used(=1),
C                 only possible for SK=1.
C   IOPTNS(6):    LAGDIR, Indicates wether branching direction should be determined by value
C                 of the Lagrange Function, see Westerlund, only possible for SK=3.  
C   IOUT :        Integer indicating the desired output unit number, i.e., all
C                 write-statements start with 'WRITE(IOUT,... '.
C   IFAIL :       Termination reason, i.e. 
C                 -3 : The algorithm is unable to calculate an index of the branching
C                      variable. 
C                 -2 : A feasible solution could not be computed by MAXNODE subproblems.
C                 -1 : A feasible solution does not exist.
C                  0 : The optimal solution is found.
C                  1 : A feasible solution is found, but tree search is terminted 
C                      because of reaching MAXNODE nodes.
C                  2 : Index in IND out of bounds.
C                  3 : MNN too small.
C                  4 : Length of working array RW, IW, or LW too short.
C                  5 : One of the parameters N, M, ME, NI, or MNN is incorrectly set.
C                  6 : Parameter IBR is incorrectly set.
C                  7 : MAXNODE is incorrectly set.
C                  8 : IOUT or IPRINT are incorrectly set.
C                  9 : Lower variable bound (XL) greater than upper variable bound (XU).
C   IPRINT :       Output flag, i.e, 
C                  0 : No output to be generated.
C                  1 : Final performance summary.
C                  2 : Total output from all generated subproblems. 
C                  3 : Final output and output from subroutine QL.
C   RW(LRW) :     Real working array of length LRW. 
C   LRW :         Length of RW, must be at least 5*NMAX*NMAX/2+7*NI+15*NMAX+4*MMAX+3*MAXNODE+24.
C   IW(LIW) :     Integer array of length LIW.
C   LIW :         Length of IW, must be at least 4*NMAX+NI+6*MAXNODE+9.
C   LW(LLW) :     Logical working array of length LLW. 
C   LLW :         Length of LW, must be at least 3*NI+2.
C   PARAM(50):	  Double Precision parameter array of fixed length
C   IPARAM(50):	  Integer parameter array of fixed length
C
C
C
C   Linking:
C   -------
C
C   The compiled Fortran file MIQLB4.FOR must be linked to the main program of the 
C   user, the solver for the corresponding relaxed continuous quadratic programs (QL),
C   a solver for performing a Cholesky decomposition (CHOLKY), and the 
C   branch-and-bound solver BFOUR.
C
C
C
C   References:  T. Spickenreuther, K. Schittkowski: MIQL: A Fortran Code for 
C   ---------    Mixed Integer Convex Quadratic Programming Based on Branch-and-bound,
C                User's Guide, Report, 
C                Department of Computer Science, University of Bayreuth, 
C                D-95440 Bayreuth, 2005
C
C                K. Schittkowski: QL: A Fortran Code for Convex Quadratic 
C                Programming - User's Guide, Report, Department of Computer 
C                Science, University of Bayreuth, D-95440 Bayreuth, 2003
C
C
C   Authors:     K. Schittkowski
C   -------      Department of Computer Science
C                University of Bayreuth
C                D-95440 Bayreuth
C                Germany
C
C
C                T. Lehmann
C                Department of Computer Science
C                University of Bayreuth
C                D-95440 Bayreuth
C                Germany            
C
C
C   Version:     1.0 (July, 2005) - first implementation
C                2.0 (June, 2007) - Integration of Branch and Bound algorithm
C                                   and quadratic solver                  
C
C***********************************************************************
C
      SUBROUTINE MIQLB4(M,ME,MMAX,N,NMAX,MNN,NI,IND,C,D,A,B,XL,XU,X,U,
     /                F,ACC,EPS,IOPTNS,IOUT,IFAIL,IPRINT,
     /                RW,LRW,IW,LIW,LW,LLW)
      IMPLICIT NONE
      INTEGER  M,ME,MMAX,N,NMAX,MNN,QLMODE,NI,IXL,IXU,
     /         IBR,ISK,MAXNODE,IOUT,IPRINT,IFAIL,NODES,
     /         HEIGHT,IOUTL,LIW,LRW,LLW,B4LRW,B4LIW,B4LLW,QLLRW,QLLIW
      INTEGER  IND(NI),IW(LIW),IOPTNS(20)
      INTEGER  I,J,IPRS,IIW,IQIW,IRW,IQRW,IY,IYL,IXF,IYU,IXB,IUB,IWC,
     /         IRWLST,IIWLST,IIWADD,IB,IRWADD,SUCQP,BBNDS,LAGDIR
      DOUBLE PRECISION A(MMAX,NMAX),C(NMAX,NMAX),B(MMAX),D(NMAX),FQP,
     /         EPS,EPSM,ACC,F,U(MNN),XL(NMAX),XU(NMAX),RW(LRW),X(N)
      LOGICAL  LW(LLW)
c     / ,ISNAN
      EXTERNAL MIQLT1
c      INTRINSIC ISNAN
      IFAIL = 0
C++++++++CHECK FOR NAN       
c      DO J=1,M
c         DO I=1,N
c            IF(J .EQ. 1)THEN
c                IF(ISNAN(D(I)))THEN
c                    IFAIL=-1
c                    WRITE(IOUT,*)'GRADIENT OF OBJECTIVE CONTAINS NAN' 
c                    RETURN
c                ENDIF
c            ENDIF
c         
c            IF(ISNAN(A(J,I)))THEN
c                IFAIL=-1
c                WRITE(IOUT,*)'GRADIENTS OF CONSTRAINT ',J,' CONTAIN NAN'
c                RETURN
c            ENDIF    
c        ENDDO
c        IF(ISNAN(B(J)))THEN
c            IFAIL=-1
c            WRITE(IOUT,*)'VALUE OF CONSTRAINT ',J,' IS NAN'
c            RETURN
c        ENDIF
c      ENDDO
c      IF(ISNAN(F))THEN
c        IFAIL=-1
c        WRITE(IOUT,*)'OBJECTIVE HAS VALUE NAN'
c        RETURN
c      ENDIF      

C++++++++ASSIGNMENT OF INTEGER OPTIONS TO VARIABLES
	IBR     = IOPTNS(1)
	ISK     = IOPTNS(2)
	MAXNODE = IOPTNS(3)
        SUCQP   = IOPTNS(4)
        BBNDS   = IOPTNS(5)
        LAGDIR  = IOPTNS(6)
        
C        WRITE(*,*)IBR,ISK,MAXNODE,SUCQP,BBNDS,LAGDIR
        
C      WRITE(IOUT,*)'MAXNODE BEGINN MIQLT',MAXNODE


      IF ((M.LT.0).OR.(ME.GT.M).OR.(M.GT.MMAX)) THEN 
         IFAIL = 5
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 5 (MIQLB4): Wrong input for ME,M, or MMAX!'
         RETURN
      ENDIF
      IF ((N.LT.0).OR.(N.GT.NMAX)) THEN
         IFAIL = 5
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 5 (MIQLB4): Wrong input for N or NMAX!'
         RETURN
      ENDIF
      IF ((NI.LT.0).OR.(NI.GT.NMAX).OR.(NI.GT.N)) THEN
         IFAIL = 5
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 5 (MIQLB4): Wrong input for NI!'
         RETURN
      ENDIF
      IF ((MNN.LT.1).OR.(MNN.LT.M+N+N)) THEN
         IFAIL = 5
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 5 (MIQLB4): Wrong input for MNN!'
         RETURN
      ENDIF
      IF ((IBR.LT.1).OR.(IBR.GT.2)) THEN
         IFAIL = 6
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 6 (MIQLB4): Wrong input for IOTPNS(1)!'
         RETURN
      ENDIF
      IF ((ISK.LT.1).OR.(ISK.GT.3)) THEN
         IFAIL = 6
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 6 (MIQLB4): Wrong input for IOTPNS(2)!'
         RETURN
      ENDIF
      IF ((IOUT.LT.1).OR.(IPRINT.LT.0).OR.(IPRINT.GT.3)) THEN
         IFAIL = 7
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 7 (MIQLB4): Wrong input for IOUT or IPRINT!'
         RETURN
      ENDIF
      DO I=1,NI
         IF ((IND(I).LT.1).OR.(IND(I).GT.N)) IFAIL = 9
      ENDDO
      IF (IFAIL.EQ.9) THEN
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 9 (MIQLB4): Wrong input for integer array IND!'
         RETURN
      ENDIF
      DO I=1,N
         IF (XL(I).GT.XU(I)) IFAIL = 8
      ENDDO
      IF (IFAIL.EQ.8) THEN
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 8 (MIQLB4): Lower bound greater than upper ',
     .      'bound!'
         RETURN
      ENDIF
C
C   dimensioning parameters 
C
      B4LRW  = 3*MAXNODE + 3*NI + 6
      B4LIW  = 6*MAXNODE + 7
      B4LLW  = 3*NI+2
      QLLRW  = 3*NMAX*NMAX/2 + 10*NMAX + 2*MMAX + 2    
      QLLIW  = NMAX
      IPRS   = 1
      IIWADD = IPRS +MAX0(NI,1)
      IQIW   = IIWADD + 4*NMAX
      IIW    = IQIW + QLLIW
      IIWLST = IIW + B4LIW + 1   
      IXL    = 1
      IXU    = IXL + MAX0(NI,1)
      IQRW   = IXU + MAX0(NI,1)
      IWC    = IQRW + QLLRW
      IY     = IWC + NMAX*NMAX
      IYL    = IY + MAX0(NI,1)
      IYU    = IYL + MAX0(NI,1)
      IXF    = IYU + N
      IXB    = IXF + 1
      IUB    = IXB + N
      IB     = IUB + MNN
      IRWADD = IB  + M
      IRW    = IRWADD  + NMAX
      IRWLST = IRW + B4LRW + 1
C
C   parameters for B&B solver BFOUR
C    
      QLMODE = 0
      IOUTL  = IOUT
      HEIGHT = 0
      FQP = D(N)*X(N)
	DO I = 1,NI
	   RW(IXL+I-1)=XL(IND(I))
	   RW(IXU+I-1)=XU(IND(I))
	END DO
      IF (LRW.LT.IRWLST) THEN
         IFAIL = 4
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 4 (MIQLB4): Wrong dimension of real ',
     /      'working array!',
     /      '                       Should be at least ',IRWLST
         RETURN
      ENDIF
      IF (LIW.LT.IIWLST) THEN
         IFAIL = 4
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 4 (MIQLB4): Wrong dimension of integer ',
     /      'working array!',
     /      '                       Should be at least ',IIWLST
         RETURN
      ENDIF
      IF (LLW.LE.B4LLW) THEN
         IFAIL = 4
         IF (IPRINT.GT.0) WRITE(IOUT,*) 
     /      ' *** ERROR 4 (MIQLB4): Wrong dimension of logical ',
     /      'working array!',
     /      '                       Should be at least ',B4LLW
         RETURN
      ENDIF
C      
      EPSM = 0.001D0
    1 IF (1.0D0+EPSM.GT.1.0D0) THEN
         EPSM = EPSM*0.5D0
         GOTO 1
      ENDIF    
      EPSM=EPSM/0.5D0
      IF (ACC.LE.EPSM) ACC = EPSM
      IF (EPS.LE.EPSM) EPS = EPSM
      IF (QLMODE.EQ.0) THEN
         CALL CHOLKY(N,NMAX,C,RW(IWC),ACC,IFAIL,RW(IRW),QLLRW)
      ELSE
         DO I=1,N
            DO J=1,N
               RW(IWC+NMAX*(I-1)+J-1) = C(J,I)
            ENDDO
         ENDDO      
      ENDIF
C
      IF (NI.EQ.0) THEN
         CALL QL(M,ME,MMAX,N,NMAX,MNN,RW(IWC),D,A,B,XL,XU,X,U,EPS,
     /           QLMODE,IOUT,IFAIL,IPRINT,RW(IQRW),QLLRW,
     /           IW(IQIW),QLLIW)
     
         FQP=F
         IW(1) = 0
         IW(2) = 0    
      ELSE
C delete old fqp value !!!
         RW(IQRW)=0.0D0

         DO I=1,M
            RW(IB-1+I)=-B(I)
         ENDDO

         CALL MIQLT1(M,ME,MMAX,N,NMAX,MNN,RW(IWC),D,A,RW(IB),
     /        XL,XU,X,U,F,RW(IQRW),
     /           EPS,IW(IPRS),QLMODE,IND,NI,
     /           ACC,IBR,ISK,MAXNODE,NODES,HEIGHT,IOUT,IPRINT,
     /           IFAIL,IW(IIW),B4LIW,RW(IRW),B4LRW,LW,B4LLW,
     /           IOUTL,RW(IY),RW(IYL),RW(IYU),
     /           IW(IQIW),QLLIW,RW(IQRW+1),QLLRW,
     /           RW(IXF),RW(IXB),RW(IUB),RW(IRWADD),
     /		 IW(IIWADD),SUCQP,BBNDS,LAGDIR)     
         FQP=RW(IQRW)
	 IW(1) = NODES
         IW(2) = HEIGHT    
      ENDIF
C       
	DO I = 1,NI
	   XL(IND(I))=RW(IXL+I-1)
	   XU(IND(I))=RW(IXU+I-1)
	END DO
	RW(1)=FQP

c      WRITE(IOUT,*)'LAGRANGE-MULTIES',(U(I),I=1,M+N+N)

C++++++++ASSIGNMENT OF VARIABLES TO INTEGER OPTIONS BEFORE RETURNING
	IOPTNS(1)=IBR
	IOPTNS(2)=ISK
	IOPTNS(3)=MAXNODE
        IOPTNS(4)=SUCQP
        IOPTNS(5)=BBNDS
        IOPTNS(6)=LAGDIR

C                    
      RETURN
      END
C
C
C***********************************************************************
C
      SUBROUTINE MIQLT1(M,MEQ,MMAX,N,NMAX,MNN,G,GRAD,A,B,XL,XU,X,U,F,
     /			FQP,EPS,
     /                 PRIORS,QLMODE,IM,NI,
     /                 ACC,BR,SK,MAXNDS,NODES,HEIGHT,IOUT,B4PRNT,
     /                 IFAIL,B4IW,B4LIW,B4RW,B4LRW,B4LW,B4LLW,
     /                 IOUTL,Y,YL,YU,IACT,LIWAR,W,LW,
     /                 FBEST,XBEST,UBEST,ADDRW,ADDIW,SUCQP,BBNDS,
     /                 LAGDIR)
      IMPLICIT NONE

C	Common Variables
      INTEGER	M,MMAX,N,NMAX,MEQ,MNN,IOUT,IFAIL,QLMODE

      DOUBLE PRECISION	G(NMAX,NMAX),B(MMAX),GRAD(NMAX),
     /	XL(NMAX),XU(NMAX),A(MMAX,NMAX),X(N),U(MNN),EPS


C	Variables from MIQL1
      INTEGER	NI,IM(NI),IOUTL,NODES,HEIGHT,
     /	BR,SK,MAXNDS,PRIORS(NI),B4PRNT,B4LIW,B4LRW,
     /	B4LLW,B4IW(B4LIW)
	
      DOUBLE PRECISION FBEST,FQP,F,XBEST(N),UBEST(MNN),
     /	Y(NI),YL(NI),YU(NI),DI,ACC,B4RW(B4LRW)	

      LOGICAL NEWBST,IFEAS,B4LW(B4LLW)



C	Variables from QL
      INTEGER	INW1,IN,MN,INFO,NACT,MAXIT,
     /	LIWAR,IACT(LIWAR)

      DOUBLE PRECISION	DIAG,ZERO

      LOGICAL	LQL



C	Variables form QL0002
      INTEGER	NFLAG,IWWN,MAX0,MIN0,IWZ,IWR,IWW,
     /	IWD,IWA,IFINC,KFINC,IA,ID,II,IR,IRA,IRB,NM,IZ,
     /	IZA,ITERC,ITREF,JFINC,IFLAG,IWS,IS,K1,IW,KK,
     /	IL,IU,JU,KFLAG,LFLAG,JFLAG,KDROP,NU,MFLAG,
     /	KNEXT,IX,IWX,IWY,IY,JL,LW

      DOUBLE PRECISION	CVMAX,DIAGR,FDIFF,FDIFFA,GA,GB,
     /	PARINC,PARNEW,RATIO,RES,STEP,SUM,SUMX,SUMY,SUMA,
     /	SUMB,SUMC,TEMP,TEMPA,VSMALL,XMAG,XMAGR,ONE,TWO,
     /	ONHA,VFACT,DMAX1,DSQRT,DABS,DMIN1,W(LW)

      INTRINSIC	DMAX1,DSQRT,DABS,DMIN1,MAX0,MIN0	    

      LOGICAL	LOWER



C	Additional Variables
      INTEGER NEXTND,NRDCON,
     /	      DELCON,IIACT1,IIACT2,IIACT3,QPC,LASTSV,ONACT2,
     /	      ONACT3,LASTND,FASTBR,ADDIW(4*N),BBNDS,ONLYVS,BRVAR,
     /        WDIR,LAGDIR,SUCQP,BBNDSC

      DOUBLE PRECISION ADDRW(N),GAP,MAXGAP,
     /	      FLSON,FRSON,SUML,SUMU,FLOOR,XSTORE

      LOGICAL DELETE,RESTART,FLEAS,MARKED,CONVEX

      EXTERNAL BFOUR,INTGAP,FLOOR

C	Help-Variables
      INTEGER	I,J,K

C      
C      INITIALIZATION OF VARIABLES THAT MAY BE UNINITIALIZED OTHERWISE
C

	INFO   = 0
	NFLAG  = 0
	JFLAG  = 0
	KDROP  = 0
	NU     = 0
	MFLAG  = 0
	KNEXT  = 0
	PARINC = 0.0D0
	PARNEW = 0.0D0
	RATIO  = 0.0D0
	RES    = 0.0D0
	STEP   = 0.0D0
	SUMY   = 0.0D0
	TEMP   = 0.0D0
	NRDCON = 0
	ONACT2 = 0
	ONACT3 = 0
	BBNDSC = 0
	FLSON  = 0.0D0
	J      = 0

C
C   Initialization from MIQL1
C

C++++++++PARTITIONING OF ADDITIONAL INTEGER ARRAY, NEEDED STORAGE: 4*N
	IIACT1=1
	IIACT2=IIACT1+N
	IIACT3=IIACT2+N
	DELCON=IIACT3+N
C	IFREE=DELCON+N

C++++++++INITIALISATION OF NEW VARIABLES AND OF BFOUR PARAMETERS	

C      FSTART=0.5d0*X(N)**2*G(N,N)+GRAD(N)*X(N)
      MAXGAP=0.1D0
C++++++++VARIABLE INDICATING FAST BRANCHING WHEN .EQ. 1(NO OPTIMALITY GUARANTEE SO FAR)
      FASTBR=0
C++++++++CONVEX=.TRUE. REMOVES UNEXPLORED SON OF MARKED NODE WITH BETTER BROTHER
      CONVEX=.TRUE.      
C++++++++VARIABLE INDICATING BETTER BOUNDS ACCORDING TO LEYFFER IF .EQ. 1

      IF(SK .NE. 1)BBNDS=0
C++++++++VARIABLE INDICATING DIRECTION OF DEPTH FIRST SEARCH ACCORDING LAGRANGE VALUE IF .EQ. 1, 
C++++++++E.G. LEFT IF L(X^L)<L(X^U), SEE WESTERLUND

      IF(SK .NE. 3)LAGDIR=0
C++++++++RUN CONTROL VARIABLE FOR BFOUR 
      ONLYVS=0
      QPC=0
      LASTND=0
      LASTSV=2
      DELETE = .FALSE.
      RESTART = .FALSE.
      IFAIL  = 1
      NODES   = 0
      HEIGHT  = 0
      B4IW(1) = 0
      B4IW(2) = 0
      B4IW(6) = 0
      B4RW(2) = 0.0
      B4RW(3) = 0.0


C
C     CONSTANT DATA from QL
C
      LQL=.FALSE.
      IF (QLMODE.EQ.1) LQL=.TRUE.
      ZERO=0.0D+0
      MAXIT=MAX(10000000,40*(M+N))
c      MAXIT=40*(M+N)
C      QPEPS=EPS
      INW1=1
C      INW2=INW1+MMAX
      VSMALL=EPS       

c      WRITE(IOUT,*)'VSMALL,EPS',VSMALL,EPS
C
C     PREPARE PROBLEM DATA FOR EXECUTION from QL
C
      IF (M.GT.0) THEN
         IN=INW1
         DO J=1,M
            W(IN)=-B(J)
            IN=IN+1
         ENDDO
      ENDIF   
      LW=3*NMAX*NMAX/2+10*NMAX+M
      MN=M+N
C**************************************************************************
C
C
C   THIS SUBROUTINE SOLVES THE QUADRATIC PROGRAMMING PROBLEM 
C
C       MINIMIZE      GRAD'*X  +  0.5 * X*G*X
C       SUBJECT TO    A(K)*X  =  B(K)   K=1,2,...,MEQ,
C                     A(K)*X >=  B(K)   K=MEQ+1,...,M,
C                     XL  <=  X  <=  XU
C
C   THE QUADRATIC PROGRAMMING METHOD PROCEEDS FROM AN INITIAL CHOLESKY-
C   DECOMPOSITION OF THE OBJECTIVE FUNCTION MATRIX, TO CALCULATE THE
C   UNIQUELY DETERMINED MINIMIZER OF THE UNCONSTRAINED PROBLEM. 
C   SUCCESSIVELY ALL VIOLATED CONSTRAINTS ARE ADDED TO A WORKING SET 
C   AND A MINIMIZER OF THE OBJECTIVE FUNCTION SUBJECT TO ALL CONSTRAINTS
C   IN THIS WORKING SET IS COMPUTED. IT IS POSSIBLE THAT CONSTRAINTS
C   HAVE TO LEAVE THE WORKING SET.
C
C
C   DESCRIPTION OF PARAMETERS:
C
C     N        : IS THE NUMBER OF VARIABLES.
C     M        : TOTAL NUMBER OF CONSTRAINTS.
C     MEQ      : NUMBER OF EQUALITY CONTRAINTS.
C     MMAX     : ROW DIMENSION OF A, DIMENSION OF B. MMAX MUST BE AT
C                LEAST ONE AND GREATER OR EQUAL TO M.
C     MN       : MUST BE EQUAL M + N.
C     NMAX     : ROW DIEMSION OF G. MUST BE AT LEAST N.
C     LQL      : DETERMINES INITIAL DECOMPOSITION.
C        LQL = .FALSE.  : THE UPPER TRIANGULAR PART OF THE MATRIX G
C                         CONTAINS INITIALLY THE CHOLESKY-FACTOR OF A SUITABLE
C                         DECOMPOSITION.
C        LQL = .TRUE.   : THE INITIAL CHOLESKY-FACTORISATION OF G IS TO BE
C                         PERFORMED BY THE ALGORITHM.
C     A(MMAX,NMAX) : A IS A MATRIX WHOSE COLUMNS ARE THE CONSTRAINTS NORMALS.
C     B(MMAX)  : CONTAINS THE RIGHT HAND SIDES OF THE CONSTRAINTS.
C     GRAD(N)  : CONTAINS THE OBJECTIVE FUNCTION VECTOR GRAD.
C     G(NMAX,N): CONTAINS THE SYMMETRIC OBJECTIVE FUNCTION MATRIX.
C     XL(N), XU(N): CONTAIN THE LOWER AND UPPER BOUNDS FOR X.
C     X(N)     : VECTOR OF VARIABLES.
C     NACT     : FINAL NUMBER OF ACTIVE CONSTRAINTS.
C     IACT(K) (K=1,2,...,NACT): INDICES OF THE FINAL ACTIVE CONSTRAINTS.
C     INFO     : REASON FOR THE RETURN FROM THE SUBROUTINE.
C         INFO = 0 : CALCULATION WAS TERMINATED SUCCESSFULLY.
C         INFO = 1 : MAXIMUM NUMBER OF ITERATIONS ATTAINED.
C         INFO = 2 : ACCURACY IS INSUFFICIENT TO MAINTAIN INCREASING
C                    FUNCTION VALUES.
C         INFO = 3 : INTERNAL INCONSISTENCY OF QP, DIVISION BY ZERO.
C         INFO < 0 : THE CONSTRAINT WITH INDEX ABS(INFO) AND THE CON-
C                    STRAINTS WHOSE INDICES ARE IACT(K), K=1,2,...,NACT,
C                    ARE INCONSISTENT.
C     MAXIT    : MAXIMUM NUMBER OF ITERATIONS.
C     VSMALL   : REQUIRED ACCURACY TO BE ACHIEVED (E.G. IN THE ORDER OF THE 
C                MACHINE PRECISION FOR SMALL AND WELL-CONDITIONED PROBLEMS).
C     DIAG     : ON RETURN DIAG IS EQUAL TO THE MULTIPLE OF THE UNIT MATRIX
C                THAT WAS ADDED TO G TO ACHIEVE POSITIVE DEFINITENESS.
C     W(LW)    : THE ELEMENTS OF W(.) ARE USED FOR WORKING SPACE. THE LENGTH
C                OF W MUST NOT BE LESS THAN (1.5*NMAX*NMAX + 10*NMAX + M).
C                WHEN INFO = 0 ON RETURN, THE LAGRANGE MULTIPLIERS OF THE
C                FINAL ACTIVE CONSTRAINTS ARE HELD IN W(K), K=1,2,...,NACT.
C   THE VALUES OF N, M, MEQ, MMAX, MN, AND NMAX AND THE ELEMENTS OF
C   A, B, GRAD AND G ARE NOT ALTERED.
C
C   THE FOLLOWING INTEGERS ARE USED TO PARTITION W:
C     THE FIRST N ELEMENTS OF W HOLD LAGRANGE MULTIPLIER ESTIMATES.
C     W(IWZ+I+(N-1)*J) HOLDS THE MATRIX ELEMENT Z(I,J).
C     W(IWR+I+0.5*J*(J-1)) HOLDS THE UPPER TRIANGULAR MATRIX
C       ELEMENT R(I,J). THE SUBSEQUENT N COMPONENTS OF W MAY BE
C       TREATED AS AN EXTRA COLUMN OF R(.,.).
C     W(IWW-N+I) (I=1,2,...,N) ARE USED FOR TEMPORARY STORAGE.
C     W(IWW+I) (I=1,2,...,N) ARE USED FOR TEMPORARY STORAGE.
C     W(IWD+I) (I=1,2,...,N) HOLDS G(I,I) DURING THE CALCULATION.
C     W(IWX+I) (I=1,2,...,N) HOLDS VARIABLES THAT WILL BE USED TO
C       TEST THAT THE ITERATIONS INCREASE THE OBJECTIVE FUNCTION.
C     W(IWA+K) (K=1,2,...,M) USUALLY HOLDS THE RECIPROCAL OF THE
C       LENGTH OF THE K-TH CONSTRAINT, BUT ITS SIGN INDICATES
C       WHETHER THE CONSTRAINT IS ACTIVE.
C
C   
C   AUTHOR:    K. SCHITTKOWSKI,
C              MATHEMATISCHES INSTITUT,
C              UNIVERSITAET BAYREUTH,
C              8580 BAYREUTH,
C              GERMANY, F.R.
C
C   AUTHOR OF ORIGINAL VERSION:
C              M.J.D. POWELL, DAMTP,
C              UNIVERSITY OF CAMBRIDGE, SILVER STREET
C              CAMBRIDGE,
C              ENGLAND
C
C
C   REFERENCE: M.J.D. POWELL: ZQPCVX, A FORTRAN SUBROUTINE FOR CONVEX
C              PROGRAMMING, REPORT DAMTP/1983/NA17, UNIVERSITY OF
C              CAMBRIDGE, ENGLAND, 1983.
C
C
C   VERSION :  2.0 (MARCH, 1987)
C
C
C****************************************************************************************************

C
C
C   INITIAL ADDRESSES
C
      IWZ=NMAX
      IWR=IWZ+NMAX*NMAX
      IWW=IWR+(NMAX*(NMAX+3))/2
      IWD=IWW+NMAX
      IWX=IWD+NMAX
      IWA=IWX+NMAX
C
C     SET SOME CONSTANTS.
C
      ZERO=0.D+0
      ONE=1.D+0
      TWO=2.D+0
      ONHA=1.5D+0
      VFACT=1.D+0
C
C     SET SOME PARAMETERS.
C     NUMBER LESS THAN VSMALL ARE ASSUMED TO BE NEGLIGIBLE.
C     THE MULTIPLE OF I THAT IS ADDED TO G IS AT MOST DIAGR TIMES
C       THE LEAST MULTIPLE OF I THAT GIVES POSITIVE DEFINITENESS.
C     X IS RE-INITIALISED IF ITS MAGNITUDE IS REDUCED BY THE
C       FACTOR XMAGR.
C     A CHECK IS MADE FOR AN INCREASE IN F EVERY IFINC ITERATIONS,
C       AFTER KFINC ITERATIONS ARE COMPLETED.
C
      DIAGR=TWO
      DIAG=ZERO
      XMAGR=1.0D-2
      IFINC=3
      KFINC=MAX0(10,N)
C
C     FIND THE RECIPROCALS OF THE LENGTHS OF THE CONSTRAINT NORMALS.
C     RETURN IF A CONSTRAINT IS INFEASIBLE DUE TO A ZERO NORMAL.
C
    1 NACT=0
      IF (M .LE. 0) GOTO 45
      DO 40 K=1,M
      SUM=ZERO
      DO I=1,N
         SUM=SUM+A(K,I)**2
      ENDDO
      IF (SUM .GT. ZERO) GOTO 20
      IF (B(K) .EQ. ZERO) GOTO 30
      INFO=-K
      IF (K .LE. MEQ) GOTO 730
      IF (B(K)) 30,30,730
   20 SUM=ONE/DSQRT(SUM)
   30 IA=IWA+K
      W(IA)=SUM
   40 CONTINUE 
   45 DO K=1,N
         IA=IWA+M+K
         W(IA)=ONE
      ENDDO

C
C     IF NECESSARY INCREASE THE DIAGONAL ELEMENTS OF G.
C
      IF (.NOT. LQL) GOTO 165
      DO 60 I=1,N
      ID=IWD+I
      W(ID)=G(I,I)
      DIAG=DMAX1(DIAG,VSMALL-W(ID))
      IF (I .EQ. N) GOTO 60
      II=I+1
      DO J=II,N
         GA=-DMIN1(W(ID),G(J,J))
         GB=DABS(W(ID)-G(J,J))+DABS(G(I,J))
         IF (GB .GT. ZERO) GA=GA+G(I,J)**2/GB
         DIAG=DMAX1(DIAG,GA)
      ENDDO
   60 CONTINUE
      IF (DIAG .LE. ZERO) GOTO 90
   70 DIAG=DIAGR*DIAG
      DO I=1,N
         ID=IWD+I
         G(I,I)=DIAG+W(ID)
      ENDDO
C
C     FORM THE CHOLESKY FACTORISATION OF G. THE TRANSPOSE
C     OF THE FACTOR WILL BE PLACED IN THE R-PARTITION OF W.
C
   90 IR=IWR
      DO 130 J=1,N
      IRA=IWR
      IRB=IR+1
      DO 120 I=1,J
      TEMP=G(I,J)
      IF (I .EQ. 1) GOTO 110
      DO 100 K=IRB,IR
         IRA=IRA+1
         TEMP=TEMP-W(K)*W(IRA)
  100 CONTINUE
  110 IR=IR+1
      IRA=IRA+1
      IF (I .LT. J) W(IR)=TEMP/W(IRA)
  120 CONTINUE
      IF (TEMP .LT. VSMALL) GOTO 140
      W(IR)=DSQRT(TEMP)
  130 CONTINUE
      GOTO 170
C
C     INCREASE FURTHER THE DIAGONAL ELEMENT OF G.
C
  140 W(J)=ONE
      SUMX=ONE
      K=J
  150 SUM=ZERO
      IRA=IR-1
      DO I=K,J
         SUM=SUM-W(IRA)*W(I)
         IRA=IRA+I
      ENDDO
      IR=IR-K
      K=K-1
      IF (K.LT.1) GOTO 165
      W(K)=SUM/W(IR)
      SUMX=SUMX+W(K)**2
      IF (K .GE. 2) GOTO 150
      DIAG=DIAG+VSMALL-TEMP/SUMX
      GOTO 70
C
C     STORE THE CHOLESKY FACTORISATION IN THE R-PARTITION
C     OF W.
C

  165 CONTINUE
      IR=IWR
      DO I=1,N
         DO J=1,I
            IR=IR+1
            W(IR)=G(J,I)
         ENDDO
      ENDDO   
C
C     SET Z THE INVERSE OF THE MATRIX IN R.
C
  170 NM=N-1
      DO 220 I=1,N
      IZ=IWZ+I
      IF (I .GT. 1) THEN
         DO J=2,I
            W(IZ)=ZERO
            IZ=IZ+N
         ENDDO
      ENDIF   
      IR=IWR+(I+I*I)/2
      W(IZ)=ONE/W(IR)
      IF (I .EQ. N) GOTO 220
      IZA=IZ
      DO 210 J=I,NM
      IR=IR+I
      SUM=ZERO
      DO K=IZA,IZ,N
         SUM=SUM+W(K)*W(IR)
         IR=IR+1
      ENDDO
      IZ=IZ+N
  210 W(IZ)=-SUM/W(IR)
  220 CONTINUE
C
C     SET THE INITIAL VALUES OF SOME VARIABLES.
C     ITERC COUNTS THE NUMBER OF ITERATIONS.
C     ITREF IS SET TO ONE WHEN ITERATIVE REFINEMENT IS REQUIRED.
C     JFINC INDICATES WHEN TO TEST FOR AN INCREASE IN F.
C
      ITERC=1
      ITREF=0
      JFINC=-KFINC
C
C     SET X TO ZERO AND SET THE CORRESPONDING RESIDUALS OF THE
C     KUHN-TUCKER CONDITIONS.
C
  230 IFLAG=1
      IWS=IWW-N
      DO 240 I=1,N
      X(I)=ZERO
      IW=IWW+I
      W(IW)=GRAD(I)
      IF (I .GT. NACT) GOTO 240
      W(I)=ZERO
      IS=IWS+I
      K=IACT(I)
      IF (K .LE. M) GOTO 235
      IF (K .GT. MN) GOTO 234
      K1=K-M
      W(IS)=XL(K1)
      GOTO 240
  234 K1=K-MN
      W(IS)=-XU(K1)
      GOTO 240
  235 W(IS)=B(K)
  240 CONTINUE
      XMAG=ZERO
      VFACT=1.D+0
      IF (NACT) 340,340,280
C
C     SET THE RESIDUALS OF THE KUHN-TUCKER CONDITIONS FOR GENERAL X.
C
  250 IFLAG=2
      IWS=IWW-N
      DO 260 I=1,N
      IW=IWW+I
      W(IW)=GRAD(I)
      IF (LQL) GOTO 259
      ID=IWD+I
      W(ID)=ZERO
      DO 251 J=I,N
  251 W(ID)=W(ID)+G(I,J)*X(J)
      DO 252 J=1,I
      ID=IWD+J
  252 W(IW)=W(IW)+G(J,I)*W(ID)
      GOTO 260
  259 DO 261 J=1,N
  261 W(IW)=W(IW)+G(I,J)*X(J)
  260 CONTINUE
      IF (NACT .EQ. 0) GOTO 340
      DO 270 K=1,NACT
      KK=IACT(K)
      IS=IWS+K
      IF (KK .GT. M) GOTO 265
      W(IS)=B(KK)
      DO 264 I=1,N
      IW=IWW+I
      W(IW)=W(IW)-W(K)*A(KK,I)
  264 W(IS)=W(IS)-X(I)*A(KK,I)
      GOTO 270
  265 IF (KK .GT. MN) GOTO 266
      K1=KK-M
      IW=IWW+K1
      W(IW)=W(IW)-W(K)
      W(IS)=XL(K1)-X(K1)
      GOTO 270
  266 K1=KK-MN
      IW=IWW+K1
      W(IW)=W(IW)+W(K)
      W(IS)=-XU(K1)+X(K1)
  270 CONTINUE
C
C     PRE-MULTIPLY THE VECTOR IN THE S-PARTITION OF W BY THE
C     INVERS OF R TRANSPOSE.
C
  280 IR=IWR
C      IP=IWW+1
C      IPP=IWW+N
      IL=IWS+1
      IU=IWS+NACT
      DO 310 I=IL,IU
      SUM=ZERO
      IF (I .EQ. IL) GOTO 300
      JU=I-1
      DO 290 J=IL,JU
      IR=IR+1
  290 SUM=SUM+W(IR)*W(J)
  300 IR=IR+1
  310 W(I)=(W(I)-SUM)/W(IR)
C
C     SHIFT X TO SATISFY THE ACTIVE CONSTRAINTS AND MAKE THE
C     CORRESPONDING CHANGE TO THE GRADIENT RESIDUALS.
C
      DO 330 I=1,N
      IZ=IWZ+I
      SUM=ZERO
      DO 320 J=IL,IU
      SUM=SUM+W(J)*W(IZ)
  320 IZ=IZ+N
      X(I)=X(I)+SUM
      IF (LQL) GOTO 329
      ID=IWD+I
      W(ID)=ZERO
      DO 321 J=I,N
  321 W(ID)=W(ID)+G(I,J)*SUM
      IW=IWW+I
      DO 322 J=1,I
      ID=IWD+J
  322 W(IW)=W(IW)+G(J,I)*W(ID)
      GOTO 330
  329 DO 331 J=1,N
      IW=IWW+J
  331 W(IW)=W(IW)+SUM*G(I,J)
  330 CONTINUE
C
C     FORM THE SCALAR PRODUCT OF THE CURRENT GRADIENT RESIDUALS
C     WITH EACH COLUMN OF Z.
C
  340 KFLAG=1
      GOTO 930
  350 IF (NACT .EQ. N) GOTO 380
C
C     SHIFT X SO THAT IT SATISFIES THE REMAINING KUHN-TUCKER
C     CONDITIONS.
C
      IL=IWS+NACT+1
      IZA=IWZ+NACT*N
      DO 370 I=1,N
      SUM=ZERO
      IZ=IZA+I
      DO 360 J=IL,IWW
      SUM=SUM+W(IZ)*W(J)
  360 IZ=IZ+N
  370 X(I)=X(I)-SUM
      INFO=0
      IF (NACT .EQ. 0) GOTO 410
C
C     UPDATE THE LAGRANGE MULTIPLIERS.
C
  380 LFLAG=3
      GOTO 740
  390 DO 400 K=1,NACT
      IW=IWW+K
  400 W(K)=W(K)+W(IW)
C
C     REVISE THE VALUES OF XMAG.
C     BRANCH IF ITERATIVE REFINEMENT IS REQUIRED.
C
  410 JFLAG=1
      GOTO 910
  420 IF (IFLAG .EQ. ITREF) GOTO 250
C
C     DELETE A CONSTRAINT IF A LAGRANGE MULTIPLIER OF AN
C     INEQUALITY CONSTRAINT IS NEGATIVE.
C
      KDROP=0
      GOTO 440
  430 KDROP=KDROP+1
      IF (W(KDROP) .GE. ZERO) GOTO 440
      IF (IACT(KDROP) .LE. MEQ) GOTO 440
      NU=NACT
      MFLAG=1

      GOTO 800
  440 IF (KDROP .LT. NACT) GOTO 430
C
C     SEEK THE GREATEAST NORMALISED CONSTRAINT VIOLATION, DISREGARDING
C     ANY THAT MAY BE DUE TO COMPUTER ROUNDING ERRORS.
C

C++++++++BEGINNING OF THE SOLUTION PROCESS FOR A NEW QP WHEN WARMSTART
C++++++++PROCEDURE IS USED 

  450 CVMAX=ZERO
      IF (M .LE. 0) GOTO 481
      DO 480 K=1,M
      IA=IWA+K
      IF (W(IA) .LE. ZERO) GOTO 480
      SUM=-B(K)
      DO 460 I=1,N
  460 SUM=SUM+X(I)*A(K,I)
      SUMX=-SUM*W(IA)
      IF (K .LE. MEQ) SUMX=DABS(SUMX)
      IF (SUMX .LE. CVMAX) GOTO 480
      TEMP=DABS(B(K))
      DO 470 I=1,N
  470 TEMP=TEMP+DABS(X(I)*A(K,I))
      TEMPA=TEMP+DABS(SUM)
      IF (TEMPA .LE. TEMP) GOTO 480
      TEMP=TEMP+ONHA*DABS(SUM)
      IF (TEMP .LE. TEMPA) GOTO 480
      CVMAX=SUMX
      RES=SUM
      KNEXT=K
  480 CONTINUE
  481 DO 485 K=1,N
      LOWER=.TRUE.
      IA=IWA+M+K
      IF (W(IA) .LE. ZERO) GOTO 485
      SUM=XL(K)-X(K)

      IF (SUM) 482,485,483
  482 SUM=X(K)-XU(K)
      LOWER=.FALSE.
  483 IF (SUM .LE. CVMAX) GOTO 485
      CVMAX=SUM
      RES=-SUM
      KNEXT=K+M
      IF (LOWER) GOTO 485
      KNEXT=K+MN
  485 CONTINUE

C  	WRITE(IOUT,*)'NACH 450 knext:',knext,cvmax
c	WRITE(iout,*)'NACH 450 NACT,IACT',NACT,(IACT(I),I=1,NACT)
C
C     TEST FOR CONVERGENCE
C
      INFO=0
      IF (CVMAX .LE. VSMALL) GOTO 700
C
C     RETURN IF, DUE TO ROUNDING ERRORS, THE ACTUAL CHANGE IN
C     X MAY NOT INCREASE THE OBJECTIVE FUNCTION
C
      JFINC=JFINC+1
      IF (JFINC .EQ. 0) GOTO 510
      IF (JFINC .NE. IFINC) GOTO 530
      FDIFF=ZERO
      FDIFFA=ZERO
      DO 500 I=1,N
      SUM=TWO*GRAD(I)
      SUMX=DABS(SUM)
      IF (LQL) GOTO 489
      ID=IWD+I
      W(ID)=ZERO
      DO 486 J=I,N
      IX=IWX+J
  486 W(ID)=W(ID)+G(I,J)*(W(IX)+X(J))
      DO 487 J=1,I
      ID=IWD+J
      TEMP=G(J,I)*W(ID)
      SUM=SUM+TEMP
  487 SUMX=SUMX+DABS(TEMP)
      GOTO 495
  489 DO 490 J=1,N
      IX=IWX+J
      TEMP=G(I,J)*(W(IX)+X(J))
      SUM=SUM+TEMP
  490 SUMX=SUMX+DABS(TEMP)
  495 IX=IWX+I
      FDIFF=FDIFF+SUM*(X(I)-W(IX))
  500 FDIFFA=FDIFFA+SUMX*DABS(X(I)-W(IX))
      INFO=2
      SUM=FDIFFA+FDIFF
      IF (SUM .LE. FDIFFA)THEN
	GOTO 700
      ENDIF
      TEMP=FDIFFA+ONHA*FDIFF
      IF (TEMP .LE. SUM)THEN
	GOTO 700
      ENDIF
      JFINC=0
      INFO=0
  510 DO 520 I=1,N
      IX=IWX+I
  520 W(IX)=X(I)
C
C     FORM THE SCALAR PRODUCT OF THE NEW CONSTRAINT NORMAL WITH EACH
C     COLUMN OF Z. PARNEW WILL BECOME THE LAGRANGE MULTIPLIER OF
C     THE NEW CONSTRAINT.
C
  530 ITERC=ITERC+1

      IF (ITERC.LE.MAXIT) GOTO 531
C++++++++IF MAXIT REACHED STOP ONLY IF THIS HAPPENED DURING ONE SINGLE QP      
      IF(QPC .GT. 1)THEN
            ITERC=1
            GOTO 531
      ELSE
            write(iout,*)'MAXIMUM NUMBER OF QP-ITERATIONS
     /			     REACHED, RETURN',MAXIT
            INFO=-1
            GOTO 710
      ENDIF
  531 CONTINUE
      IWS=IWR+(NACT+NACT*NACT)/2
      IF (KNEXT .GT. M) GOTO 541
      DO 540 I=1,N
      IW=IWW+I
  540 W(IW)=A(KNEXT,I)
      GOTO 549
  541 DO 542 I=1,N
      IW=IWW+I
  542 W(IW)=ZERO
      K1=KNEXT-M
      IF (K1 .GT. N) GOTO 545
      IW=IWW+K1
      W(IW)=ONE
      IZ=IWZ+K1
      DO 543 I=1,N
      IS=IWS+I
      W(IS)=W(IZ)
  543 IZ=IZ+N
      GOTO 550
  545 K1=KNEXT-MN
      IW=IWW+K1
      W(IW)=-ONE
      IZ=IWZ+K1
      DO 546 I=1,N
      IS=IWS+I
      W(IS)=-W(IZ)
  546 IZ=IZ+N
      GOTO 550
  549 KFLAG=2
      GOTO 930
  550 PARNEW=ZERO
C
C     APPLY GIVENS ROTATIONS TO MAKE THE LAST (N-NACT-2) SCALAR
C     PRODUCTS EQUAL TO ZERO.
C
      IF (NACT .EQ. N) GOTO 570
      NU=N
      NFLAG=1
      GOTO 860
C
C     BRANCH IF THERE IS NO NEED TO DELETE A CONSTRAINT.
C
  560 IS=IWS+NACT
      IF (NACT .EQ. 0) GOTO 640
      SUMA=ZERO
      SUMB=ZERO
      SUMC=ZERO
      IZ=IWZ+NACT*N
      DO 563 I=1,N
      IZ=IZ+1
      IW=IWW+I
      SUMA=SUMA+W(IW)*W(IZ)
      SUMB=SUMB+DABS(W(IW)*W(IZ))
  563 SUMC=SUMC+W(IZ)**2
      TEMP=SUMB+.1D+0*DABS(SUMA)
      TEMPA=SUMB+.2D+0*DABS(SUMA)
      IF (TEMP .LE. SUMB) GOTO 570
      IF (TEMPA .LE. TEMP) GOTO 570
      IF (SUMB .GT. VSMALL) GOTO 5
      GOTO 570
    5 SUMC=DSQRT(SUMC)
      IA=IWA+KNEXT
      IF (KNEXT .LE. M) SUMC=SUMC/W(IA)
      TEMP=SUMC+.1D+0*DABS(SUMA)
      TEMPA=SUMC+.2D+0*DABS(SUMA)
      IF (TEMP .LE. SUMC) GOTO 567
      IF (TEMPA .LE. TEMP) GOTO 567
      GOTO 640
C
C     CALCULATE THE MULTIPLIERS FOR THE NEW CONSTRAINT NORMAL
C     EXPRESSED IN TERMS OF THE ACTIVE CONSTRAINT NORMALS.
C     THEN WORK OUT WHICH CONTRAINT TO DROP.
C
  567 LFLAG=4
      GOTO 740
  570 LFLAG=1
      GOTO 740
C
C     COMPLETE THE TEST FOR LINEARLY DEPENDENT CONSTRAINTS.
C
  571 IF (KNEXT .GT. M) GOTO 574
      DO 573 I=1,N
      SUMA=A(KNEXT,I)
      SUMB=DABS(SUMA)
      IF (NACT.EQ.0) GOTO 581
      DO 572 K=1,NACT
      KK=IACT(K)
      IF (KK.LE.M) GOTO 568
      KK=KK-M
      TEMP=ZERO
      IF (KK.EQ.I) TEMP=W(IWW+KK)
      KK=KK-N
      IF (KK.EQ.I) TEMP=-W(IWW+KK)
      GOTO 569
  568 CONTINUE
      IW=IWW+K
      TEMP=W(IW)*A(KK,I)
  569 CONTINUE
      SUMA=SUMA-TEMP
  572 SUMB=SUMB+DABS(TEMP)
  581 IF (SUMA .LE. VSMALL) GOTO 573
      TEMP=SUMB+.1D+0*DABS(SUMA)
      TEMPA=SUMB+.2D+0*DABS(SUMA)
      IF (TEMP .LE. SUMB) GOTO 573
      IF (TEMPA .LE. TEMP) GOTO 573
      GOTO 630
  573 CONTINUE
      LFLAG=1
      GOTO 775
  574 K1=KNEXT-M
      IF (K1 .GT. N) K1=K1-N
      DO 578 I=1,N
      SUMA=ZERO
      IF (I .NE. K1) GOTO 575
      SUMA=ONE
      IF (KNEXT .GT. MN) SUMA=-ONE
  575 SUMB=DABS(SUMA)
      IF (NACT.EQ.0) GOTO 582
      DO 577 K=1,NACT
      KK=IACT(K)
      IF (KK .LE. M) GOTO 579
      KK=KK-M
      TEMP=ZERO
      IF (KK.EQ.I) TEMP=W(IWW+KK)
      KK=KK-N
      IF (KK.EQ.I) TEMP=-W(IWW+KK)
      GOTO 576
  579 IW=IWW+K
      TEMP=W(IW)*A(KK,I)
  576 SUMA=SUMA-TEMP
  577 SUMB=SUMB+DABS(TEMP)
  582 TEMP=SUMB+.1D+0*DABS(SUMA)
      TEMPA=SUMB+.2D+0*DABS(SUMA)
      IF (TEMP .LE. SUMB) GOTO 578
      IF (TEMPA .LE. TEMP) GOTO 578
      GOTO 630
  578 CONTINUE
      LFLAG=1
      GOTO 775
C
C     BRANCH IF THE CONTRAINTS ARE INCONSISTENT.
C
  580 INFO=-KNEXT
      IF (KDROP .EQ. 0)THEN
	GOTO 700
      ENDIF
      PARINC=RATIO
      PARNEW=PARINC
C
C     REVISE THE LAGRANGE MULTIPLIERS OF THE ACTIVE CONSTRAINTS.
C
  590 IF (NACT.EQ.0) GOTO 601
      DO 600 K=1,NACT
      IW=IWW+K
      W(K)=W(K)-PARINC*W(IW)
      IF (IACT(K) .GT. MEQ) W(K)=DMAX1(ZERO,W(K))
  600 CONTINUE

C++++++++LABEL 601 IS USED FOR WARMSTARTS WHEN ACTIVE CONSTRAINTS HAVE TO BE
C++++++++REMOVED BECAUSE OF GUARANTEEING DUAL FEASIBILITY
  601 IF (KDROP .EQ. 0) GOTO 680
C
C     DELETE THE CONSTRAINT TO BE DROPPED.
C     SHIFT THE VECTOR OF SCALAR PRODUCTS.
C     THEN, IF APPROPRIATE, MAKE ONE MORE SCALAR PRODUCT ZERO.
C

      NU=NACT+1
      MFLAG=2
      GOTO 800
  610 IWS=IWS-NACT-1
      NU=MIN0(N,NU)
      DO 620 I=1,NU
      IS=IWS+I
      J=IS+NACT
  620 W(IS)=W(J+1)

C++++++++IF THERE ARE MORE ACTIVE CONSTRAINTS TO ELIMINATE GOTO 601 OTHERWISE START
C++++++++NEW ITERATION AT LABEL 450, THE FLAG DELETE AND THE COUNTER NRDCON
C++++++++SHOW WHICH DECISION TO MAKE
	IF(DELETE .AND. NRDCON .GT.0)THEN
		KDROP=ADDIW(DELCON-1+NRDCON)
		NRDCON=NRDCON-1
		GOTO 601
	ELSEIF(DELETE .AND. NRDCON .LE. 0)THEN
C+++++++RESET FLAG DELETE IF THERE ARE NO MORE ACTIVE CONSTRAINTS DO DELETE
		DELETE=.FALSE.
		GOTO 450	
	ENDIF    

      NFLAG=2
      GOTO 860
C
C     CALCULATE THE STEP TO THE VIOLATED CONSTRAINT.
C
  630 IS=IWS+NACT
  640 SUMY=W(IS+1)
      IF (SUMY.EQ.0.0) THEN
C++++++++BEFORE RETURNING CONSIDER THIS QP AS LEAF IN BRANCH AND BOUND TREE
C++++++++AND TRY TO CONTINUE WITH ANOTHER NODE IN THE TREE
         INFO=3
	 GOTO 701
      ENDIF   
      STEP=-RES/SUMY
      PARINC=STEP/SUMY
      IF (NACT .EQ. 0) GOTO 660
C
C     CALCULATE THE CHANGES TO THE LAGRANGE MULTIPLIERS, AND REDUCE
C     THE STEP ALONG THE NEW SEARCH DIRECTION IF NECESSARY.
C
      LFLAG=2
      GOTO 740
  650 IF (KDROP .EQ. 0) GOTO 660
      TEMP=ONE-RATIO/PARINC
      IF (TEMP .LE. ZERO) KDROP=0
      IF (KDROP .EQ. 0) GOTO 660
      STEP=RATIO*SUMY
      PARINC=RATIO
      RES=TEMP*RES
C
C     UPDATE X AND THE LAGRANGE MULTIPIERS.
C     DROP A CONSTRAINT IF THE FULL STEP IS NOT TAKEN.
C
  660 IWY=IWZ+NACT*N
      DO 670 I=1,N
      IY=IWY+I
  670 X(I)=X(I)+STEP*W(IY)
      PARNEW=PARNEW+PARINC

C++++++++CALCULATION OF IMPROVED BOUNDS ACCORDING TO LEYFFER
C++++++++ONE GOLDFARB/IDNANI ITERATION DONE FOR SON
	IF(BBNDS .GT. 1)THEN
		 GOTO 702	
	ENDIF

 1002 IF (NACT .GE. 1) GOTO 590
C
C     ADD THE NEW CONSTRAINT TO THE ACTIVE SET.
C
  680 NACT=NACT+1
      W(NACT)=PARNEW
      IACT(NACT)=KNEXT
      IA=IWA+KNEXT
      IF (KNEXT .GT. MN) IA=IA-N
      W(IA)=-W(IA)
C
C     ESTIMATE THE MAGNITUDE OF X. THEN BEGIN A NEW ITERATION,
C     RE-INITILISING X IF THIS MAGNITUDE IS SMALL.
C
      JFLAG=2
      GOTO 910
  690 IF (SUM .LT. (XMAGR*XMAG)) GOTO 230
      IF (ITREF) 450,450,250


C++++++++FUNKTIONIERT SO PRINZIPIELL NICHT, FINDET U.U. NICHT DAS OPTIMUM DES
C++++++++MIQL, SIEHE BEISPIEL 1, ITERATION 2

C++++++++FASTBR = 1: EARLY BRANCHING AFTER ADDING EVERY NEW CONSTRAINT
C++++++++FASTBR = 0: NO EARLY BRANCHING
C++++++++ALSO POSSIBLE BUT NOT IMPLEMENTED:
C++++++++FASTBR = 2: EARLY BRANCHING AFTER ADDING OR ELIMINATING A CONSTRAINT
      IF(FASTBR .EQ. 1)THEN

C++++++++ITERATIVES REFINEMENT BEFORE EARLY BRANCHING
	ITREF=ITREF+1
	IF (ITREF .EQ. 1)GOTO 250	          

         DO I=1,NI
            Y(I) = X(IM(I))
            YL(I) = XL(IM(I))
            YU(I) = XU(IM(I))

C++++++++EARLY BRANCHING IS ONLY POSSIBLE IF EVERY INTEGER VARIABLE FULLFILLS
C++++++++ITS BOXCONSTRAINTS, MAYBY THIS HAS TO BE ENFORCED FIRST
	    IF((Y(I) .LT. YL(I)).OR.(Y(I) .GT. YU(I))) GOTO 450
         ENDDO

	CALL INTGAP(Y,NI,GAP)
	IF(GAP .GT. MAXGAP) THEN
	WRITE(IOUT,*)'+++++++++++GAP EARLY BRANCHING+++++++',GAP

	        GOTO 701
	ELSE
		GOTO 450
	ENDIF	

      ELSE
	GOTO 450
      ENDIF	

C
C     INITIATE ITERATIVE REFINEMENT IF IT HAS NOT YET BEEN USED,
C     OR RETURN AFTER RESTORING THE DIAGONAL ELEMENTS OF G.
C
  700 IF (ITERC .EQ. 0) GOTO 710
      ITREF=ITREF+1
      JFINC=-1
      IF (ITREF .EQ. 1) GOTO 250
      IF (LQL)THEN
         DO 720 I=1,N
            ID=IWD+I
  720       G(I,I)=W(ID)
      DIAG=ZERO
      ENDIF

C*****************************************************************
C	Integration of Branch and Bound in QL
C	before usually returning Call BFOUR
C*****************************************************************

  701 NODES = NODES + 1
      IF(INFO .GE. 10) THEN
	IFEAS = .FALSE.
      ELSEIF(INFO .EQ. 0)THEN
        IFEAS = .TRUE.

      ELSE
C++++++++AN ERROR OCCURED IN THE SOLUTION PROCESS OF THE CURRENT QP, TREAT
C++++++++THIS NODE AS LEAF IN THE BRANCH AND BOUND TREE, I.E. CONSIDER IT AS AN
C++++++++INFEASIBLE NODE AND CONTINUE WITH ANOTHER NODE.
C++++++++IF THE WARMSTART PROCEDURE IS USED THIS ERROR COULD BE CAUSED BY
C++++++++NUMERICAL ERRORS FORM THE WARMSTART. THERFORE TRY TO SOLVE IT ONCE AGAIN, RESTART IS FALSE, FORM
C++++++++SCRATCH 
	IFEAS = .FALSE.
	INFO=0
	IF (.NOT. RESTART)THEN
	   RESTART = .TRUE.
	   GOTO 1
	ENDIF
      ENDIF
      F = 0.0D0

C++++++++CALCULATE THE CURRENT OBJECTIVE VALUE
         IF (QLMODE.GT.0) THEN         
            DO I=1,N
               DI = 0.0D+0
               DO J=1,N
                  DI = DI + G(I,J)*X(J)
               ENDDO   
               F = F + (0.5D0*DI + GRAD(I))*X(I)
            ENDDO        
         ELSE
            DO I=1,N
               ADDRW(I) = 0.0D+0
               DO J=I,N
                  ADDRW(I) = ADDRW(I) + G(I,J)*X(J)
               ENDDO   
               F = F + 0.5D0*ADDRW(I)*ADDRW(I) + GRAD(I)*X(I)
            ENDDO                 
         ENDIF    
      FQP=DMIN1(FQP,F)
         DO I=1,NI
            Y(I) = X(IM(I))
            YL(I) = XL(IM(I))
            YU(I) = XU(IM(I))
         ENDDO
         
C++++++++REORDER MULTIPLIERS ACCORDING TO QL
  710 DO 715 J=1,MNN
  715	U(J)=ZERO
      IF(NACT .EQ. 0)GOTO 717
      DO 716 I=1,NACT
        J=IACT(I)
	U(J)=W(I)
  716	CONTINUE
  717 CONTINUE

C++++++++VARIABLE NEXTND SHOWS THE RELATION OF THE LAST NODE IN THE BRANCH AND
C++++++++BOUND TREE AND THE NEXT ONE. THE FOLLOWING SPECIFIC RELATIONS ARE
C++++++++DISTINGUISHED: 
C++++++++NEXTND=1: SON		NEXTND=3: NEPHEW
C++++++++NEXTND=2: BROTHER	NEXTND=0: NO SPECIAL RELATION

C++++++++LABEL FOR IMPROVED BOUND CALCULATION
  702 CONTINUE
	IF(BBNDS .EQ. 0)THEN
C++++++++NO IMPROVED BOUNDS ACCORDING TO LEYFFER
	      IF(SK .EQ. 2)THEN
C++++++++SAVE ACTIVE CONSTRAINTS FOR SK=2, FATHER & SON OR SON & SON
C++++++++USE DIFFERENT ARRAYS CORRESPONDING TO SON OR NOT SON AND LAST USED
C++++++++ARRAY(LASTSV)

		IF((NEXTND .EQ. 1 .AND. LASTSV .EQ. 2).OR.
     /		(NEXTND .EQ. 2 .AND. LASTSV .EQ. 2).OR.			
     /		(NEXTND .EQ. 3 .AND. LASTSV .EQ. 3).OR.			
     /		(NEXTND .EQ. 0 .AND. LASTSV .EQ. 3))THEN
		  ONACT3=NACT	
		  DO 721 I=1,NACT
		   ADDIW(IIACT3-1+I)=IACT(I)
  721		  CONTINUE
		  LASTSV=3	    
		ELSEIF((NEXTND .EQ. 1 .AND. LASTSV .EQ. 3).OR.
     /		(NEXTND .EQ. 2 .AND. LASTSV .EQ. 3).OR.			
     /		(NEXTND .EQ. 3 .AND. LASTSV .EQ. 2).OR.			
     /		(NEXTND .EQ. 0 .AND. LASTSV .EQ. 2)) THEN
		  ONACT2=NACT	
  		  DO 722 I=1,NACT
		     ADDIW(IIACT2-1+I)=IACT(I)
  722		  CONTINUE
		  LASTSV=2
		ENDIF
	      ENDIF

	ELSEIF(BBNDS .EQ. 3)THEN
C++++++++IMPROVED BOUNDS CALCULATION ACCORDING TO LEYFFER
C++++++++3.STEP: EXPLORE SECOND SON
      
C++++++++CALCULATE OBJECTIVE VALUE FOR RIGHT SON
	 IF(INFO .LE. 0) THEN
	    FRSON=0.0D0      
         IF (QLMODE.GT.0) THEN
C++++++++RESTRICT IMPROVED BOUND CALCULATION FOR RIGHT SON 
C++++++++ON A MAXIMAL NUMBER OF QP-ITERATIONS(LESS THAN 10, MAY BY 0),
C++++++++THEREFORE COUNTER  BBNDSC IS NEEDED
            BBNDSC=BBNDSC+1   
            DO I=1,N
               DI = 0.0D+0
               DO J=1,N
                  DI = DI + G(I,J)*X(J)
               ENDDO   
               FRSON = FRSON + (0.5D0*DI + GRAD(I))*X(I)
            ENDDO        
         ELSE
            DO I=1,N
               ADDRW(I) = 0.0D+0
               DO J=I,N
                  ADDRW(I) = ADDRW(I) + G(I,J)*X(J)
               ENDDO   
               FRSON = FRSON + 0.5D0*ADDRW(I)*ADDRW(I) + GRAD(I)*X(I)
            ENDDO                 
         ENDIF        
            
C++++++++CONTINUE QP SOLUTION PROCESS IF FRSON .LE. F BECAUSE OF ELIMINATION
C++++++++OF ACTIVE CONSTRAINT AT CALCULATION FOR LEFT SON, 
         IF((FRSON-F).LT. -EPS .AND. BBNDSC .GT. 5)THEN
            ITREF=0
            GOTO 1002
         ELSEIF(FRSON-F .LT. ZERO)THEN
            FRSON = F      
         ENDIF         
	 ELSE
C++++++++INFO NOT EQUAL ZERO(ERROR)
	      FRSON=1.0D30
	 ENDIF

     
C++++++++IMPROVED BOUND IS MINIMUM OF FLSON AND FRSON
      		F=DMIN1(FLSON,FRSON) 
            
            
            IF(DMIN1(FLSON,FRSON).EQ.FLSON)THEN
                WDIR=0
            ELSEIF(DMIN1(FLSON,FRSON).EQ.FRSON)THEN
                WDIR=1
            ELSE
                WRITE(IOUT,*)'ERROR F_IMPROVED NOT 
     /            EQUAL TO FRSON OR FLSON'
            ENDIF  
            FRSON=DMAX1(FLSON,FRSON)
C	 WRITE(IOUT,*)'F IMPROVED, F WORSE,WDIR',F,FRSON,WDIR	
C++++++++RESET BOUNDS TO CONTINUE NORMAL B&B
		XL(IM(BRVAR))=YL(BRVAR) 
		BBNDS=4		
		ONLYVS=2

	ELSEIF(BBNDS .EQ. 1)THEN
C++++++++IMPROVED BOUNDS CALCULATION ACCORDING TO LEYFFER
C++++++++1.STEP: STORE ACTIVE CONSTRAINTS IN PARTITION 2
C++++++++	 RUN BFOUR UNTIL BRANCHING VARIABLE IS SELECTED
C++++++++ NOT NECESSARY????????????????	       
		  ONACT2=NACT	
  		  DO 741 I=1,NACT
		     ADDIW(IIACT2-1+I)=IACT(I)
  741		  CONTINUE

C++++++++VARIABLE FOR BFOUR:
C++++++++ONLYVS=0: NORMAL BFOUR RUN
C++++++++ONLYVS=1: RETURN FROM BFOUR AFTER BRANCHING VARIABLE IS SELECTED
C++++++++ONLYVS=2: SCIP VARIABLE SELECTION AND GOTO NODE CREATION IN BFOUR
C++++++++          IMPROVED BOUNDS MODE, SK = 1
C++++++++ONLYVS=3: SCIP VARIABLE SELECTION AND GOTO NODE CREATION IN BFOUR
C++++++++          LAGRANGE DIRECTION MODE, SK = 3
		  ONLYVS = 1
		  

	ELSEIF(BBNDS .EQ. 2)THEN	  
C++++++++IMPROVED BOUNDS CALCULATION ACCORDING TO LEYFFER
C++++++++2.STEP: EXPLORE FIRST SON

C++++++++CALCULATE FUNCTIONVALUE FOR LEFT SON
	 IF(INFO .LE. 0) THEN	
	    FLSON=0.0D0         
         IF (QLMODE.GT.0) THEN   
            DO I=1,N
               DI = 0.0D+0
               DO J=1,N
                  DI = DI + G(I,J)*X(J)
               ENDDO   
               FLSON = FLSON + (0.5D0*DI + GRAD(I))*X(I)
            ENDDO        
         ELSE
            DO I=1,N
               ADDRW(I) = 0.0D+0
               DO J=I,N
                  ADDRW(I) = ADDRW(I) + G(I,J)*X(J)
               ENDDO   
               FLSON = FLSON + 0.5D0*ADDRW(I)*ADDRW(I) + GRAD(I)*X(I)
            ENDDO                 
         ENDIF  
         
C++++++++BE AWARE OF NUMERICS
         IF(ABS(FLSON-F).LT. EPS)FLSON=F  

	 ELSE
C++++++++INFO NOT EQUAL ZERO(ERROR)
	      FLSON=1.0D30
	 ENDIF     

C	 WRITE(IOUT,*)'FLSON',FLSON
	 
C++++++++RESET X BEFORE EXPLORING RIGHT SON
      IWY=IWZ+NACT*N
      DO 1001 I=1,N
      IY=IWY+I
 1001 X(I)=X(I)-STEP*W(IY)


C++++++++SET BOUNDS TO EXPLORE RIGHT SON
		XU(IM(BRVAR))=YU(BRVAR)
		    
		IF(Y(BRVAR).GE.0)THEN     		 			 
			XL(IM(BRVAR))=INT(Y(BRVAR))+1.0D0     
		ELSE
			XL(IM(BRVAR))=INT(Y(BRVAR))
		ENDIF
		BBNDS=3
C++++++++INITIALIZATION OF COUNTER FOR RIGHT SON CALCULATION
		BBNDSC=0		

C++++++++DELETE DIFFERENT ACTIVE CONSTRAINTS TO REGAIN DUAL FEASIBILITY
C++++++++NUMBER OF ACTIVE CONSTRAINTS TO BE DELETED
C++++++++NOT NECESSARY?
		NRDCON=0
		DO 743 I=1,NACT
		   DO 742 J=1,ONACT2
		      IF(IACT(I).EQ.ADDIW(IIACT2-1+J))GOTO 743
  742		   CONTINUE
		   DELETE=.TRUE.
		   NRDCON=NRDCON+1
		   ADDIW(DELCON-1+NRDCON)=I
  743		CONTINUE	   

C++++++++DELETE CONSTRAINTS UNTILL ALL DIFFERENT ONES ARE ELIMINATED, I.E. NRDCON=0
C++++++++POSSIBLY NONE IS DIFFERENT
		IF(NRDCON .EQ. 0)GOTO 450
		KDROP=ADDIW(DELCON-1+NRDCON)
		NRDCON=NRDCON-1
		GOTO 601
		   
	ENDIF

C++++++++ONLY VARIABLE SELECTION IN BRANCH AND BOUND IF SK=3 AND LAGDIR=1
      IF(SK .EQ. 3 .AND. LAGDIR .EQ. 1)THEN
        ONLYVS=1
      ENDIF

C++++++++DEBUG MESSAGES
c	WRITE(IOUT,*)'LAGR. VOR B4',(W(I),I=1,NACT)	      
C	WRITE(IOUT,*)'LAGR. VOR B4',(U(I),I=1,N+M+N)	
C	WRITE(iout,*)'NACT,IACT',NACT,(IACT(I),I=1,NACT)
C	WRITE(IOUT,*)'X VOR B4',(X(I),I=1,N)
      B4PRNT=0
C++++++++CALL BRANCH AND BOUND ROUTINE		
  706	CONTINUE

	CALL BFOUR(NI,Y,F,IFEAS,YL,YU,ACC,BR,SK,PRIORS,
     /              MAXNDS,IFAIL,IOUT,B4PRNT,B4RW,B4LRW,B4IW,B4LIW,
     /              B4LW,B4LLW,IOUTL,NEWBST,NEXTND,ONLYVS,BRVAR,FLEAS,
     /              MARKED,FRSON,WDIR,CONVEX)

C++++++++CALCULATION OF LAGRANGIAN VALUES FOR DETERMINING BRANCHING DIRECTION IF SK = 3
C!!!!!!!!!!!!!!!!!!!BERECHNUNG VON MINDESTENS EINER VARIANTE BZGL QLMODE IST FALSCH!!!!!!!!!!!!!!
      IF(SK .EQ. 3 .AND. LAGDIR .EQ. 1 .AND. IFAIL .EQ. 1)THEN
C++++++++CORESPONDING NODE IS INTEGRAL, NO LAGRANGE DIRECTION NEEDED      
        IF(NEWBST)THEN
            DO I=1,N
               XBEST(I) = X(I)
            ENDDO   
            DO I=1,MNN
               UBEST(I) = U(I)
            ENDDO    
            FBEST = F
	    ONLYVS=2
	    LAGDIR=2 
	    GOTO 706      
        ENDIF
        
C++++++++CORESPONDING NODE IS LEAF, NO LAGRANGE DIRECTION NEEDED      
        IF((.NOT. IFEAS).OR. FLEAS .OR. MARKED)THEN
            ONLYVS=2
            LAGDIR=2
            GOTO 706
        ENDIF  
      
         SUML=0.0D0
         SUMU=0.0D0
C         PRODUC=0.0D0   
         IF (QLMODE.GT.0) THEN 
            DO I=1,N
               IF(I.NE.IM(BRVAR))THEN
                 SUML = SUML + X(I)*G(I,IM(BRVAR))*FLOOR(X(IM(BRVAR)))
                 SUMU = SUMU + X(I)*G(I,IM(BRVAR))*(FLOOR(X(IM(BRVAR)))
     /                  +1.0D0)
               ELSE
                 SUML = SUML + GRAD(I)*FLOOR(X(I))
                 SUML = SUML + (FLOOR(X(I)))**2*G(I,I)*0.5D0
                 SUMU = SUMU + GRAD(I)*(FLOOR(X(I))+1.0D0)
                 SUMU = SUMU + (FLOOR(X(I))+1.0D0)**2*G(I,I)*0.5D0

              ENDIF         
            ENDDO        
         ELSE        
            XSTORE=X(IM(BRVAR))
            X(IM(BRVAR))=FLOOR(X(IM(BRVAR)))
            DO I=1,N
               ADDRW(I) = 0.0D+0
               DO J=I,N
                  ADDRW(I) = ADDRW(I) + G(I,J)*X(J)
               ENDDO   
               SUML = SUML + 0.5D0*ADDRW(I)*ADDRW(I) + GRAD(I)*X(I)
            ENDDO 
            X(IM(BRVAR))=X(IM(BRVAR))+1.0D0
            DO I=1,N
               ADDRW(I) = 0.0D+0
               DO J=I,N
                  ADDRW(I) = ADDRW(I) + G(I,J)*X(J)
               ENDDO   
               SUMU = SUMU + 0.5D0*ADDRW(I)*ADDRW(I) + GRAD(I)*X(I)
            ENDDO                  
            X(IM(BRVAR))=XSTORE
         ENDIF

          DO I=1,M            
            SUML=SUML-U(I)*A(I,IM(BRVAR))*FLOOR(X(IM(BRVAR)))
            SUMU=SUMU-U(I)*A(I,IM(BRVAR))*(FLOOR(X(IM(BRVAR)))+1.0D0)
          ENDDO
         IF(SUML.LT.SUMU)THEN
            WDIR=0
         ELSE
            WDIR=1
         ENDIF
         
C++++++++CONTINUE BRANCH AND BOUND
         LAGDIR=2   
         ONLYVS=3
         GOTO 706
         
      ELSEIF(SK .EQ. 3 .AND. LAGDIR .EQ. 2)THEN   
         ONLYVS = 0
         LAGDIR = 1
      ENDIF   
       
	RESTART=.FALSE.	    
C++++++++DO NOT CALCULATE IMPROVED BOUNDS IF QP WAS INTEGER FEASIBLE OR INFEASIBLE
	IF(BBNDS .EQ. 1 .AND. (IFAIL .EQ. 1) .AND.
     /	 IFEAS .AND. NEWBST)THEN
            DO I=1,N
               XBEST(I) = X(I)
            ENDDO   
            DO I=1,MNN
               UBEST(I) = U(I)
            ENDDO    
            FBEST = F
            ONLYVS=2
            BBNDS=4 
            GOTO 706
	ENDIF
C++++++++CURREND NODE IS A LEAF	 
	IF(BBNDS .EQ. 1 .AND.(IFAIL .EQ. 1).AND.
     /  	((.NOT. IFEAS).OR. FLEAS .OR. MARKED))THEN
		 ONLYVS=2
		 BBNDS=4 
		 GOTO 706
	ENDIF	 
	
C++++++++IMPROVED BOUNDS CALCULATION ACCORDING TO LEYFFER
	IF(BBNDS .EQ. 1 .AND. (IFAIL .EQ. 1)) THEN
C++++++++SET BOUNDS TO EXPLORE LEFT SON
		IF(Y(BRVAR).GE.0)THEN     		 			 
			XU(IM(BRVAR))=INT(Y(BRVAR))     
		ELSE
			XU(IM(BRVAR))=INT(Y(BRVAR))-1.0D0
		ENDIF
		BBNDS=2	
		GOTO 450

	ELSEIF(BBNDS .EQ. 4) THEN
C++++++++CONTINUE NORMAL B&B RUN
		BBNDS=1
		ONLYVS=0  
	ENDIF	  

C++++++++SAVE ACTIVE CONSTRAINTS AFTER RELAXED QP,I.E. ROOT OF BRANCH AND BOUND
	      IF(B4IW(2).EQ.1) THEN
C		ONACT1=NACT	      
		DO 719 I=1,NACT
		   ADDIW(IIACT1-1+I)=IACT(I)
  719		CONTINUE
		  ONACT3=NACT	
		  DO 718 I=1,NACT
		   ADDIW(IIACT3-1+I)=IACT(I)
  718		  CONTINUE
		  ONACT2=NACT	
  		  DO 714 I=1,NACT
		     ADDIW(IIACT2-1+I)=IACT(I)
  714		  CONTINUE


	      ENDIF



		
C++++++++BRANCH AND BOUND STRATEGIE, CHANGE NODE SELECTION STRATEGIE AFTER
C++++++++FIRST INTEGRAL SOLUTION IS FOUND
c		IF(B4LW(4*NI+1).AND.SK .EQ.3)THEN
C			SK=1
c		ENDIF
 
       IF (HEIGHT.LT.B4IW(6)) HEIGHT = B4IW(6)
C++++++++SAVE NEW BEST SOLUTION
       IF (NEWBST) THEN
            DO I=1,N
               XBEST(I) = X(I)
            ENDDO   
            DO I=1,MNN
               UBEST(I) = U(I)
            ENDDO    
            FBEST = F
       ENDIF

C++++++++EARLY RETURN IF FIRST INTEGRAL SOLUTION, THAT IS BETTER THAN
C++++++++STARTING POINT IS FOUND
c	IF(FBEST .LT.zero .and. newbst)THEN
c		 IFAIL=0
c		 GOTO 2222
c		 ENDIF
		 
C++++++++INITIALISATION FOR NEXT QP-RUN
       DO I=1,NI
            XL(IM(I)) = YL(I)
            XU(IM(I)) = YU(I)
       ENDDO
	KDROP=0
	ITREF=0

C++++++++LASTND STORES VALUE OF NEXTND FORM THE PREVIOUS ITERATION

C++++++++FOR NUMERICAL REASONS SOLVE QP COMPLETELY NEW
	QPC=QPC+1     
	IF(IFAIL .EQ. 1 .AND. QPC .EQ. SUCQP)THEN
		  QPC=0	
		  LASTND=NEXTND  
		  GOTO 1
	ENDIF 


C++++++++IF NEXTND IS SON, THEN THE NEXT B&B PROBLEM IS JUST A NEW ITERATION
C++++++++WITH ONE MORE CONSTRAINT
       IF(IFAIL .EQ. 1 .AND. NEXTND.EQ.1) THEN
		 LASTND=NEXTND
		 GOTO 450 	
       ENDIF

C++++++++WARMSTART FOR SK=2 FOR NODES THAT ARE NO SONS
C++++++++ALTHOUGH THE CONSIDERED NODES ARE NO BROTHERS THE CORRESBONDING QP ARE
C++++++++NEARLY IDENTICAL
C++++++++IDEA: ELIMINATE THE DIFFERENT ACTIVE CONSTRAINTS COMPARED TO FATHER
C++++++++OR BROTHER TO INSURE DUAL FEASIBILITY
	IF(IFAIL .EQ. 1 .AND. SK .EQ. 2 .AND.(NEXTND .EQ. 3 .OR.
     /	 NEXTND .EQ. 2).AND. LASTND .NE.0) THEN	   

C++++++++NUMBER OF ACTIVE CONSTRAINTS TO BE DELETED
	NRDCON=0

C++++++++ONLY THE STORAGE PARTITION NOT USED RECENTLY HAS TO BE CONSIDERED
C++++++++BECAUSE NEXT NODE IS NO SON
	IF(LASTSV .EQ. 2) THEN
	DO 724 I=1,NACT
	   DO 723 J=1,ONACT3
	      IF(IACT(I).EQ.ADDIW(IIACT3-1+J))GOTO 724
  723	   CONTINUE
	   DELETE=.TRUE.
	   NRDCON=NRDCON+1
	   ADDIW(DELCON-1+NRDCON)=I
  724	CONTINUE	   

	ELSEIF(LASTSV .EQ. 3) THEN
	DO 732 I=1,NACT
	   DO 731 J=1,ONACT2
	      IF(IACT(I).EQ.ADDIW(IIACT2-1+J))GOTO 732
  731	   CONTINUE
	   DELETE=.TRUE.
	   NRDCON=NRDCON+1
	   ADDIW(DELCON-1+NRDCON)=I
  732	CONTINUE	   

	ENDIF
	
	LASTND=NEXTND
		  
C++++++++DELETE CONSTRAINTS UNTILL ALL DIFFERENT ONES ARE ELIMINATED, I.E. NRDCON=0

C++++++++POSSIBLY NONE IS DIFFERENT
	IF(NRDCON .EQ. 0)GOTO 450
	KDROP=ADDIW(DELCON-1+NRDCON)
	NRDCON=NRDCON-1
	GOTO 601
		   
	ENDIF	   

	LASTND=NEXTND

C++++++++IF NO WARMSTART IS DESIRED, THEN START WITH A NEW QP FORM LABEL 1
       IF(IFAIL .EQ. 1) GOTO 1

C	Return according to IFAIL

C 2222 CONTINUE
      DO I=1,N
         X(I) = XBEST(I)
      ENDDO   
      DO I=1,MNN
         U(I) = UBEST(I)
      ENDDO  

      F = FBEST
      IF(IFAIL .NE. 0 .AND. B4PRNT .GT. 0)
     /	        write(iout,*)'!!!!!!!!!!!Problem in b4,ifail',
     /		Ifail

C*****************************************************************
  
  730 RETURN
C
C
C     THE REMAINING INSTRUCTIONS ARE USED AS SUBROUTINES.
C
C
C********************************************************************
C
C
C     CALCULATE THE LAGRANGE MULTIPLIERS BY PRE-MULTIPLYING THE
C     VECTOR IN THE S-PARTITION OF W BY THE INVERSE OF R.
C
  740 IR=IWR+(NACT+NACT*NACT)/2
      I=NACT
      SUM=ZERO
      GOTO 770
  750 IRA=IR-1
      SUM=ZERO
      IF (NACT.EQ.0) GOTO 761
      DO 760 J=I,NACT
      IW=IWW+J
      SUM=SUM+W(IRA)*W(IW)
  760 IRA=IRA+J
  761 IR=IR-I
      I=I-1
  770 IW=IWW+I
      IS=IWS+I
      W(IW)=(W(IS)-SUM)/W(IR)
      IF (I .GT. 1) GOTO 750
      IF (LFLAG .EQ. 3) GOTO 390
      IF (LFLAG .EQ. 4) GOTO 571
C
C     CALCULATE THE NEXT CONSTRAINT TO DROP.
C
C  775 IP=IWW+1
C      IPP=IWW+NACT
  775 KDROP=0
      IF (NACT.EQ.0) GOTO 791
      DO 790 K=1,NACT
      IF (IACT(K) .LE. MEQ) GOTO 790
      IW=IWW+K
      IF ((RES*W(IW)) .GE. ZERO) GOTO 790
      TEMP=W(K)/W(IW)
      IF (KDROP .EQ. 0) GOTO 780
      IF (DABS(TEMP) .GE. DABS(RATIO)) GOTO 790
  780 KDROP=K
      RATIO=TEMP
  790 CONTINUE
  791 GOTO (580,650), LFLAG
C
C
C********************************************************************
C
C
C     DROP THE CONSTRAINT IN POSITION KDROP IN THE ACTIVE SET.
C
		
  800 IA=IWA+IACT(KDROP)
c      write(iout,*)'800 kdrop',iact(kdrop)
      IF (IACT(KDROP) .GT. MN) IA=IA-N
      W(IA)=-W(IA)
      IF (KDROP .EQ. NACT) GOTO 850
C
C     SET SOME INDICES AND CALCULATE THE ELEMENTS OF THE NEXT
C     GIVENS ROTATION.
C
      IZ=IWZ+KDROP*N
      IR=IWR+(KDROP+KDROP*KDROP)/2
  810 IRA=IR
      IR=IR+KDROP+1
      TEMP=DMAX1(DABS(W(IR-1)),DABS(W(IR)))
      IF (TEMP.GT.VSMALL) THEN     
         SUM=TEMP*DSQRT((W(IR-1)/TEMP)**2+(W(IR)/TEMP)**2)
      ELSE
         SUM=VSMALL
      ENDIF   
      GA=W(IR-1)/SUM
      GB=W(IR)/SUM
C
C     EXCHANGE THE COLUMNS OF R.
C
      DO 820 I=1,KDROP
      IRA=IRA+1
      J=IRA-KDROP
      TEMP=W(IRA)
      W(IRA)=W(J)
  820 W(J)=TEMP
      W(IR)=ZERO
C
C     APPLY THE ROTATION TO THE ROWS OF R.
C
      W(J)=SUM
      KDROP=KDROP+1
      DO 830 I=KDROP,NU
      TEMP=GA*W(IRA)+GB*W(IRA+1)
      W(IRA+1)=GA*W(IRA+1)-GB*W(IRA)
      W(IRA)=TEMP
  830 IRA=IRA+I
C
C     APPLY THE ROTATION TO THE COLUMNS OF Z.
C
      DO 840 I=1,N
      IZ=IZ+1
      J=IZ-N
      TEMP=GA*W(J)+GB*W(IZ)
      W(IZ)=GA*W(IZ)-GB*W(J)
  840 W(J)=TEMP
C
C     REVISE IACT AND THE LAGRANGE MULTIPLIERS.
C
      IACT(KDROP-1)=IACT(KDROP)
      W(KDROP-1)=W(KDROP)
      IF (KDROP .LT. NACT) GOTO 810
  850 NACT=NACT-1
      GOTO (250,610), MFLAG
C
C
C********************************************************************
C
C
C     APPLY GIVENS ROTATION TO REDUCE SOME OF THE SCALAR
C     PRODUCTS IN THE S-PARTITION OF W TO ZERO.
C
  860 IZ=IWZ+NU*N
  870 IZ=IZ-N
  880 IS=IWS+NU
      NU=NU-1
      IF (NU .EQ. NACT) GOTO 900
      IF (W(IS) .EQ. ZERO) GOTO 870
      TEMP=DMAX1(DABS(W(IS-1)),DABS(W(IS)))
      SUM=TEMP*DSQRT((W(IS-1)/TEMP)**2+(W(IS)/TEMP)**2)
      GA=W(IS-1)/SUM
      GB=W(IS)/SUM
      W(IS-1)=SUM
      DO 890 I=1,N
      K=IZ+N
      TEMP=GA*W(IZ)+GB*W(K)
      W(K)=GA*W(K)-GB*W(IZ)
      W(IZ)=TEMP
  890 IZ=IZ-1
      GOTO 880
  900 GOTO (560,630), NFLAG
C
C
C********************************************************************
C
C
C     CALCULATE THE MAGNITUDE OF X AN REVISE XMAG.
C
  910 SUM=ZERO
      DO 920 I=1,N
      SUM=SUM+DABS(X(I))*VFACT*(DABS(GRAD(I))+DABS(G(I,I)*X(I)))
      IF (LQL) GOTO 920
      IF (SUM .LT. 1.D-30) GOTO 920
      VFACT=1.D-5*VFACT
      SUM=1.D-5*SUM
      XMAG=1.D-5*XMAG
  920 CONTINUE
      XMAG=DMAX1(XMAG,SUM)
      GOTO (420,690), JFLAG
C
C
C********************************************************************
C
C
C     PRE-MULTIPLY THE VECTOR IN THE W-PARTITION OF W BY Z TRANSPOSE.
C
  930 JL=IWW+1
      IZ=IWZ
      DO 940 I=1,N
      IS=IWS+I
      W(IS)=ZERO
      IWWN=IWW+N
      DO 940 J=JL,IWWN
      IZ=IZ+1
  940 W(IS)=W(IS)+W(IZ)*W(J)
      GOTO (350,550), KFLAG
      RETURN
      END
