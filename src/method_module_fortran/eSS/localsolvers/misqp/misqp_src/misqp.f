C
      SUBROUTINE MISQP(M,ME,N,NINT,NBIN,X,F,G,DF,DG,U,XL,XU,B,
     /                 ACC,ACCQP,MAXIT,MAXPEN,MAXUND,RESOPT,
     /                 NONMON,IPRINT,MODE,IFAIL,
     /                 RW,LRW,IW,LIW,LW,LLW,MAXNDE)
C
C***********************************************************************
C
C
C        An Implementation of a Trust Region Method for Solving
C            Mixed-Integer Nonlinear Optimization Problems 
C
C
C   MISQP solves the mixed-integer nonlinear program (MINLP)
C
C             minimize        F(X,Y)
C             subject to      G(J)(X,Y)   =  0  , J=1,...,ME
C                             G(J)(X,Y)  >=  0  , J=ME+1,...,M
C                             XL  <=  X  <=  XU
C                             YL  <=  Y  <=  YU
C
C   where X is a real and y an integer variable vector.
C
C   The Fortran subroutine is an implementation of a modified sequential 
C   quadratic programming (SQP) method. Under the assumption that integer 
C   variables have a 'smooth' influence on the model functions, i.e., that 
C   function values do not change too drastically when in- or decrementing 
C   an integer value, successive quadratic approximations are applied.
C   The algorithm is stabilized by a trust region method with Yuan's second 
C   order corrections. 
C
C   It is not assumed that the mixed-integer program is relaxable. In other 
C   words, function values are required only at integer points. The Hessian 
C   of the Lagrangian function is approximated by BFGS updates subject to 
C   the continuous variables. Derivative information subject to the integer 
C   variables is obtained by a difference formula evaluated at grid points.
C
C
C   USAGE:
C
C      CALL MISQP(M,ME,MME,N,NINT,NBIN,X,F,G,DF,DG,U,XL,XU,B,
C     /                 ACC,ACCQP,MAXIT,MAXPEN,MAXUND,RESOPT,
C     /                 NONMON,IPRINT,MODE,IFAIL,
C     /                 RW,LRW,IW,LIW,LW,LLW)
C
C
C   ARGUMENTS:
C
C   M :       Total number of constraints.
C   ME :      Number of equality constraints.
C   MMAX :    Row dimension of array DG containing Jacobian of constraints.
C             MMAX must be at least one and greater or equal to M+ME.
C   N :       Number of optimization variables, continuous and integer ones.
C   NINT :    Number of integer variables, must be less than or equal to N.
C   NBIN :    Number of binary variables, must be less than or equal to N.
C   NMAX :    Row dimension of C. NMAX must be at least two and greater than N.
C   MNN :     Must be equal to M+N+N, dimensioning parameter of multiplier 
C             vector.
C   X(N) :    Initially, X has to contain starting values. On return, X is
C             replaced by the current iterate. In the driving program
C             the row dimension of X has to be equal to NMAX.
C             X contains first the continuous, then the integer followed by
C             the binary variables.
C   F :       F contains the actual objective function value evaluated at X.
C   G(MMAX) :  G contains the actual constraint function values at X.
C   DF(NMAX) :  DF contains the gradient values of the objective function.
C   DG(MMAX,NMAX) :  DG contains the gradient values of constraints
C             in the first M rows. In the driving program, the row dimension
C             of DG has to be equal to MMAX.
C   U(MNN) :  On return, the first M locations contain
C             the multipliers of the M nonlinear constraints, the subsequent
C             N locations the multipliers subject to the lower bounds, and the
C             final N locations the multipliers subject to the upper bounds.
C             At an optimal solution, all multipliers with respect to
C             inequality constraints should be nonnegative.
C   XL(N),XU(N) :  On input, the one-dimensional arrays XL and XU must
C             contain the upper and lower bounds of the variables, first
C             for the continuous, then for the integer and subsequently for 
C             the binary variables.
C   B(NMAX,NMAX) :  On return, B contains the last computed approximation
C             of the Hessian matrix of the Lagrangian function.
C             In the driving program, the row dimension of C has to be equal
C             to NMAX. 
C   ACC :     The user has to specify the desired final accuracy
C             (e.g. 1.0D-7). The termination accuracy should not be smaller
C             than the accuracy by which gradients are computed. If ACC is
C             less or equal to zero, then the machine precision is computed
C             by MISQP and subsequently multiplied by 1.0D+4.
C   ACCQP :   The tolerance is needed for the QP solver to perform several
C             tests, for example whether optimality conditions are satisfied
C             or whether a number is considered as zero or not. If ACCQP is
C             less or equal to zero, then the machine precision is computed
C             by MISQP and subsequently multiplied by 1.0D+4.
C   MAXIT :   Maximum number of iterations, where one iteration corresponds to
C             one evaluation of a set of gradients (e.g. 100).
C   MAXPEN :  Maximum number of successive increments of the penalty parameter 
C             without success (e.g. 50).
C   MAXUND :  Maximum number of successive iterations without improvements
C             of the iterate X (e.g. 10).
C   RESOPT :  If set to TRUE and if integer variables exist, an additional 
C             restart will be performed to check whether an improvement 
C             is possible (recommended for functions with curved narrow valleys).
C   NONMON :  Maximum number of successive iterations, which are to be 
C             considered for the non-monotone trust region algorithm.
C   IPRINT :  Specification of the desired output level.
C          0 :  No output of the program.
C          1 :  Only a final convergence analysis is given.
C          2 :  One line of intermediate results is printed in each iteration.
C          3 :  More detailed information is printed for each iteration.
C          4 :  In addition, some messages of the QP solver are displayed.
C   MODE :   The parameter allows to change some default tolerances.
C          0 :   Use default values.
C          1 :   User-provided penalty parameter SIGMA, scaling constant DELTA, 
C                and initial trust region radii ITRC and ITRI for continuous
C                and integer variables in RW(1), RW(2), RW(3), and RW(4).
C                Default values are
C                  RW(1) = 10.0
C                  RW(2) = 0.01 
C                  RW(3) = 10.0
C                  RW(4) = 10.0
C   IOUT :    Integer indicating the desired output unit number, i.e., all
C             write-statements start with 'WRITE(IOUT,... '.
C   IFAIL :   The parameter shows the reason for terminating a solution
C             process. Initially IFAIL must be set to zero. On return IFAIL
C             could contain the following values:
C         -2 :   Compute new gradient values in DF and DG, see above.
C         -1 :   Compute new function values in F and G, see above.
C          0 :   Optimality conditions satisfied.
C          1 :   Termination after MAXIT iterations.
C          2 :   More than MAXUND iterations for fixing trust region.
C          3 :   More than MAXPEN updates of penalty parameter.
C          4 :   Termination at infeasible iterate.
C          5 :   Termination with zero trust region for integer variables.
C          6 :   Length of a working array is too short.
C          7 :   False dimensions, e.g., M$>$MMAX or N$>$=NMAX.
C        >90 :   QP solver terminated with an error message IFQL, 
C                IFAIL = IFQL + 100.
C   RW(LRW),LRW :  Real working array of length LRW, and LRW must be
C                  at least 5*NMAX*NMAX/2 + 10*(NINT+NBIN) + 25*N + MMAX + 
C                  10*M + 4*ME + 3*MAXNDE + 2*NONMON + 85.
C   IW(LIW),LIW :  Integer working array of length LIW, where LIW must be 
C                  at least NMAX + 5*MAXNDE + 2*(NINT+NBIN) + 24.
C   LW(LLW),LLW :  Logical working array of length LLW, where LLW must be
C                  at least 15+4*(NINT+NBIN).
C
C
C   FUNCTION AND GRADIENT EVALUATION:
C
C   The user has to provide functions and gradients in the same program, which
C   executes also misqp, according to the following rules:
C
C   1) Choose starting values for the variables to be optimized, and store
C      them in X.
C
C   2) Compute objective and all constraint function values values at X and
C      store them in F and G, respectively. 
C
C   3) Compute gradients of objective function and all constraints, and
C      store them in DF and DG, respectively. The j-th row of DG contains
C      the gradient of the j-th constraint, j=1,...,m. 
C
C   4) Set IFAIL=0 and execute MISQP.
C
C   5) If MISQP terminates with IFAIL=0, the internal stopping criteria are 
C      satisfied. 
C
C   6) In case of IFAIL>0, an error occurred.
C
C   7) If MISQP returns with IFAIL=-1, compute objective function values and
C      constraint values for all variables found in X, store them in F and G,
C      and call MISQP again. 
C
C   8) If MISQP terminates with IFAIL=-2, compute gradient values subject to
C      variables stored in X, and store them in DF and DG. Only partial 
C      derivatives subject to the continuous variables need to be provided. 
C      Then call MISQP again.
C
C
C
C
C   AUTHOR:     O. Exler
C   -------     Department of Computer Sience
C               University of Bayreuth
C               95440 Bayreuth
C               Germany
C
C
C
C   VERSION:    2.2 (02/2008) 
C   --------
C
C
C***********************************************************************
C
      IMPLICIT NONE
      INTEGER M,ME,MMAX,MNN,N,NMAX,NINT,NBIN,
     /        IOUT,IFAIL,IPRINT,MODE,MAXIT,MAXPEN,MAXUND,
     /        IW,LIW,LLW,LRW,ILOW,NONMON,MAXNDE
      DIMENSION        IW(LIW)
      DOUBLE PRECISION X,XL,XU,F,G,DF,DG,U,B,ACC,ACCQP,RW
      DIMENSION        X(N+1),XL(N+1),XU(N+1),
     /                 G(M+ME+1),DF(N+1), 
     /                 DG(M+ME+1,N+1),U(M+N+N),
     /                 B(N+1,N+1),RW(LRW)
      LOGICAL          LW,RESOPT
      DIMENSION        LW(LLW)
      EXTERNAL         MISQP0
C
C SET SOME CONSTANTS
C    
      IOUT=61
	IF (IFAIL.EQ.0.AND.IPRINT.GT.0) THEN
         OPEN(IOUT,FILE='misqp_print.txt',STATUS='UNKNOWN')
	END IF

      MMAX=M+ME+1
      NMAX=N+1
      MNN =M+N+N
    		
      CALL MISQP0(M,ME,MMAX,N,NINT,NBIN,NMAX,MNN,X,F,G,DF,DG,U,
     /            XL,XU,B,ACC,ACCQP,MAXIT,MAXPEN,MAXUND,RESOPT,
     /            NONMON,MAXNDE,IPRINT,MODE,IOUT,IFAIL,
     /            RW,LRW,IW,LIW,LW,LLW)

	IF (IFAIL.GE.0.AND.IPRINT.GT.0) THEN
	   CLOSE(IOUT)
	END IF
	
      END
C***********************************************************************
C END SUBROUTINE MISQP
C***********************************************************************
