*     Copyright 1992-1997 Numerical Algorithms Ltd. Oxford
      SUBROUTINE ZARG0(PROC,ISTAT,PM,RY,IY,X,Y,XY,N)
*
*     11 August 1993- Modified for compatibility with MATLAB 4.0a.1
*
      INTEGER        N,ISTAT,PM(*),RY(*),IY(*),X(*),Y(*),XY(*)
      CHARACTER *(*) PROC
      EXTERNAL       ZSIZE,ZPREAL,ZPIMAG
*
*     GET SIZE OF ARGUMENTS
*
      CALL ZSIZE(PROC,ISTAT,PM,RY,IY,X,Y,XY,N)
*
*     GET POINTERS TO REAL AND IMAGINARY PART OF ARGUMENTS
*
      CALL ZPREAL(PM,RY,N)
      CALL ZPIMAG(PM,IY,N)
*
      RETURN 
      END

      SUBROUTINE ZARG1(PM,RY,IY,X,Y,FLAG,N)
*
      INTEGER        N,PM(*),RY(*),IY(*),X(*),Y(*),FLAG(*),I
      EXTERNAL       ZSTORE,ZPREAL,ZPIMAG
*
*     ENSURE ARRAYS ARE ZERO.
*
      DO 10 I=1,N
        RY(I)=0
        IY(I)=0
10    CONTINUE
*
*     ALLOCATE SPACE FOR ARGUMENTS
*
      CALL ZSTORE(PM,X,Y,FLAG,N)
*
*     GET POINTERS TO REAL AND IMAGINARY PART OF ARGUMENTS
*
      CALL ZPREAL(PM,RY,N)
      CALL ZPIMAG(PM,IY,N)
*
      RETURN
      END

      SUBROUTINE ZTEST(PROC,NLHS,NRHS,NL,NR)
*
*     PURPOSE: CHECK TO SEE IF THE NUMBER OF INPUT AND OUTPUT 
*              ARGUMENTS MATCH. ON ERROR ISSUE AN APPROPRIATE 
*              MESSAGE, CLEAR HEAP AND RETURN TO MATLAB.
*
      INTEGER        NLHS,NRHS,NL,NR,LC,LS
      CHARACTER *(*) PROC
      CHARACTER * 6  C
      CHARACTER * 72 ECHO
      EXTERNAL       ZMEXERR,ZITOC,ZSTRLN
*
      CALL ZSTRLN(PROC,LS)
*
      IF (INT(NRHS,4) .NE. NR) THEN
          CALL ZITOC(LC,C, NR)
          ECHO=PROC(:LS)//' requires '//C(:LC)//' RHS arguments'
          CALL ZMEXERR(ECHO)
      ENDIF
*
      IF (INT(NLHS,4) .NE. NL) THEN
          CALL ZITOC(LC,C,NL)
          ECHO=PROC(:LS)//' requires '//C(:LC)//' LHS arguments'
          CALL ZMEXERR(ECHO) 
      ENDIF
*
      RETURN
      END

      SUBROUTINE ZPREAL(PM,YR,N)
*
*     PURPOSE: RETURN AN ARRAY OF POINTERS YR(I) TO THE REAL PART OF 
*              THE MATRICES POINTED TO BY PM(I).
*
      INTEGER  I,N,PM(*),YR(*),mxGetPr
      LOGICAL  ZSTRNG
#ifdef MATLAB      
!      EXTERNAL mxGetPr,ZSTRNG
!*
!      DO 10 I=1,N
!*
!*         IF ARGUMENT IS NOT A STRING ...
!*
!          IF (.NOT.ZSTRNG(YR(I))) YR(I)=mxGetPr(PM(I))
!*
! 10   CONTINUE
!*
!      RETURN
#endif      
      END

      SUBROUTINE ZPIMAG(PM,YI,N)
*
*     PURPOSE: RETURN AN ARRAY OF POINTERS YI(I) TO THE IMAGINARY PART 
*              OF THE MATRICES POINTED TO BY PM(I).
*
      INTEGER    I,N,PM(*),YI(*),mxGetPi
      LOGICAL    ZSTRNG     
      EXTERNAL   mxGetPi,ZSTRNG
#ifdef MATLAB
!*
!      DO 10 I=1,N
!*
!*         IF ARGUMENT IS NOT A STRING ...
!*
!          IF (.NOT.ZSTRNG(YI(I))) YI(I)=mxGetPi(PM(I))
!*
! 10   CONTINUE
!*
      RETURN
#endif
      END

      SUBROUTINE ZSTORE(PM,XDIM,YDIM,FLAG,N)
*
*     PURPOSE: CREATE STORAGE SPACE FOR MATRICES OF SIZE 
*              [XDIM(I),YDIM(I)] ON THE MATLAB HEAP AND 
*              RETURN POINTERS PM(I) TO THEM.
*                       
*              FLAG = 0 FOR REAL AND 1 FOR IMAGINARY MATRIX
*
#ifdef MATLAB
      INTEGER  I,N,PM(*),XDIM(*),YDIM(*),FLAG(*),mxCreateDoubleMatrix
      EXTERNAL mxCreateDoubleMatrix
*
      DO 10 I=1,N
         PM(I)=mxCreateDoubleMatrix(XDIM(I),YDIM(I),FLAG(I))
 10   CONTINUE
*
      RETURN
#endif      
      END

      SUBROUTINE ZSIZE(PROC,ISTAT,PM,RY,IY,X,Y,XY,N)
*
*     PURPOSE: GET DIMENSIONS [X,Y] OF THE MATRICES POINTED
*              TO BY PM(I). XY = X*Y
*
      INTEGER        M
      PARAMETER     (M=1024)
      INTEGER        I,J,LS,ISTAT,N,PM(*),X(*),Y(*)
      INTEGER        XY(*),RY(*),IY(*),ZGETSTR,status,mexEvalString
      CHARACTER *(*) PROC
      CHARACTER *(M)  S
      CHARACTER * 72 ECHO
      EXTERNAL       ZGETSIZ,ZMEXERR,ZITOC,ZSTRLN,ZGETSTR
*
      DO 10 I=1,N
*
*         IF ARGUMENT IS A STRING THEN SET REAL POINTER TO -1010
*         AND IMAGINARY POINTER TO ZERO WITH DIMENSIONS SET TO 1
*
          J=ZGETSTR(PM(I),S)
*
          IF (J.EQ.0) THEN
              RY(I)=-1010
              IY(I)= 0
              CALL ZGETSIZ(PM(I),X(I),Y(I))
              XY(I)=X(I)*Y(I)
          ELSE
*
              RY(I)=0
              IY(I)=0
*
              CALL ZGETSIZ(PM(I),X(I),Y(I))
              XY(I)=X(I)*Y(I)
*             
*             IF ARGUMENT HAS ZERO DIMENSION GIVE ERROR MESSAGE
*             
              IF (XY(I).EQ.0) THEN
*                 
                  CALL ZSTRLN(PROC,LS)
*                 
                  IF (LS.GT.0) THEN
                      ECHO='??? Error using ==> '//PROC(:LS)//'.m'
#ifdef MATLAB                      
                      status=mexEvalString('disp '''//ECHO//'''')
                      status=mexEvalString('disp '' ''')
#endif                      
                  ENDIF
*         
*         
                  IF (ISTAT.EQ.0) THEN
                     CALL PLACE_BLAME(I,S,LS,PROC)
                     ECHO='RHS argument ('//S(:LS)//') is undefined'
                  ELSE
                     CALL ZITOC(I,S,LS)
                     ECHO='LHS argument ('//S(:LS)//') is undefined'
                  ENDIF
*                 
                  CALL ZMEXERR(ECHO)
*
              ENDIF
*             
          ENDIF
*         
 10   CONTINUE
*     
      RETURN
      END


      SUBROUTINE ZINI(Y,IA,Q1,Q2,BUFFER)
*
*     PURPOSE: COPY THE INTEGER FORTRAN ARRAY Y TO THE MATLAB
*                       -------
*              ARRAY POINTED TO BY RY. 
*     ( INTEGER VERSION OF mxCopyReal8ToPtr )
*
      INTEGER          Q1(*),Q2(*)
      INTEGER          I,IA,N,RY,Y(*)
      DOUBLE PRECISION BUFFER(*)
*
      EXTERNAL         mxCopyReal8ToPtr
*     
      N =Q1(IA)
      RY=Q2(IA)
*
*     COPY Y TO BUFFER CONVERTING INTEGER TO DOUBLE PRECISION 
*      
      DO 10 I=1,N
         BUFFER(I)=DBLE(Y(I))
 10   CONTINUE
*
*     USE UTILITY FUNCTION mxCopyReal8ToPtr TO COPY BUFFER 
*     ( AND THUS Y ) TO MATLAB
*
#ifdef MATLAB
      CALL mxCopyReal8ToPtr(BUFFER,RY,N)
#endif      
*
      RETURN
      END

      SUBROUTINE ZINR(Y,IA,Q1,Q2,BUFFER)
*
*     PURPOSE: COPY THE SINGLE PRECISION FORTRAN ARRAY Y TO THE MATLAB
*                       ----------------
*              ARRAY POINTED TO BY RY. 
*     ( REAL VERSION OF mxCopyReal8ToPtr )
*
      INTEGER          Q1(*),Q2(*)
      INTEGER          I,IA,N,RY
      REAL             Y(*)
      DOUBLE PRECISION BUFFER(*)
*
      EXTERNAL         mxCopyReal8ToPtr
*
      N =Q1(IA)
      RY=Q2(IA)
*
*     COPY Y TO BUFFER CONVERTING  REAL TO DOUBLE PRECISION 
*      
      DO 10 I=1,N
         BUFFER(I)=DBLE(Y(I))
 10   CONTINUE
*
*     USE UTILITY FUNCTION mxCopyReal8ToPtr TO COPY BUFFER 
*     (THUS Y) TO MATLAB
*
#ifdef MATLAB
      CALL mxCopyReal8ToPtr(BUFFER,RY,N)
#endif      
*
      RETURN
      END


      SUBROUTINE ZIND(Y,IA,Q1,Q2)
*
      INTEGER          Q1(*),Q2(*)
      INTEGER          IA,N,RY
      DOUBLE PRECISION Y(*)
*
      EXTERNAL         mxCopyReal8ToPtr
*
      N =Q1(IA)
      RY=Q2(IA)
*
#ifdef MATLAB
      CALL mxCopyReal8ToPtr(Y,RY,N)
#endif      
*
      RETURN
      END


      SUBROUTINE ZINC(Y,IA,Q1,Q2,Q3,BUFFER)
*
*     PURPOSE: COPY THE COMPLEX FORTRAN ARRAY Y TO THE COMPLEX MATLAB
*                       -------
*              ARRAY POINTED TO BY RY & IY. 
*     ( COMPLEX VERSION OF mxCopyReal8ToPtr )
*
      INTEGER          Q1(*),Q2(*),Q3(*)
      INTEGER          I,IA,N,RY,IY
      COMPLEX          Y(*)
      DOUBLE PRECISION BUFFER(*)
      EXTERNAL         mxCopyReal8ToPtr
*
      N =Q1(IA)
      RY=Q2(IA)
      IY=Q3(IA)
*
*     COPY REAL PART OF Y TO BUFFER 
*      
      DO 10 I=1,N
         BUFFER(I)=DBLE(REAL(Y(I)))
 10   CONTINUE
*
*     USE mxCopyReal8ToPtr TO COPY BUFFER (THUS RE(Y)) TO MATLAB RY
*
#ifdef MATLAB
      CALL mxCopyReal8ToPtr(BUFFER,RY,N)
#endif      
*
*     COPY IMAGINARY PART OF Y TO BUFFER 
*      
      DO 20 I=1,N
         BUFFER(I)=DBLE(AIMAG(Y(I)))
 20   CONTINUE
*
*     USE mxCopyReal8ToPtr TO COPY BUFFER (THUS IM(Y)) TO MATLAB IY
*
#ifdef MATLAB
      CALL mxCopyReal8ToPtr(BUFFER,IY,N)
#endif      

      RETURN
      END

      SUBROUTINE ZINZ(Y,IA,Q1,Q2,Q3,BUFFER)
*
*     PURPOSE: COPY THE DOUBLE COMPLEX FORTRAN ARRAY Y TO THE COMPLEX 
*                       --------------
*              MATLAB ARRAY POINTED TO BY RY & IY.
*              
*
      INTEGER          I,IA,N,RY,IY
      INTEGER          Q1(*),Q2(*),Q3(*)
      DOUBLE PRECISION BUFFER(*)
      DOUBLE COMPLEX   Y(*)
      INTRINSIC DBLE
*
      EXTERNAL       mxCopyReal8ToPtr
*
      N =Q1(IA)
      RY=Q2(IA)
      IY=Q3(IA)
*
*     COPY REAL PART OF Y TO BUFFER 
*      
      DO 10 I=1,N
         BUFFER(I)=DBLE(Y(I))
 10   CONTINUE
*
*     USE mxCopyReal8ToPtr TO COPY BUFFER (THUS RE(Y)) TO MATLAB RY
*
#ifdef MATLAB
      CALL mxCopyReal8ToPtr(BUFFER,RY,N)
#endif      
*
*     COPY IMAGINARY PART OF Y TO BUFFER 
*     
      DO 20 I=1,N
          BUFFER(I)=DIMAG(Y(I))
 20   CONTINUE
*
*     USE mxCopyReal8ToPtr TO COPY BUFFER (THUS IM(Y)) TO MATLAB IY
*     
#ifdef MATLAB
      CALL mxCopyReal8ToPtr(BUFFER,IY,N)
#endif      
*
      RETURN
      END

      SUBROUTINE ZINL(Y,IA,Q1,Q2,BUFFER)
*
*     PURPOSE: COPY THE LOGICAL FORTRAN ARRAY Y TO THE MATLAB
*                       -------
*              ARRAY POINTED TO BY RY. 
*
      INTEGER          Q1(*),Q2(*)
      INTEGER          I,IA,N,RY
      LOGICAL          Y(*)
      DOUBLE PRECISION BUFFER(*)
      EXTERNAL         mxCopyReal8ToPtr
*
      N =Q1(IA)
      RY=Q2(IA)
*
*     COPY Y TO BUFFER CONVERTING INTEGER TO DOUBLE PRECISION 
*      
      DO 10 I=1,N
         IF (Y(I)) THEN
            BUFFER(I)=1.0D0
         ELSE
            BUFFER(I)=0.0D0
         ENDIF
 10   CONTINUE
*
*     USE UTILITY FUNCTION mxCopyReal8ToPtr TO COPY BUFFER 
*     ( AND THUS Y ) TO MATLAB
*
#ifdef MATlab
      CALL mxCopyReal8ToPtr(BUFFER,RY,N)
#endif      
*
      RETURN
      END

      SUBROUTINE ZINS(S,IA,Q1)
*
      INTEGER         IA,Q1(*),PM
      CHARACTER * (*) S
      EXTERNAL        mxCopyCharacterToPtr
*
      PM=Q1(IA)
#ifdef MATLAB      
*      CALL mxCopyCharacterToPtr(S,PM,LEN(S))
*
#endif
      RETURN
      END


      SUBROUTINE ZINX(S,IA,Q1,Q2,BUFFER)
*
      INTEGER          I,IA,Q1(*),Q2(*),N,PM(1),RPM,ASCII
      INTEGER          mxGetPr,mexCallMATLAB
      CHARACTER        C,S(*)
      DOUBLE PRECISION BUFFER(*)
#ifdef MATLAB      
      EXTERNAL         mxCopyReal8ToPtr,mxGetPr,mexCallMATLAB
*
      N =Q1(IA)
      PM(1)=Q2(IA)
*
*     COPY ARRAY TO STRING
*
      DO 10 I=1,N
          C = S(I)
          ASCII=ICHAR(C)
          BUFFER(I)=DBLE(ASCII)
 10   CONTINUE
*
      RPM=mxGetPr(PM(1))
*
#ifdef MATLAB
      CALL mxCopyReal8ToPtr(BUFFER,RPM,N)
#endif      
*
*     SET THE POINTER TO THAT OF A CHARACTER STRING
*
      I=mexCallMATLAB(1,Q2(IA),1,PM,'setstr')
*
      RETURN
#endif      
      END


      SUBROUTINE ZOUTI(PROC,ISTAT,IA,Y,Q1,Q2,Q3,BUFFER)
*
*     PURPOSE: COPY MATLAB ARRAY POINTED TO BY RY TO THE FORTRAN 
*              INTEGER ARRAY Y. ( INTEGER VERSION OF mxCopyPtrToReal8 )
*              -------
*
      INTEGER          Q1(*),Q2(*),Q3(*)
      INTEGER          I,IA,ISTAT,N,RY,IY,Y(*),LS,status,mexEvalString
      DOUBLE PRECISION BUFFER(*)
      LOGICAL          ERROR,ZSTRNG
      CHARACTER * (*)  PROC
      CHARACTER * 15    S
      CHARACTER * 72   ECHO
      EXTERNAL         mxCopyPtrToReal8,ZMEXERR,ZITOC,ZSTRLN,ZSTRNG
*
      N =Q1(IA)
      RY=Q2(IA)
      IY=Q3(IA)
*
*     IS ARGUMENT A STRING ?
*
      ERROR=ZSTRNG(RY)
*
      IF (.NOT. ERROR ) THEN
*
*         IS ARGUMENT COMPLEX ?
*         
          IF ( IY.NE.0 ) THEN
*  
#ifdef MATLAB           
#ifdef MPI      
              CALL mxCopyPtrToReal8(IY,BUFFER,N)
#endif   
#endif           
*             
*             CHECK CONTENTS OF BUFFER
*             
              DO 20 I=1,N
                  ERROR=ERROR .OR. (BUFFER(I).NE.0.0D0)
 20           CONTINUE    
*             
          ENDIF
*         
*         COPY MATLAB CONTENTS TO BUFFER
* 
#ifdef MATLAB        
#ifdef MPI      
          CALL mxCopyPtrToReal8(RY,BUFFER,N)
#endif       
#endif   
*         
*         COPY BUFFER TO Y CONVERTING DOUBLE PRECISION TO INTEGER
*         
          DO 10 I=1,N
*             
*             DOES ARGUMENT HAVE FRACTIONAL PART ?
*             
              ERROR=(ERROR .OR. (((BUFFER(I)-DBLE(IDINT(BUFFER(I)))) 
     +            .NE. 0.0D0) .OR. (RY.EQ.0)))
*             
              Y(I)=IDINT(BUFFER(I))
*             
 10       CONTINUE
*         
      ENDIF
*     
      IF (ERROR) THEN
*
          CALL ZSTRLN(PROC,LS)
*         
          IF (LS.GT.0) ECHO='??? Error using ==> '//PROC(:LS)//'.m'
#ifdef MATLAB                                
          status=mexEvalString('disp '''//ECHO//'''')
          status=mexEvalString('disp '' ''')
#endif          
*         
*         
          IF (ISTAT.EQ.0) THEN
             CALL PLACE_BLAME(IA,S,LS,PROC)
             ECHO='RHS argument ('//S(:LS)//') must be INTEGER'
          ELSE
             CALL ZITOC(IA,S,LS)
             ECHO='LHS argument ('//S(:LS)//') must be INTEGER'
          ENDIF
*
          CALL ZMEXERR(ECHO)
*         
      ENDIF
*
      RETURN
      END



      SUBROUTINE ZOUTR(PROC,ISTAT,IA,Y,Q1,Q2,Q3,BUFFER)
*
      INTEGER          Q1(*),Q2(*),Q3(*)
      INTEGER          I,IA,ISTAT,RY,IY,N,LS,status,mexEvalString
      REAL             Y(*)
      DOUBLE PRECISION BUFFER(*)
      CHARACTER * (*)  PROC
      CHARACTER * 15    S
      CHARACTER * 72   ECHO
      LOGICAL          ERROR,ZSTRNG
      EXTERNAL         mxCopyPtrToReal8,ZMEXERR,ZITOC,ZSTRLN,ZSTRNG
*
      N =Q1(IA)
      RY=Q2(IA)
      IY=Q3(IA)
*
*     IS ARGUMENT A STRING
*
      ERROR=ZSTRNG(RY)
*
      IF (.NOT. ERROR) THEN
*
          IF (IY.NE.0) THEN
*             
*             IS ARGUMENT COMPLEX 
*   
#ifdef MATLAB          
#ifdef MPI      
              CALL mxCopyPtrToReal8(IY,BUFFER,N)
#endif              
#endif
*             
*             CHECK CONTENTS OF BUFFER
*             
              DO 10 I=1,N
                  ERROR=ERROR .OR. ( BUFFER(I).NE.0.0D0)
 10           CONTINUE
*             
          ENDIF
*         
      ENDIF
*
      IF (ERROR) THEN
*             
          CALL ZSTRLN(PROC,LS)
*         
          IF (LS.GT.0) ECHO='??? Error using ==> '//PROC(:LS)//'.m'
#ifdef MATLAB                                
          status=mexEvalString('disp '''//ECHO//'''')
          status=mexEvalString('disp '' ''')
#endif          
*         
*         
          IF (ISTAT.EQ.0) THEN
             CALL PLACE_BLAME(IA,S,LS,PROC)
             ECHO='RHS argument ('//S(:LS)//') must be REAL'
          ELSE
             CALL ZITOC(IA,S,LS)
             ECHO='LHS argument ('//S(:LS)//') must be REAL'
          ENDIF
*
          CALL ZMEXERR(ECHO)
*         
      ENDIF
*  
#ifdef MATLAB   
#ifdef MPI
      CALL mxCopyPtrToReal8(RY,BUFFER,N)
#endif      
#endif
*
*     CONVERT TO SINGLE PRECISION
*
      DO 20 I=1,N
         Y(I)=SNGL(BUFFER(I))
20    CONTINUE
*
      RETURN
      END      

      SUBROUTINE ZOUTD(PROC,ISTAT,IA,Y,Q1,Q2,Q3)
*
      INTEGER          Q1(*),Q2(*),Q3(*)
      INTEGER          I,IA,ISTAT,RY,IY,N,LS,status,mexEvalString
      DOUBLE PRECISION Y(*)
      CHARACTER * (*)  PROC
      CHARACTER * 15    S
      CHARACTER * 72   ECHO
      LOGICAL          ERROR,ZSTRNG
      EXTERNAL         mxCopyPtrToReal8,ZMEXERR,ZITOC,ZSTRLN,ZSTRNG
*
      N =Q1(IA)
      RY=Q2(IA)
      IY=Q3(IA)
*
*     IS ARGUMENT A STRING
*
      ERROR=ZSTRNG(RY)
*
      IF (.NOT. ERROR) THEN
*
          IF (IY.NE.0) THEN
*             
*             IS ARGUMENT COMPLEX 
* 
#ifdef MATLAB            
#ifdef MPI
              CALL mxCopyPtrToReal8(IY,Y,N)
#endif              
#endif
*             
*             CHECK CONTENTS OF BUFFER
*             
              DO 10 I=1,N
                  ERROR=ERROR .OR. ( Y(I).NE.0.0D0)
 10           CONTINUE
*             
          ENDIF
*         
      ENDIF
*
      IF (ERROR) THEN
*             
          CALL ZSTRLN(PROC,LS)
*         
          IF (LS.GT.0) ECHO='??? Error using ==> '//PROC(:LS)//'.m'
#ifdef MATLAB                                
          status=mexEvalString('disp '''//ECHO//'''')
          status=mexEvalString('disp '' ''')
#endif
*         
*         
          IF (ISTAT.EQ.0) THEN
             CALL PLACE_BLAME(IA,S,LS,PROC)
             ECHO='RHS argument ('//S(:LS)//') must be REAL'
          ELSE
             CALL ZITOC(IA,S,LS)
             ECHO='LHS argument ('//S(:LS)//') must be REAL'
          ENDIF
*
          CALL ZMEXERR(ECHO)
*         
      ENDIF
*  
#ifdef MATLAB   
#ifdef MPI
      CALL mxCopyPtrToReal8(RY,Y,N)
#endif
#endif
*
      RETURN
      END      


      SUBROUTINE ZOUTC(PROC,ISTAT,IA,Y,Q1,Q2,Q3,BUFFER)
*
*     PURPOSE: COPY MATLAB ARRAY POINTED TO BY RY & IY TO THE FORTRAN 
*              COMPLEX ARRAY Y. ( COMPLEX VERSION OF mxCopyPtrToReal8 )
*              -------
*
      INTEGER          Q1(*),Q2(*),Q3(*)
      INTEGER          I,IA,ISTAT,N,RY,IY,LS,status,mexEvalString
      COMPLEX          Y(*)
      DOUBLE PRECISION BUFFER(*)
      CHARACTER *(*)   PROC
      CHARACTER  * 15   S
      CHARACTER  * 72  ECHO
      LOGICAL          ERROR,ZSTRNG
      EXTERNAL         mxCopyPtrToReal8,ZITOC,ZMEXERR,ZSTRLN,ZSTRNG
*
      N =Q1(IA)
      RY=Q2(IA)
      IY=Q3(IA)
*
*     IS ARGUMENT A STRING?
*
      ERROR=ZSTRNG(RY)
*     
      IF (.NOT. ERROR) THEN

*
*         COPY REAL PART OF MATLAB CONTENTS TO BUFFER
*     
#ifdef MATLAB    
#ifdef MPI2
          CALL mxCopyPtrToReal8(RY,BUFFER,N)
#endif          
#endif
*         
*         COPY BUFFER TO Y CONVERTING DOUBLE PRECISION TO REAL AND
*         THEN COMPLEX. NB. ONLY REAL PART IS STORED AT THIS STAGE.
*         
          DO 10 I=1,N
              Y(I)=CMPLX(SNGL(BUFFER(I)),0.0)
 10       CONTINUE
      ENDIF
*
      IF (ERROR) THEN
*
          CALL ZSTRLN(PROC,LS)
*         
          IF (LS.GT.0) ECHO='??? Error using ==> '//PROC(:LS)//'.m'
#ifdef MATLAB                                
          status=mexEvalString('disp '''//ECHO//'''')
          status=mexEvalString('disp '' ''')
#endif          
*         
*         
          IF (ISTAT.EQ.0) THEN
             CALL PLACE_BLAME(IA,S,LS,PROC)
             ECHO='RHS argument ('//S(:LS)//') must be COMPLEX'
          ELSE
             CALL ZITOC(IA,S,LS)
             ECHO='LHS argument ('//S(:LS)//') must be COMPLEX'
          ENDIF
*         
          CALL ZMEXERR(ECHO)
*         
      ENDIF
*
*     COPY IMAGINARY PART OF MATLAB CONTENTS TO BUFFER
*
      IF (IY.GT.0) THEN
*
#ifdef MATLAB
#ifdef MPI2
          CALL mxCopyPtrToReal8(IY,BUFFER,N)
#endif          
#endif
*         
*         COPY BUFFER TO Y CONVERTING DOUBLE PRECISION TO REAL AND 
*         THEN COMPLEX. ADD IMAGINARY PART TO REAL PART TO FORM RESULT.
*         
          DO 20 I=1,N
              Y(I)=Y(I)+CMPLX(0.0,SNGL(BUFFER(I)))
 20       CONTINUE
*         
      ENDIF
*
      RETURN
      END
      SUBROUTINE ZOUTZ(PROC,ISTAT,IA,Y,Q1,Q2,Q3,BUFFER)
*
*     PURPOSE: COPY MATLAB ARRAY POINTED TO BY RY & IY TO THE FORTRAN 
*              COMPLEX ARRAY Y. 
*              -------
*     ( D-COMPLEX VERSION OF mxCopyPtrToReal8 )
*
      INTEGER          Q1(*),Q2(*),Q3(*)
      INTEGER          I,IA,ISTAT,N,RY,IY,LS,status,mexEvalString
      DOUBLE COMPLEX   Y(*)
      DOUBLE PRECISION BUFFER(*)
      CHARACTER *(*)   PROC
      CHARACTER  * 15   S
      CHARACTER  * 72  ECHO
      LOGICAL          ERROR,ZSTRNG
      EXTERNAL         mxCopyPtrToReal8,ZITOC,ZMEXERR,ZSTRLN,ZSTRNG
*
      N =Q1(IA)
      RY=Q2(IA)
      IY=Q3(IA)
*
*     IS ARGUMENT A STRING?
*
      ERROR=ZSTRNG(RY)
*
*     INITIALIZE FORTRAN ARRAY 'Y'  TO ZERO
*     
      IF (.NOT. ERROR) THEN
*         
*         COPY REAL PART OF MATLAB CONTENTS TO BUFFER
*         
          IF (RY.GT.0) THEN
*  
#ifdef MATLAB           
#ifdef MPI
              CALL mxCopyPtrToReal8(RY,BUFFER,N)
#endif              
#endif
*             
*             COPY BUFFER TO Y CONVERTING DOUBLE PRECISION TO REAL AND
*             THEN COMPLEX. NB. ONLY REAL PART IS STORED AT THIS STAGE.
*             
              DO 10 I=1,N
                  Y(I)=DCMPLX(BUFFER(I),0.0D0)
 10           CONTINUE
*             
          ENDIF
*         
*         CHECK IF IMAGINARY PART EXISTS ( I.E. IY IS NOT ZERO )
*         
*         COPY IMAGINARY PART OF MATLAB CONTENTS TO BUFFER
*         
          IF (IY.GT.0) THEN
*           
#ifdef MATLAB  
#ifdef MPI
              CALL mxCopyPtrToReal8(IY,BUFFER,N)
#endif              
#endif
*             
*             COPY BUFFER TO Y CONVERTING DOUBLE PRECISION TO REAL
*             AND THEN COMPLEX. ADD IMAGINARY PART TO REAL PART TO 
*             FORM  RESULT.
*             
              DO 20 I=1,N
                  Y(I)=Y(I)+DCMPLX(0.0D0,BUFFER(I))
 20           CONTINUE
*             
          ENDIF
*         
      ENDIF
*
      IF (ERROR) THEN
*
          CALL ZSTRLN(PROC,LS)
*         
          IF (LS.GT.0) ECHO='??? Error using ==> '//PROC(:LS)//'.m'
#ifdef MATLAB                                
          status=mexEvalString('disp '''//ECHO//'''')
          status=mexEvalString('disp '' ''')
#endif          
*         
*         
          IF (ISTAT.EQ.0) THEN
             CALL PLACE_BLAME(IA,S,LS,PROC)
             ECHO='RHS argument ('//S(:LS)//') must be COMPLEX'
          ELSE
             CALL ZITOC(IA,S,LS)
             ECHO='LHS argument ('//S(:LS)//') must be COMPLEX'
          ENDIF
*         
          CALL ZMEXERR(ECHO)
*         
      ENDIF
*
      RETURN
      END

      SUBROUTINE ZOUTL(PROC,ISTAT,IA,Y,Q1,Q2,Q3,BUFFER)
*
*     PURPOSE: COPY MATLAB ARRAY POINTED TO BY RY TO THE FORTRAN 
*              LOGICAL ARRAY Y. 
*              -------
*
      INTEGER          Q1(*),Q2(*),Q3(*)
      INTEGER          I,IA,ISTAT,N,RY,IY,LS,status,mexEvalString
      LOGICAL          Y(*)
      DOUBLE PRECISION BUFFER(*)
      CHARACTER *(*)   PROC
      CHARACTER * 15    S
      CHARACTER * 72   ECHO
      LOGICAL          ERROR,ZSTRNG
      EXTERNAL         mxCopyPtrToReal8,ZMEXERR,ZITOC,ZSTRLN,ZSTRNG
*
      N =Q1(IA)
      RY=Q2(IA)
      IY=Q3(IA)
*
*     IS ARGUMENT A STRING?
*
      ERROR=ZSTRNG(RY)
*
      IF (.NOT. ERROR) THEN
*         
*         IS ARGUMENT COMPLEX ?
*         
          IF (IY.NE.0) THEN
*             
#ifdef MATLAB
              CALL mxCopyPtrToReal8(IY,BUFFER,N)
#endif              
*             
*             CHECK CONTENTS OF BUFFER
*             
              DO 20 I=1,N
                  ERROR=ERROR .OR. (BUFFER(I).NE.0.0D0)
 20           CONTINUE      
*             
          ENDIF
*         
*         COPY MATLAB CONTENTS TO BUFFER
*         
#ifdef MATLAB
          CALL mxCopyPtrToReal8(RY,BUFFER,N)
#endif          
*         
*         COPY BUFFER TO Y CONVERTING DOUBLE PRECISION TO INTEGER
*         
          DO 10 I=1,N
*             
*             CHECK WHETHER BUFFER CONTAINS 0'S AND 1'S          
*             
              ERROR=(ERROR.OR.(BUFFER(I).NE.0.0D0).AND.
     +            (BUFFER(I).NE.1.0D0)) 
*             
              IF (IDINT(BUFFER(I)).EQ.1) THEN
                  Y(I)=.TRUE.
              ELSE
                  Y(I)=.FALSE.
              ENDIF   
 10       CONTINUE
*       
      ENDIF
*
      IF (ERROR) THEN
*
          CALL ZSTRLN(PROC,LS)
*         
          IF (LS.GT.0) ECHO='??? Error using ==> '//PROC(:LS)//'.m'
#ifdef MATLAB                                
          status=mexEvalString('disp '''//ECHO//'''')
          status=mexEvalString('disp '' ''')
#endif          
*         
*         
          IF (ISTAT.EQ.0) THEN
             CALL PLACE_BLAME(IA,S,LS,PROC)
             ECHO='RHS argument ('//S(:LS)//') must be LOGICAL'
          ELSE
             CALL ZITOC(IA,S,LS)
             ECHO='LHS argument ('//S(:LS)//') must be LOGICAL'
          ENDIF
*
          CALL ZMEXERR(ECHO)
*         
      ENDIF
*     
      RETURN
      END

      SUBROUTINE ZOUTS(PROC,ISTAT,IA,STRING,Q1)
*
*     PURPOSE: COPY THE MATLAB STRING POINTED TO BY PM TO THE
*              FORTRAN ARRAY 'STRING'
*              ------
*     14 Nov. 1996 - Modified for compatibility with MATLAB 5.0 Beta 10
*
      INTEGER         Q1(*)
      INTEGER         I,IA,ISTAT,PM,LS,mxIsChar,mxGetString,
     +     status,mexEvalString
      CHARACTER * (*) STRING,PROC
      CHARACTER * 15   S
      CHARACTER * 72  ECHO
      EXTERNAL        mxIsChar,mxGetString,ZMEXERR,ZITOC,ZSTRLN
*
      PM=Q1(IA)
*
      STRING=' '
*
*     CHECK THE CONTENTS OF S AND RETURN THE LENGTH OF THE SUBSTRING 
*     CONTAINING VALID FORTRAN CHARACTERS ( IN THE COLLATING SEQUENCE )
*
#ifdef MATLAB
      IF ( mxIsChar(PM).EQ.1 ) THEN
          I = mxGetString(PM,STRING,1024)
      ELSE
*
          CALL ZSTRLN(PROC,LS)
*         
          IF (LS.GT.0) ECHO='??? Error using ==> '//PROC(:LS)//'.m'
#ifdef MATLAB                                
          status=mexEvalString('disp '''//ECHO//'''')
          status=mexEvalString('disp '' ''')
#endif          
*         
*         
          IF (ISTAT.EQ.0) THEN
             CALL PLACE_BLAME(IA,S,LS,PROC)
             ECHO='RHS argument ('//S(:LS)//') must be STRING'
          ELSE
             CALL ZITOC(IA,S,LS)
             ECHO='LHS argument ('//S(:LS)//') must be STRING'
          ENDIF
*         
          CALL ZMEXERR(ECHO)
*         
      ENDIF
#endif      
*
      RETURN
      END

      SUBROUTINE ZOUTX(PROC,ISTAT,IA,STRING,Q1,Q2)
*
*     PURPOSE: COPY THE MATLAB STRING POINTED TO BY PM TO THE
*              FORTRAN ARRAY 'STRING'
*              ------
*     14 Nov. 1996 - Modified for compatibility with MATLAB 5.0 Beta 10
*
      INTEGER         Q1(*),Q2(*)
      INTEGER         I,IA,ISTAT,PM,LS,N,mxIsChar,mxGetString,
     +     status,mexEvalString
      CHARACTER * 1024 BUFFER
      CHARACTER       STRING(*),PROC
      CHARACTER * 15   S
      CHARACTER * 72  ECHO
      EXTERNAL        mxIsChar,mxGetString,ZMEXERR,ZITOC,ZSTRLN
*
      N  = Q1(IA)
      PM = Q2(IA)
*
*     CHECK THE CONTENTS OF S AND RETURN THE LENGTH OF THE SUBSTRING 
*     CONTAINING VALID FORTRAN CHARACTERS ( IN THE COLLATING SEQUENCE )
*
#ifdef MATLAB
      IF ( mxIsChar(PM).EQ.0 ) THEN
*
          CALL ZSTRLN(PROC,LS)
*         
          IF (LS.GT.0) ECHO='??? Error using ==> '//PROC(:LS)//'.m'
#ifdef MATLAB                                
          status=mexEvalString('disp '''//ECHO//'''')
          status=mexEvalString('disp '' ''')
#endif          
*         
*         
          IF (ISTAT.EQ.0) THEN
             CALL PLACE_BLAME(IA,S,LS,PROC)
             ECHO='RHS argument ('//S(:LS)//') must be STRING'
          ELSE
             CALL ZITOC(IA,S,LS)
             ECHO='LHS argument ('//S(:LS)//') must be STRING'
          ENDIF
*         
          CALL ZMEXERR(ECHO)
*
      ELSE
          I = mxGetString(PM,BUFFER,1024)
*
*     COPY BUFFER TO FORTRAN ARRAY
*             
          DO 10 I = 1 , N
              STRING(I)=BUFFER(I:I)
 10       CONTINUE
*         
      ENDIF
*
#endif
      RETURN
      END

      SUBROUTINE ZCHECK(PROC,ISTAT,X1,Y1,X2,Y2,Z,RY,N)
*
*     PURPOSE: CHECK DIMENSIONS OF ARGUMENTS
*    
*     NOTE   : IF THE SPECIFIED DIMENSION OF A VARIABLE IS = 0
*              THEN IT IS SET TO THE ACTUAL INPUT DIMENSION.
*              HENCE THE CORRECT NUMBER OF ELEMENTS ARE COPIED
*              TO/FROM MATLAB TO/FROM FORTRAN
*
      INTEGER         I,J,N,LS,X1(*),Y1(*),X2(*),Y2(*),Z(*)
      INTEGER         ISTAT,RY(*),status,mexEvalString
      CHARACTER * (*) PROC
      CHARACTER *  15  S
      CHARACTER * 72  ECHO
      LOGICAL ERROR
      EXTERNAL        ZMEXERR,ZITOC,ZSTRLN
*
      DO 10 I=1,N
*       
*       IF ARGUMENT IS NOT A STRING THEN CHECK DIMENSIONS
*
*       IF ROW VECTOR THEN REVERSE DIMENSIONS TO DISABLE 
*       DIMENSION CHECKING
*
        ERROR=.FALSE.
        IF (X2(I).EQ.0 .AND. Y2(I).EQ.0) THEN
C String: Set size
          X2(I)=X1(I)
          Y2(I)=Y1(I)
          Z(I)=X1(I)*Y1(I)
        ELSE IF (Y2(I).EQ.0) THEN
C Two dimesional assumed size array, check first dimension
          IF (X1(I).NE.X2(I)) THEN
            ERROR=.TRUE.
          ELSE
            Y2(I)=Y1(I)
            Z(I)=X1(I)*Y1(I)
          ENDIF
        ELSE IF (X2(I).EQ.0) THEN
C One dimesional assumed size array, check second dimension is one
          IF (Y1(I).EQ.1) THEN
            X2(I)=X1(I)
            Z(I)=X1(I)*Y1(I)
          ELSE IF (X1(I).EQ.1) THEN
            X2(I)=Y1(I)
            Z(I)=X1(I)*Y1(I)
          ELSE
            ERROR=.TRUE.
          ENDIF
        ELSE IF (X1(I).NE.X2(I) .OR. Y1(I).NE.Y2(I)) THEN
          ERROR=.TRUE.
          IF (X1(I).EQ.1 .AND. Y2(I).EQ.1 .AND.
     +       (X1(I).EQ.Y2(I) .AND. Y1(I).EQ.X2(I))) THEN
C Accept a row vector, where a column vector is expected
            ERROR=.FALSE.
          ENDIF
        ENDIF
        IF (ERROR) THEN
            CALL ZSTRLN(PROC,J)
*         
            IF (J.GT.0) ECHO='??? Error using ==> '//PROC(:J)//'.m'
*         
#ifdef MATLAB                      
            status=mexEvalString('disp '''//ECHO//'''')
            status=mexEvalString('disp '' ''')
#endif            
*         
*         CONVERT INTEGER TO CHARACTER
*           
*         
            WRITE (ECHO,'(A,I10)') 'Supplied first dimension :',X1(I)
#ifdef MATLAB                                  
            status=mexEvalString('disp '''//ECHO//'''')
#endif            
            WRITE (ECHO,'(A,I10)') 'Expected first dimension :',X2(I)
#ifdef MATLAB                                  
            status=mexEvalString('disp '''//ECHO//'''')
#endif 
            IF (Y1(I).GT.1.OR.Y2(I).GT.1) THEN
               WRITE (ECHO,'(A,I10)') 'Supplied second dimension:',Y1(I)
#ifdef MATLAB                                  
            status=mexEvalString('disp '''//ECHO//'''')
#endif 
               WRITE (ECHO,'(A,I10)') 'Expected second dimension:',Y2(I)
#ifdef MATLAB                                  
            status=mexEvalString('disp '''//ECHO//'''')
#endif 
            END IF
            IF (ISTAT.EQ.1) THEN
               CALL ZITOC(I,S,LS)
               ECHO='LHS argument ('//S(:LS)//
     +              ') has incorrect dimension'
            ELSE
               CALL PLACE_BLAME(I,S,LS,PROC)
               ECHO='RHS argument ('//S(:LS)//
     +              ') has incorrect dimension'
            ENDIF
*         
            CALL ZMEXERR(ECHO)
*         
        ENDIF
*       
 10   CONTINUE
*     
      RETURN
      END
      
      SUBROUTINE ZSTRLN(S,LS)
*
*     PURPOSE : DETERMINE THE LENGTH OF THE CHARACTER STRING 'S'
*
      INTEGER        LS,LENS,ASCII
      CHARACTER *(*) S
*
      LENS=LEN(S)
*
      LS=1
*
 10   IF (LS .LE. LENS) THEN
*
          ASCII=ICHAR(S(LS:LS))
*
          IF (ASCII.GT.32) THEN
              LS=LS+1
              GOTO 10
          ENDIF
*         
      ENDIF
*
      LS=LS-1
*
      RETURN
      END


      SUBROUTINE ZITOC(I,C,L)
*
*     PURPOSE : CONVERT INTEGER 'I' TO A CHARACTER STRING 'C'.
*               L = LENGTH OF THE STRING
*
      INTEGER        I,J,L,N,DEC,DIGIT,LENC,status,mexEvalString
      CHARACTER *(*) C
*
      C= ' '
*
      LENC=LEN(C)
*
      N = I
*
*     DETERMINE THE NUMBER OF DIGITS IN COMPRISING THE INTEGER
*
      L = 1
*
 10   IF ((N/10) .GE. 1) THEN
         L=L+1
         N=N/10
         GOTO 10
      ENDIF
*     
*     EXTRACT DIGITS FROM LEFT TO RIGHT AND COPY TO STRING
*
      N=I
*
      DO 20 J=1,L
*
*         IF STRING CAN NOT HOLD INTEGER ISSUE WARNING AND RETURN
*
          IF (J .GT. LENC) THEN
#ifdef MATLAB              
              status=mexEvalString
     +             ('(ZITOC) WARNING : INTEGER LONGER THAN STRING')
              status=mexEvalString("disp ' '")
#endif              
              RETURN
          ELSE
              DEC   = 10**(L-J)
              DIGIT = N/DEC
              WRITE(C(J:J),30) DIGIT
              N = MOD(N,DEC)
          ENDIF
*
 20   CONTINUE
*
 30   FORMAT(I1)
*
      RETURN
      END
      
      LOGICAL FUNCTION ZSTRNG(I)
*
*     PURPOSE: .TRUE.    IF I=-1010 ( POINTER TO A STRING )
*              .FALSE.   OTHERWISE
*
      INTEGER    I
*     
      ZSTRNG = (I .EQ. -1010)
*
      RETURN
      END

      INTEGER FUNCTION ZIDSET(MFILE,NAME)
*
*     THIS SUBROUTINE SETS THE DIMENSION OF ALL ASSUMED SIZE ARRAYS
*     USED IN SUB PROCEDURE ARGUMENTS. IT LOOKS FOR THE DIMENSION
*     OF THE VARIABLE 'NAME' SET IN THE MAIN MATLAB AREA AS NAME_DIM1
*     FOR VECTORS AND NAME_DIM2 FOR MATRICES. IF THE DIMENSION CANNOT
*     BE DETERMINED AN APPROPRIATE ERROR MESSAGE IS DISPLAYED AND 
*     CONTROL IS RETURNED TO MATLAB
*
      CHARACTER *(*)   MFILE,NAME
      CHARACTER * 72   ECHO
      INTEGER          mexGetVariablePtr,I,PM,mexEvalString,mxGetM
      INTEGER          IMX,status
      LOGICAL          FAIL
      DOUBLE PRECISION mxGetScalar
#ifdef MATLAB      
      EXTERNAL         mexGetVariablePtr,mxGetScalar,MexEvalString,
     +                 ZMEXERR
#endif     
*
*     GET POINTER TO THE NAMED VARIABLE
*
      ECHO='global '//NAME
*
*     Declare variable as "global" MATLAB-4
*
#ifdef MATLAB
      PM = mexEvalString(ECHO(1:7+LEN(NAME)))
      PM = mexGetVariablePtr(NAME)
#endif      
*
*     IF VARIABLE NOT DEFINED THEN GIVE ERROR MESSAGE
*
      IF ( PM .LE. 0) THEN
         FAIL = .TRUE.
         IMX = 0
      ELSE
         FAIL = .FALSE.
#ifdef MATLAB         
         IMX = mxGetM(PM)
#endif         
      ENDIF
      IF ( FAIL .OR. IMX .LE. 0 ) THEN
          ECHO='??? Error using ==> '//MFILE
#ifdef MATLAB                                        
          status=mexEvalString('disp '''//ECHO//'''')
          status=mexEvalString('disp '' ''')
#endif          
          I=INDEX(NAME,'_')
          ECHO='Undefined dimension for assumed size array '//
     +         '"'// NAME(:I-1)//'"'
#ifdef MATLAB                                             
          status=mexEvalString('disp '''//ECHO//'''')
          ECHO='  i.e. MATLAB global '//'"'//NAME//'" has not been set.'
          status=mexEvalString('disp '''//ECHO//'''')
          status=mexEvalString('disp '' ''')
        
          ECHO='see "ngg_errors" for further information.'
          CALL ZMEXERR(ECHO)
#endif  
      ELSE
#ifdef MATLAB          
          ZIDSET=INT(mxGetScalar(PM))
#endif          
      ENDIF
      RETURN
      END

      FUNCTION ZALREAL(N)
      INTEGER M, N, ZALREAL, mxCalloc
      EXTERNAL mxCalloc
      M = MAX0(1,N)
#ifdef MATLAB      
      ZALREAL = mxCalloc(M,8)
#endif      
      RETURN
      END

      SUBROUTINE ZGETSIZ(PM,M,N)
      INTEGER PM,M,N,mxGetM, mxGetN
#ifdef MATLAB            
      EXTERNAL mxGetM, mxGetN
      M = mxGetM(PM)
      N = mxGetN(PM)
#endif      
      RETURN
      END

      SUBROUTINE ZMEXERR(S)
      CHARACTER *(*) S
#ifdef MATLAB      
      EXTERNAL mexErrMsgTxt
      CALL mexErrMsgTxt(S)
#endif      
      RETURN
      END

      FUNCTION ZMATFCN(NLHS,PLHS,NRHS,PRHS,S)
      INTEGER NLHS,NRHS,PLHS(*),PRHS(*),ZMATFCN,mexCallMATLAB
      CHARACTER *(*) S
      EXTERNAL mexCallMATLAB
#ifdef MATLAB      
      ZMATFCN=mexCallMATLAB(NLHS,PLHS,NRHS,PRHS,S)
#endif      
      RETURN
      END
      
      FUNCTION ZGETSCA(PM)
      INTEGER PM,ZGETSCA
      DOUBLE PRECISION mxGetScalar
      EXTERNAL mxGetScalar
#ifdef MATLAB      
      ZGETSCA=INT(mxGetScalar(PM))
#endif      
      RETURN
      END

      FUNCTION ZGETSTR(PM,S)
*     14 Nov. 1996 - Modified for compatibility with MATLAB 5.0 Beta 10
      INTEGER I,PM,ZGETSTR,mxGetString,mxIsChar
      CHARACTER *(*) S
#ifdef MATLAB      
      EXTERNAL mxGetString,mxIsChar
      IF (mxIsChar(PM).EQ.1) THEN
          I = mxGetString(PM,S,LEN(S))
*     Return zero for success as per MATLAB 4.
          ZGETSTR=0
      ELSE
          ZGETSTR=1
      ENDIF
      RETURN
#endif      
      END

      SUBROUTINE MexFunction(NLHS,PLHS,NRHS,PRHS)
      INTEGER  NLHS,NRHS,PLHS(*),PRHS(*)
      EXTERNAL ZMEXFUN
      CALL ZMEXFUN(NLHS,PLHS,NRHS,PRHS)
      RETURN
      END


      SUBROUTINE PLACE_BLAME(ARGNO,STRING,STRLEN,PROC)
      CHARACTER*(*)STRING
      CHARACTER*(*)PROC

      INTEGER M,ARGNO,STRLEN,PROC_LEN
      PARAMETER (M=2048)
      CHARACTER*(M) NAMES
      CHARACTER*(30) PROC_NAME
      COMMON /ARGLIST/ PROC_NAME,NAMES
      SAVE /ARGLIST/
      INTEGER LASTCH,FISTCH,COUNT,CHLEN
      INTEGER LS
*
      COUNT = 0
      FISTCH = 1
      LASTCH = 1
      CHLEN = 0

      CALL ZITOC(ARGNO,STRING,STRLEN)
      CALL ZSTRLN(PROC,LS)
C     Check to see if the names list of for this procedure
      PROC_LEN=ICHAR(PROC_NAME(1:1)) - 49
      IF (PROC_NAME(2:PROC_LEN+1).EQ.PROC(1:LS)) THEN
 10      CONTINUE
         COUNT = COUNT + 1
         FISTCH = FISTCH+CHLEN
         CHLEN=INDEX(NAMES(LASTCH:),',')
         LASTCH = FISTCH+CHLEN
         IF (COUNT.LT.ARGNO.AND.CHLEN.NE.0) GOTO 10
         
         IF (COUNT.EQ.ARGNO) THEN
            IF (CHLEN.EQ.0) THEN
               LASTCH = FISTCH
 20            IF(NAMES(LASTCH:LASTCH).NE.' '.AND.
     $              LASTCH.LT.LEN(NAMES))THEN
                  LASTCH=LASTCH+1
                  GOTO 20
               END IF
               STRING=NAMES(FISTCH:LASTCH-1)
               STRLEN=LASTCH-FISTCH
            ELSE IF (COUNT.EQ.ARGNO) THEN
               STRING=NAMES(FISTCH:LASTCH-2)
               STRLEN = LASTCH-FISTCH-1
            ENDIF
         ENDIF
      ENDIF
      
      RETURN
      END
      SUBROUTINE ZFREE(pl,nl,pr,nr)

C     PURPOSE: FREE STORAGE SPACE FOR MATRICES ON THE MATLAB HEAP

      INTEGER i, nl,nr , pl(*), pr(*)
      EXTERNAL mxDestroyArray
#ifdef MATLAB
      DO 10 i = 1, nl
        call mxDestroyArray(pl(i))
10    CONTINUE
      DO 20 i = 1, nr
        call mxDestroyArray(pr(i))
20    CONTINUE
#endif
      END

