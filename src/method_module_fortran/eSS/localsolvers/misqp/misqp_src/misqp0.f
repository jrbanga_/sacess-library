C
      SUBROUTINE MISQP0(M,ME,MMAX,N,NINT,NBIN,NMAX,MNN,X,F,G,DF,DG,U,
     /                 XL,XU,B,ACC,ACCQP,MAXIT,MAXPEN,MAXUND,RESOPT,
     /                 NONMON,MAXNDE,IPRINT,MODE,IOUT,IFAIL,
     /                 RW,LRW,IW,LIW,LW,LLW)

C***********************************************************************
C
C
C        An Implementation of a Trust Region Method for Solving
C            Mixed-Integer Nonlinear Optimization Problems 
C
C
C   MISQP solves the mixed-integer nonlinear program (MINLP)
C
C             minimize        F(X,Y)
C             subject to      G(J)(X,Y)   =  0  , J=1,...,ME
C                             G(J)(X,Y)  >=  0  , J=ME+1,...,M
C                             XL  <=  X  <=  XU
C                             YL  <=  Y  <=  YU
C
C   where X is a real and y an integer variable vector.
C
C   The Fortran subroutine is an implementation of a modified sequential 
C   quadratic programming (SQP) method. Under the assumption that integer 
C   variables have a 'smooth' influence on the model functions, i.e., that 
C   function values do not change too drastically when in- or decrementing 
C   an integer value, successive quadratic approximations are applied.
C   The algorithm is stabilized by a trust region method with Yuan's second 
C   order corrections. 
C
C   It is not assumed that the mixed-integer program is relaxable. In other 
C   words, function values are required only at integer points. The Hessian 
C   of the Lagrangian function is approximated by BFGS updates subject to 
C   the continuous variables. Derivative information subject to the integer 
C   variables is obtained by a difference formula evaluated at grid points.
C
C
C   USAGE:
C
C      CALL MISQP(M,ME,MMAX,N,NINT,NBIN,NMAX,MNN,X,F,G,DF,DG,U,
C     /                 XL,XU,B,ACC,ACCQP,MAXIT,MAXPEN,MAXUND,
C     /                 RESOPT,NONMON,MAXNDE,IPRINT,
C     /                 MODE,IOUT,IFAIL,RW,LRW,IW,LIW,LW,LLW)
C
C
C   ARGUMENTS:
C
C   M :       Total number of constraints.
C   ME :      Number of equality constraints.
C   MMAX :    Row dimension of array DG containing Jacobian of constraints.
C             MMAX must be at least one and greater or equal to M+ME.
C   N :       Number of optimization variables, continuous and integer ones.
C   NINT :    Number of integer variables, must be less than or equal to N.
C   NBIN :    Number of binary variables, must be less than or equal to N.
C   NMAX :    Row dimension of C. NMAX must be at least two and greater than N.
C   MNN :     Must be equal to M+N+N, dimensioning parameter of multiplier 
C             vector.
C   X(N) :    Initially, X has to contain starting values. On return, X is
C             replaced by the current iterate. In the driving program
C             the row dimension of X has to be equal to NMAX.
C             X contains first the continuous, then the integer followed by
C             the binary variables.
C   F :       F contains the actual objective function value evaluated at X.
C   G(MMAX) :  G contains the actual constraint function values at X.
C   DF(NMAX) :  DF contains the gradient values of the objective function.
C   DG(MMAX,NMAX) :  DG contains the gradient values of constraints
C             in the first M rows. In the driving program, the row dimension
C             of DG has to be equal to MMAX.
C   U(MNN) :  On return, the first M locations contain
C             the multipliers of the M nonlinear constraints, the subsequent
C             N locations the multipliers subject to the lower bounds, and the
C             final N locations the multipliers subject to the upper bounds.
C             At an optimal solution, all multipliers with respect to
C             inequality constraints should be nonnegative.
C   XL(N),XU(N) :  On input, the one-dimensional arrays XL and XU must
C             contain the upper and lower bounds of the variables, first
C             for the continuous, then for the integer and subsequently for 
C             the binary variables.
C   B(NMAX,NMAX) :  On return, B contains the last computed approximation
C             of the Hessian matrix of the Lagrangian function.
C             In the driving program, the row dimension of C has to be equal
C             to NMAX. 
C   ACC :     The user has to specify the desired final accuracy
C             (e.g. 1.0D-7). The termination accuracy should not be smaller
C             than the accuracy by which gradients are computed. If ACC is
C             less or equal to zero, then the machine precision is computed
C             by MISQP and subsequently multiplied by 1.0D+4.
C   ACCQP :   The tolerance is needed for the QP solver to perform several
C             tests, for example whether optimality conditions are satisfied
C             or whether a number is considered as zero or not. If ACCQP is
C             less or equal to zero, then the machine precision is computed
C             by MISQP and subsequently multiplied by 1.0D+4.
C   MAXIT :   Maximum number of iterations, where one iteration corresponds to
C             one evaluation of a set of gradients (e.g. 100).
C   MAXPEN :  Maximum number of successive increments of the penalty parameter 
C             without success (e.g. 50).
C   MAXUND :  Maximum number of successive iterations without improvements
C             of the iterate X (e.g. 10).
C   RESOPT :  If set to TRUE and if integer variables exist, an additional 
C             restart will be performed to check whether an improvement 
C             is possible (recommended for functions with curved narrow valleys).
C   NONMON :  Maximum number of successive iterations, which are to be 
C             considered for the non-monotone trust region algorithm.
C   MAXNDE :  Maximum number of branch-and-bound steps for solving MIQP.
C   IPRINT :  Specification of the desired output level.
C          0 :  No output of the program.
C          1 :  Only a final convergence analysis is given.
C          2 :  One line of intermediate results is printed in each iteration.
C          3 :  More detailed information is printed for each iteration.
C          4 :  In addition, some messages of the QP solver are displayed.
C   MODE :   The parameter allows to change some default tolerances.
C          0 :   Use default values.
C          1 :   User-provided penalty parameter SIGMA, scaling constant DELTA, 
C                and initial trust region radii ITRC and ITRI for continuous
C                and integer variables in RW(1), RW(2), RW(3), and RW(4).
C                Default values are
C                  RW(1) = 10.0
C                  RW(2) = 0.01 
C                  RW(3) = 10.0
C                  RW(4) = 10.0
C   IOUT :    Integer indicating the desired output unit number, i.e., all
C             write-statements start with 'WRITE(IOUT,... '.
C   IFAIL :   The parameter shows the reason for terminating a solution
C             process. Initially IFAIL must be set to zero. On return IFAIL
C             could contain the following values:
C         -2 :   Compute new gradient values in DF and DG, see above.
C         -1 :   Compute new function values in F and G, see above.
C          0 :   Optimality conditions satisfied.
C          1 :   Termination after MAXIT iterations.
C          2 :   More than MAXUND iterations for fixing trust region.
C          3 :   More than MAXPEN updates of penalty parameter.
C          4 :   Termination at infeasible iterate.
C          5 :   Termination with zero trust region for integer variables.
C          6 :   Length of a working array is too short.
C          7 :   False dimensions, e.g., M$>$MMAX or N$>$=NMAX.
C        >90 :   QP solver terminated with an error message IFQL, 
C                IFAIL = IFQL + 100.
C   RW(LRW),LRW :  Real working array of length LRW, and LRW must be
C                  at least 5*NMAX*NMAX/2 + 7*(NINT+NBIN) + 23*N + 16*NMAX+ 
C                  6*MMAX + 10*M +5*ME + 3*MAXNDE + 2*NONMON + 104.
C   IW(LIW),LIW :  Integer working array of length LIW, where LIW must be 
C                  at least 5*NMAX + 6*MAXNDE + 2*(NINT+NBIN) + 27.
C   LW(LLW),LLW :  Logical working array of length LLW, where LLW must be
C                  at least 14+3*(NINT+NBIN).
C
C
C   FUNCTION AND GRADIENT EVALUATION:
C
C   The user has to provide functions and gradients in the same program, which
C   executes also misqp, according to the following rules:
C
C   1) Choose starting values for the variables to be optimized, and store
C      them in X.
C
C   2) Compute objective and all constraint function values values at X and
C      store them in F and G, respectively. 
C
C   3) Compute gradients of objective function and all constraints, and
C      store them in DF and DG, respectively. The j-th row of DG contains
C      the gradient of the j-th constraint, j=1,...,m. 
C
C   4) Set IFAIL=0 and execute MISQP.
C
C   5) If MISQP terminates with IFAIL=0, the internal stopping criteria are 
C      satisfied. 
C
C   6) In case of IFAIL>0, an error occurred.
C
C   7) If MISQP returns with IFAIL=-1, compute objective function values and
C      constraint values for all variables found in X, store them in F and G,
C      and call MISQP again. 
C
C   8) If MISQP terminates with IFAIL=-2, compute gradient values subject to
C      variables stored in X, and store them in DF and DG. Only partial 
C      derivatives subject to the continuous variables need to be provided. 
C      Then call MISQP again.
C
C
C
C
C   AUTHOR:     O. Exler
C   -------     University of Bayreuth
C               95440 Bayreuth
C               Germany
C
C
C
C   VERSION:    2.0 (12/2005) 
C   --------    2.1 (06/2007)
C
C
C***********************************************************************
      IMPLICIT NONE
      INTEGER I,J,M,ME,MME,MN,MNN,MNNQP,MMAX,N,N1,NMAX,NINT,NCONT,NBIN,
     /  IOUT,IFAIL,IPRINT,MODE,MAXIT,MAXPEN,MAXUND,MAXNDE,NONMON,
     /  INFUNC,INFUNG,INGRAD,IITER,INQL,INQL2,INODE,INUNDO,INPEN,IIFAIL,
     /  IINDEX,IIWMI1,LIWMI1,MAXNDS,
     /  IDINT,IFIRST,IHITL,IHITR,INEW,ISEC,ISEC1,ILOMI1,LLOMI1,
     /  IBD,ID,IDL,IDU,IDQL1,IDBD,IDFDDB,IDELTA,IDELT1,IDLA,IDLA1,
     /  IDMAXC,IDMAXI,IETA,IWF,IFEPS1,IFEPS2,IFOLD,IGEPS1,IGEPS2,IGOLD,
     /  IGQL2,ILAG,ILAG1,ILAG2,IKKT,IPNEW1,IPOLD,IPRED,IR,IR1,IRES,
     /  IRESN,IRESN1,ISIGMA,ISIGM1,ITRC,ITRI,IU,IUOLD,IUQL1,IXOLD,
     /  IWDERI,LWDERI,IDBDFA,IFASIG,IFASI1,IDIDEL,IDIDE1,IDITRC,IDITRI,
     /  ITRCFA,ITRCF1,ITRCF2,ITRIFA,ITRIF1,ITRIF2,IDEC1,IDEC2,IDEC3,
     /  IDEC4,IDEC5,IDEC6,IDEC7,IDEC8,IDEC9,IDEC10,IEPSM,IWMI1,LWMI1,
     /  IW,LIW,LLW,LRW,IFITER,IRESIT,IBESTX,IBESTF,IBESTG,
     /  IBESTR,IREST,ISTART,ISCALE,IRSC,ICURVE,IXREST,IDREST,
     /  ISCUB,INTBIN,ITRB,IDMAXB,IBESTU,IBADD,IRELCOU,ISCAL,
     /  IADDWS,IOPTNS(20),
C
C MATLAB!!!!! / maximum variables = 1000 , maximum constraints = 1000  !!!!!
C
     /  IWMIQL,LLWMIQ,LIWMIQ,LRWMIQ
            
C
C MATLAB!!!!!!
C
C
      DIMENSION        IW(LIW),IWMIQL(1510000)
      DOUBLE PRECISION X,XL,XU,F,G,DF,DG,U,B,ACC,ACCQP,RW,XINT,
     /                 ZERO,ONE,DNINT,PEN,RESVIO,
     /                 MAXIM,	
C
C MATLAB!!!!!
C
     /                 RWMIQL            
C
C MATLAB!!!!!!
C
      DIMENSION        X(NMAX),XL(NMAX),XU(NMAX),G(MMAX),DF(NMAX),
     /                 DG(MMAX,NMAX),U(MNN),B(NMAX,NMAX),RW(LRW),
     /                 RWMIQL(3300000)
      LOGICAL          LW,RESOPT,
C
C MATLAB!!!!!
C
     /                 LWMIQL
C
C MATLAB!!!!!!
C
      DIMENSION        LW(LLW),LWMIQL(3100)
      INTRINSIC        DNINT
      EXTERNAL         MISQP1,MISQP2
C
C MATLAB
C
      LLWMIQ=3100
      LIWMIQ=1510000
      LRWMIQ=3300000
C
C MATLAB
C

C
C	INITIALIZE VARIABLES THAT MAY BE UNINITIALIZED OTHERWISE
C

	IWF    = 0
	IFEPS1 = 0
	IFEPS2 = 0
	IGEPS1 = 0
	IGEPS2 = 0
	ILAG   = 0
	ILAG1  = 0
	ILAG2  = 0
      

C
C SET SOME CONSTANTS
C
      ZERO    = 0.0D+0
      ONE     = 1.0D+0
	INTBIN  = NINT+NBIN
      NCONT   = N-INTBIN
      MME     = M+ME
      N1      = N+1
      MNNQP   = MME+N1+N1

      MAXNDS=MAXNDE
	IF (INTBIN.EQ.0) MAXNDS=0
C
C     MIQLB4 OPTIONS
C
      IOPTNS(1)    = 1
      IOPTNS(2)    = 1
      IOPTNS(3)    = MAXNDS
      IOPTNS(4)    = 1000
      IOPTNS(5)    = 1
      IOPTNS(6)    = 0   

C
C MIQP SOLVER
C
 

C
C INITIAL ADDRESSES IN IW (1+13+NINT+NBIN)
C
      INFUNG = INTBIN+1
      IIFAIL = INFUNG+1
      IINDEX = IIFAIL+1
      INFUNC = IINDEX+1
      INGRAD = INFUNC+1
      IITER  = INGRAD+1
      INQL   = IITER+1
      INQL2  = INQL+1
      INODE  = INQL2+1
      INUNDO = INODE+1
      INPEN  = INUNDO+1
	IRSC   = INPEN+1
	IRELCOU= IRSC+1
	IIWMI1 = IRELCOU+1
      LIWMI1 = LIW-IIWMI1+1
C
C INITIAL ADDRESSES IN LW (11  +   1)
C
      INEW   = 1
      ISEC   = INEW+1
	IDINT  = ISEC+1
      IFIRST = IDINT+1
      ISEC1  = IFIRST+1
      IHITL  = ISEC1+1
      IHITR  = IHITL+1
	IREST  = IHITR+1
	ISTART = IREST+1
	ICURVE = ISTART+1
	ISCAL  = ICURVE+1
      ILOMI1 = ISCAL+1
      LLOMI1 = LLW-ILOMI1+1
C
C INITIAL ADDRESSES IN RW (72 + MMAX + 9*M + 4*ME + 21*N + 2*NONMON)
C  
      ISIGMA = 1
      IDELTA = ISIGMA+1
      ITRC   = IDELTA+1
      ITRI   = ITRC+1
	ITRB   = ITRI+1
      IU     = ITRB+1      
      ID     = IU+MNNQP
      IDL    = ID+N1
      IDU    = IDL+N1
      IDQL1  = IDU+N1
      IBD    = IDQL1+N1
      IDBD   = IBD+N1 
      IDFDDB = IDBD+1
      IDLA   = IDFDDB+1
      IDLA1  = IDLA+N1
      IDMAXC = IDLA1+N1
      IDMAXI = IDMAXC+1
      IDMAXB = IDMAXI+1
      IETA   = IDMAXB+1
      IFOLD  = IETA+N1
      IGOLD  = IFOLD+1
      IGQL2  = IGOLD+MAX0(M,1)
      IKKT   = IGQL2+MMAX
      IPNEW1 = IKKT+1
      IPOLD  = IPNEW1+1
      IPRED  = IPOLD+1
      IR     = IPRED+1
      IR1    = IR+1
      IRESN  = IR1+1
      IRESN1 = IRESN+1
      IRES   = IRESN1+1
      IUOLD  = IRES+1
      IUQL1  = IUOLD+MNNQP
      IXOLD  = IUQL1+MNNQP
      ISIGM1 = IXOLD+N
      IDELT1 = ISIGM1+1
      IDBDFA = IDELT1+1
      IFASIG = IDBDFA+1
      IFASI1 = IFASIG+1
      IDIDEL = IFASI1+1
      IDIDE1 = IDIDEL+1
      ITRCFA = IDIDE1+1
      ITRCF1 = ITRCFA+1
      ITRCF2 = ITRCF1+1
      ITRIFA = ITRCF2+1
      ITRIF1 = ITRIFA+1
      ITRIF2 = ITRIF1+1
      IDITRC = ITRIF2+1
      IDITRI = IDITRC+1
      IDEC1  = IDITRI+1
      IDEC2  = IDEC1+1
      IDEC3  = IDEC2+1
      IDEC4  = IDEC3+1
      IDEC5  = IDEC4+1
      IDEC6  = IDEC5+1
      IDEC7  = IDEC6+1
      IDEC8  = IDEC7+1
      IDEC9  = IDEC8+1
      IDEC10 = IDEC9+1
      IEPSM  = IDEC10+1
	IFITER = IEPSM+1
	IRESIT = IFITER+MAX0(NONMON,1)
	IBESTX = IRESIT+MAX0(NONMON,1)
	IBESTF = IBESTX+N
	IBESTG = IBESTF+1
	IBESTR = IBESTG+MAX0(M,1)
	IBESTU = IBESTR+1
	IXREST = IBESTU+MNNQP
	IDREST = IXREST+N
	ISCALE = IDREST+N
	ISCUB  = ISCALE+MAX0(NCONT,1)
	IBADD  = ISCUB+1
      IADDWS = IBADD+MAX0(1,INTBIN)
      IWDERI = IADDWS+NMAX+MMAX      
C      WRITE(*,*)'N,M',N,M,NMAX,MMAX

C
C SPACE FOR DERIVATIVES CALCULATION INTEGER (SUBROUTINE MISQP2)
C
      IF (INTBIN.GT.0) THEN
         IWF    = IWDERI+MAX0(1,M)
         IGEPS1 = IWF+1
         IFEPS1 = IGEPS1+MAX0(1,M)
         IGEPS2 = IFEPS1+1
         IFEPS2 = IGEPS2+MAX0(1,M)
         ILAG   = IFEPS2+1
         ILAG1  = ILAG+1
         ILAG2  = ILAG1+1
         LWDERI = 3*M + 6
      ELSE
         LWDERI = 0
      END IF
      IWMI1 = IWDERI+LWDERI
      LWMI1 = LRW-IWMI1+1
C
      IF (IFAIL.NE.0) GOTO 100
C
C CHECK DIMENSIONS
C
      IF (INTBIN.GT.N.OR.N1.GT.NMAX.OR.MME.GT.MMAX.OR.MNN.NE.M+N+N)THEN
         IFAIL = 7
         IF (IPRINT.EQ.0) RETURN
         WRITE(IOUT,900)
         RETURN
      ENDIF
C
C CHECK WORKING ARRAY LENGTH
C
C - INTEGER
C
c      I = IIWMI1-1+NMAX+INTBIN+6*MAXNDS+13+4*NMAX
C
C MATLAB!!!!!!!
C
      I = IIWMI1-1
C
C MATLAB!!!!!!!
C
      IF (I.GT.LIW) THEN
         IFAIL = 6
         IF (IPRINT.EQ.0) RETURN
         WRITE(IOUT,905) I
         RETURN
      ENDIF
C
C - LOGICAL
C
C      I = ILOMI1+3*INTBIN+2
C
C MATLAB!!!!!!!
C
      I = ILOMI1
C
C MATLAB!!!!!!!
C
      IF (I.GT.LLW) THEN
         IFAIL = 6
         IF (IPRINT.EQ.0) RETURN
         WRITE(IOUT,910) I
         RETURN
      ENDIF
C
C - REAL
C
C      I = IWMI1-1+5*NMAX*NMAX/2+7*INTBIN+15*NMAX+4*MMAX+3*MAXNDS+24
C
C MATLAB!!!!!!!
C
      I = IWMI1-1 
C
C MATLAB!!!!!!!
C
      IF (I.GT.LRW) THEN
         IFAIL = 6
         IF (IPRINT.EQ.0) RETURN
         WRITE(IOUT,915) I
         RETURN
      ENDIF
C
C INITIALIZE PARAMETERS
C
C - FACTOR / DIVISOR
      RW(IFASIG) = 1.1D+1
      RW(IFASI1) = 2.0D+0
      RW(IDIDEL) = 1.1D+1
      RW(IDIDE1) = 4.0D+0
      RW(ITRCFA) = 2.0D+0
      RW(ITRCF1) = 4.0D+0
      RW(ITRCF2) = 2.0D+0
      RW(ITRIFA) = 2.0D+0
      RW(ITRIF1) = 4.0D+0
      RW(ITRIF2) = 2.0D+0
      RW(IDITRC) = 2.0D+0
      RW(IDITRI) = 2.0D+0
C
C - DECISION-PARAMETERS
C
C VERY GOOD STEP 1: R>DEC1 => TAKE STEP 1 => EXPAND TRUST
      RW(IDEC1) =0.75D0
C
C BAD STEP 1: R<DEC2 => TRY STEP 2
      RW(IDEC2) =0.25D0
C
C AVERAGE STEP 1: DEC2<R<DEC1 => TAKE STEP 1
C BUT STEP 2 VERY GOOD:  DEC3<R1<DEC4 => EXPAND TRUST
      RW(IDEC3) =0.9D0
      RW(IDEC4) =1.1D0
C
C IF R1>DEC5: TRY STEP 2, ELSE: REDUCE TRUST, TAKE STEP 1
      RW(IDEC5) =0.75D0
C
C IF (PNEW STEP2) < (PNEW STEP1) => TAKE STEP 2 !!!
C GOOD STEP 2: R>DEC6 => EXPAND TRUST
      RW(IDEC6) =0.75D0
C
C BAD STEP 2: R<DEC7 => REDUCE TRUST, BUT PERHAPS TAKE STEP 2
      RW(IDEC7) = 0.25D0
C
C VERY GOOD STEP 1/2: R>DEC8 => EXPAND TRUST MORE
C (DEC8: CONT/ DEC9: INT)
      RW(IDEC8) = 0.9D0
      RW(IDEC9) = 0.9D0
C
C IF R>DEC10: STEP WILL BE ACCEPTED
      RW(IDEC10) = 0.0D0
C
C - BFGS-PARAMETER
C
      RW(IDBDFA) = 2.0D-1
C
C COMPUTE MACHINE PRECISION
C
      RW(IEPSM) = 0.000001D0
   10 IF (1.0D0+RW(IEPSM).GT.1.0D0) THEN
         RW(IEPSM) = RW(IEPSM)*0.5D0
         GOTO 10
      END IF    
      RW(IEPSM) = RW(IEPSM)*2.0D0
      IF (ACC.LE.RW(IEPSM)) ACC = RW(IEPSM)*1.0D+4
      IF (ACCQP.LE.RW(IEPSM)) ACCQP = RW(IEPSM)*1.0D+4
C
C FIRST OUTPUT 
C
      IF (IPRINT.EQ.0) GOTO 50
      WRITE(IOUT,1000) MODE,ACC,ACCQP,MAXIT,MAXPEN,MAXUND,RESOPT,NONMON,
     /                 MAXNDS,IPRINT,NCONT,NINT,NBIN,M,ME
      IF (IPRINT.NE.2) GOTO 50
      IF (INTBIN.EQ.0) THEN
         WRITE(IOUT,1009)	   
      ELSE IF (INTBIN.EQ.N) THEN
         WRITE(IOUT,1010)
      ELSE
         WRITE(IOUT,1020)
      END IF
   50 CONTINUE  
C
C SET FLAGS FOR START -> CHANGES B IN BINARY VARIABLES
C
 	LW(ISTART)=.TRUE.
C
C RESTART: CURVE RECOVERY
C
	LW(IREST)=.FALSE.
	IW(IRSC)=0
C
C SCALING
C
	MAXIM=ZERO
      DO I=1,NCONT
	   MAXIM=DMAX1(MAXIM,(XU(I)-XL(I)))
	END DO    
      LW(ISCAL)=.FALSE.
C      goto 52
      IF (NCONT.EQ.0.OR.INTBIN.EQ.0.OR.MAXIM.LE.RW(IEPSM)) GOTO 52
      LW(ISCAL)=.TRUE.
      RW(ISCUB)=1.0D0
   51 RW(ISCUB)=RW(ISCUB)*1.0D+1
      IF (MAXIM/RW(ISCUB).GT.1.0D+3) GOTO 51
      IF (.NOT.LW(ISCAL)) GOTO 52
      DO I=1,NCONT
         RW(ISCALE+I-1)=(XU(I)-XL(I))/RW(ISCUB)
      END DO
C
C BEST POINT -> STORED BECAUSE OF NONMONOTONE SEARCH
C
   52 RW(IBESTF) = F
      RW(IBESTR) = ZERO
      DO I=1,ME
         RW(IBESTR)=DMAX1(RW(IBESTR),DABS(G(I)))
      END DO
      RW(IBESTR)=- RW(IBESTR)
      DO I=ME+1,M
        IF (G(I).LT.RW(IBESTR)) RW(IBESTR)=G(I)
      END DO
      RW(IBESTR)=DABS(RW(IBESTR))
	DO I=1,M
         RW(IBESTG+I-1)=G(I)
	END DO
	IF (LW(ISCAL)) THEN
	   DO I=1,NCONT
            RW(IBESTX+I-1)=(X(I)-XL(I))/RW(ISCALE+I-1)
	   END DO
	ELSE
	   DO I=1,NCONT
            RW(IBESTX+I-1)=X(I)
	   END DO
      END IF
	DO I=NCONT+1,N
         RW(IBESTX+I-1)=X(I)
	END DO
	DO I=0,MNNQP-1
         RW(IBESTU+I)=ZERO
	END DO
C
C MODE UPDATES
C
      IF (MODE.LT.2 .OR. (MODE.GE.4 .AND. MODE.LT.6)) THEN
C
C INITIAL HESSIAN (CONT: I , INT: SET BY MISQP2)
C AND MULTIPLIERS U
C
         DO I=1,N1
            DO J=I+1,N1
               B(J,I) = ZERO
               B(I,J) = B(J,I)
            END DO
            B(I,I) = ONE
         END DO
         B(N1,N1) = ACC
         DO I=0,MNNQP-1
            RW(IU+I) = ZERO
	      RW(IUOLD+I) = ZERO
         END DO
      ELSE        
         MN=M+N
         DO I=1,M
            RW(IU+I-1) = U(I)
         END DO
         DO I=M+1,MME
            RW(IU+I-1) = ZERO
         END DO
         J = MME
         DO I=M+1,MNN
            RW(IU+J) = U(I)
            IF (I.EQ.MN) THEN
               J=J+1
               RW(IU+J) = ZERO
           END IF
           J=J+1
        END DO
        RW(IU+J) = ZERO
      END IF
      IF (MODE.EQ.0) THEN
         RW(ISIGMA) = 1.0D+1
         RW(IDELTA) = 1.0D-2
         RW(ITRI) = 1.0D+1
         RW(ITRC) = 1.0D+1
	   RW(ITRB) = ONE
      END IF
C      
      IW(INFUNC) = 1
      IW(INGRAD) = 1
C
C CHECK AND CORRECT INITIAL POINT
C
      DO I=NCONT+1,NCONT+NINT
         XL(I)=DNINT(XL(I))
         XU(I)=DNINT(XU(I))
	END DO
	IF (NCONT+NINT.LT.N) THEN
         DO I=NCONT+NINT+1,N
            XL(I)=ZERO
	      XU(I)=ONE
	   END DO
	ENDIF   

      LW(INEW)   = .FALSE.
      DO I=1,N
         IF (X(I).LT.XL(I)) THEN
            LW(INEW)=.TRUE.
            X(I) = XL(I)
	   END IF
         IF (X(I).GT.XU(I)) THEN
            LW(INEW)=.TRUE.
            X(I) = XU(I)
         END IF
      END DO
C
C INTEGER CONTROL
C
      IF (INTBIN.EQ.0) GOTO 54
      IW(INFUNG) = 0
      IW(IINDEX) = NCONT + 1
      LW(IDINT)  = .FALSE.
      LW(IFIRST) = .TRUE.
C      LW(INEW)   = .FALSE.
C
C CHECK IF STARTING VALUES ARE INTEGER 
C
      J=1
      DO I=NCONT+1,N
         XINT = DNINT(X(I))
         IF (DABS(XINT-X(I)).GT.RW(IEPSM)) LW(INEW)=.TRUE.
         IF (XINT.GT.XU(I)+RW(IEPSM)) THEN
            X(I) = XINT - ONE
         ELSE IF (XINT.LT.XL(I)-RW(IEPSM)) THEN
            X(I) = XINT + ONE
         ELSE
            X(I) = XINT
         END IF
         IW(J)=I
         J=J+1  
      END DO      
   54 IF (LW(INEW)) THEN
         IF (IPRINT.NE.0) WRITE(IOUT,920)
         IW(INFUNC) = 2
         IFAIL = -1
         RETURN
C	   GOTO 9999
      END IF
C***********************************************************************
  100 CONTINUE
      IF (.NOT.LW(ISCAL)) GOTO 101
      DO I=1,NCONT
         X(I) = (X(I)-XL(I))/RW(ISCALE+I-1)
      END DO
      IF (IFAIL.EQ.-2.OR.IFAIL.EQ.0) THEN
         DO I=1,NCONT
            DF(I)=DF(I)*RW(ISCALE+I-1)
	      DO J=1,M
               DG(J,I)=DG(J,I)*RW(ISCALE+I-1)
            END DO
	   END DO
      END IF
C
C CONSTRAINT VIOLATION
C
C AENDERN: MUSS NICHT IMMER BERECHNET WERDEN!!!
C
  101 PEN = F
      RESVIO = ZERO
      IF (M.EQ.0) GOTO 110
      DO I=1,ME
         RESVIO=DMAX1(RESVIO,DABS(G(I)))
      END DO
      RESVIO=-RESVIO
      DO I=ME+1,M
        IF (G(I).LT.RESVIO) RESVIO=G(I)
      END DO
      RESVIO=DABS(RESVIO)
      PEN = PEN + RW(ISIGMA)*RESVIO
C
C 14 12 2005
C
  110 IF (IFAIL.EQ.-1.OR.IFAIL.EQ.0) THEN
         IF (LW(INEW).OR.
     /       (RW(IBESTR).GT.ACC.AND.RESVIO.LT.RW(IBESTR)).OR.
     /       (RW(IBESTR).LE.ACC.AND.RESVIO.LE.ACC.AND.
     /        F.LT.RW(IBESTF))) THEN

            IF (LW(IREST)) THEN
C
C RESTART SUCCESSFUL
C
               IF ((RW(IBESTR).GT.ACC.AND.RESVIO.LT.RW(IBESTR)).OR.
     /            (RW(IBESTR).LE.ACC.AND.RESVIO.LE.ACC.AND.
     /            RW(IBESTF)-F.GT.ACC)) THEN               
                  IW(IRSC)=0
	         END IF
            END IF
            RW(IBESTF)=F
            RW(IBESTR)=RESVIO
	      DO I=1,M
               RW(IBESTG+I-1)=G(I)
	      END DO
	      DO I=1,N
               RW(IBESTX+I-1)=X(I)
	      END DO
		   DO I=0,MNNQP-1
               RW(IBESTU+I)=RW(IU+I)
	      END DO
	   ENDIF
	ENDIF


C
C CALCULATE VALUES AFTER INTEGER CHANGED
C
      IF (LW(INEW)) THEN
         IF (IFAIL.EQ.-1) THEN
            IW(INGRAD)=2
            IFAIL=-2
            IF (LW(ISCAL)) GOTO 9999
	      RETURN
         END IF
         LW(INEW)=.FALSE.
         IFAIL = 0
      END IF
      IF (INTBIN.EQ.0) GOTO 200
C
C INTEGER DERIVATIVES
C
      IF (IFAIL.EQ.-2.OR.IFAIL.EQ.0.OR.LW(IDINT)) THEN
         IF (.NOT.LW(IDINT)) THEN
            LW(IDINT)=.TRUE.
            IW(IIFAIL) = IFAIL
         END IF
C
C CALL MISQP2 -> CALCULATES INTEGER DERIVATIVES
C
c      write(iout,*)'badd vor call',(RW(IBADD+I),I=1,n-ncont)

         CALL MISQP2(M,ME,MME,MMAX,N,N1,NMAX,X,F,G,DF,DG,
     /        XL,XU,B,RW(IEPSM),IW(IINDEX),RW(IWDERI),RW(IWF),
     /        RW(IGEPS1),RW(IFEPS1),RW(IGEPS2),RW(IFEPS2),
     /        LW(IDINT),LW(IFIRST),LW(ISEC1),
     /        LW(IHITL),LW(IHITR),ACC,LW(ISTART),NCONT,
     /        RW(IUOLD),RW(ILAG),RW(ILAG1),RW(ILAG2),RW(IBADD),
     /        NINT,RW(IDLA))
c	write(iout,*)'++++++++++++badd+++++++++++++++++'      
c      write(iout,*)'badd',(RW(IBADD+I),I=1,n-ncont)

         IF (LW(IDINT)) THEN
C
C COUNT FUNCTION CALLS IN MISQP2
C
            IW(INFUNG) = IW(INFUNG)+1
            IFAIL = -1 
	      GOTO 9999
         END IF
         IFAIL = IW(IIFAIL)
         IW(IINDEX) = NCONT+1
         LW(IFIRST) = .TRUE.
	   LW(ISTART) = .FALSE.

	   PEN = F
         RESVIO = ZERO
         IF (M.EQ.0) GOTO 200
         DO I=1,ME
            RESVIO=DMAX1(RESVIO,DABS(G(I)))
         END DO
         RESVIO=-RESVIO
         DO I=ME+1,M
           IF (G(I).LT.RESVIO) RESVIO=G(I)
         END DO
         RESVIO=DABS(RESVIO)
         PEN = PEN + RW(ISIGMA)*RESVIO

      END IF
  200 CONTINUE
C
C CALL MISQP1
C
C  210 CONTINUE
      CALL MISQP1(M,ME,MME,MMAX,N,N1,NMAX,MNNQP,X,F,G,DF,DG,
     /     RW(ISIGMA),RW(IDELTA),RW(ITRC),RW(ITRI),RW(ITRB),RW(IU),
     /     XL,XU,NCONT,NINT,NBIN,IW,B,ACC,ACCQP,ACCQP,MAXIT,MAXPEN,
     /     MAXUND,IW(INODE),IPRINT,IOUT,IFAIL,
     /     IW(INFUNC),
     /     IW(INGRAD),IW(IITER),IW(INQL),IW(INQL2),IW(INUNDO),IW(INPEN),
     /     RW(ID),RW(IDL),RW(IDU),RW(IDQL1),RW(IBD),RW(IDBD),RW(IDFDDB),
     /     RW(IDLA),RW(IDLA1),RW(IDMAXC),RW(IDMAXI),RW(IDMAXB),RW(IETA),
     /     RW(IFOLD),
     /     RW(IGOLD),RW(IGQL2),RW(IKKT),RW(IPNEW1),RW(IPOLD),RW(IPRED),
     /     RW(IR),RW(IR1),RW(IRESN),RW(IRESN1),RW(IRES),
     /     RW(IUOLD),RW(IUQL1),RW(IXOLD),RW(IEPSM),RW(ISIGM1),
     /     RW(IDELT1),RW(IDBDFA),RW(IFASIG),RW(IFASI1),
     /     RW(IDIDEL),RW(IDIDE1),RW(ITRCFA),RW(ITRCF1),RW(ITRCF2),
     /     RW(ITRIFA),RW(ITRIF1),RW(ITRIF2),RW(IDITRC),RW(IDITRI),
     /     RW(IDEC1),RW(IDEC2),RW(IDEC3),RW(IDEC4),RW(IDEC5),
     /     RW(IDEC6),RW(IDEC7),RW(IDEC8),RW(IDEC9),RW(IDEC10),
     /     LW(ISEC),RWMIQL,LRWMIQ,IWMIQL,LIWMIQ,
     /     LWMIQL,LLWMIQ,RW(IFITER),RW(IRESIT),PEN,RESVIO,LW(IREST),
     /     LW(ICURVE),LW(ISCAL),IW(IRSC),RW(IXREST),RW(IDREST),
     /     RW(ISCUB),RW(IBESTR),INTBIN,RW(IBADD),IW(IRELCOU),
     /     NONMON,RESOPT,IOPTNS)
C
 9999 IF (IFAIL.GE.0) THEN
         GOTO 999
	END IF
      IF (LW(ISCAL)) THEN
         DO I=1,NCONT
	      X(I) = XL(I)+RW(ISCALE+I-1)*X(I)
         END DO
	  END IF
      IF (IFAIL.LT.0) RETURN

  999 CONTINUE
C DA KOENNEN NATUERLICH DIE MULTIPLIKATOREN NICHT STIMMEN!!
C EVTL AUCH ABSPEICHERN
      IF (LW(ISCAL)) THEN
         DO I=1,NCONT
            X(I) = XL(I)+RW(ISCALE+I-1)*RW(IBESTX+I-1)
         END DO
	ELSE
         DO I=1,NCONT
            X(I) = RW(IBESTX+I-1)
         END DO
      END IF
      DO I=NCONT+1,N
         X(I) = RW(IBESTX+I-1)
      END DO
      F=RW(IBESTF)
	DO I=1,M
         G(I)=RW(IBESTG+I-1)
	END DO
C
C SET MULTIPLIERS U
C
      MN=M+N
      DO I=1,ME
        U(I) = RW(IBESTU+I-1) - RW(IBESTU+M+I-1)
      END DO
      DO I=ME+1,M
        U(I) = RW(IBESTU+I-1)
      END DO
      DO I=M+1,MN
        U(I) = RW(IBESTU+I+ME-1)
      END DO
      DO I=MN+1,MNN
        U(I) = RW(IBESTU+I+ME)
      END DO

C      MN=M+N
C      DO I=1,ME
C        U(I) = RW(IUOLD+I-1) - RW(IUOLD+M+I-1)
C      END DO
C      DO I=ME+1,M
C        U(I) = RW(IUOLD+I-1)
C      END DO
C      DO I=M+1,MN
C        U(I) = RW(IUOLD+I+ME-1)
C      END DO
C      DO I=MN+1,MNN
C        U(I) = RW(IUOLD+I+ME)
C      END DO
      IF (IPRINT.EQ.0) RETURN
C
C FINAL OUTPUT
C
      WRITE(IOUT,1100) F,(X(I),I=1,N)
      WRITE(IOUT,1110) (U(I),I=1,MNN)
      IF (M.NE.0) WRITE(IOUT,1120) (G(I),I=1,M)
      WRITE(IOUT,1130) (XL(I)-X(I),I=1,N)
      WRITE(IOUT,1140) (XU(I)-X(I),I=1,N)
      IF (INTBIN.EQ.0) THEN
         WRITE(IOUT,1150) IW(INFUNC)
      ELSE
         WRITE(IOUT,1150) IW(INFUNC) + IW(INFUNG)
         WRITE(IOUT,1151) IW(INFUNC),IW(INFUNG)
      END IF
      WRITE(IOUT,1160) IW(INGRAD)
      WRITE(IOUT,1170) IW(INQL)
      WRITE(IOUT,1171) IW(INQL2)
      WRITE(IOUT,1172) IW(INODE)
      WRITE(IOUT,1180) IFAIL 
C***********************************************************************
C
C FORMAT DEFINITIONS
C
  900 FORMAT('   *** ERROR 7: WRONG DIMENSIONS')
  905 FORMAT('   *** ERROR 6: LENGTH OF IW TOO SMALL, MUST BE AT LEAST',
     /       I8)
  910 FORMAT('   *** ERROR 6: LENGTH OF LW TOO SMALL, MUST BE AT LEAST',
     /       I8)
  915 FORMAT('   *** ERROR 6: LENGTH OF RW TOO SMALL, MUST BE AT LEAST',
     /       I8)
  920 FORMAT('   *** WARNING: STARTING POINT SET TO INTEGER VALUES')
 1000 FORMAT(//,'   ---------------------------------------------',/,
     /          '     START OF THE TRUST REGION ALGORITHM MISQP',/,
     /          '   ---------------------------------------------',//,
     /  '   PARAMETERS: ',/,
     /  '      MODE   = ',I5,/,
     /  '      ACC    = ',G16.8,/,
     /  '      ACCQP  = ',G16.8,/,
     /  '      MAXIT  = ',I5,/,
     /  '      MAXPEN = ',I5,/,
     /  '      MAXUND = ',I5,/,
     /  '      RESOPT = ',L5,/,
     /  '      NONMON = ',I5,/,
     /  '      MAXNDS = ',I8,/,
     /  '      IPRINT = ',I5,/,
     /  '      NCONT  = ',I5,/,
     /  '      NINT   = ',I5,/,
     /  '      NBIN   = ',I5,/,
     /  '      M      = ',I5,/,
     /  '      ME     = ',I5,//)
 1009 FORMAT('   OUTPUT IN THE FOLLOWING ORDER:',/,
     /       '      IT    - ITERATION NUMBER',/,
     /       '      F     - OBJECTIVE FUNCTION VALUE',/,
     /       '      MCV   - MAXIMUM CONSTRAINT VIOLATION',/,
     /       '      SIGMA - PENALTY PARAMETER',/,
     /       '      TRUST - TRUST REGION RADIUS',/,
     /       '      KKT   - MAXIMUM NORM OF KKT RESIDUAL',//,
     / '  IT       F           MCV      SIGMA     TRUST',
     /               '      KKT',/,
     / ' --------------',
     / '---------------------------------------')
 1010 FORMAT('   OUTPUT IN THE FOLLOWING ORDER:',/,
     /       '      IT    - ITERATION NUMBER',/,
     /       '      F     - OBJECTIVE FUNCTION VALUE',/,
     /       '      MCV   - MAXIMUM CONSTRAINT VIOLATION',/,
     /       '      SIGMA - PENALTY PARAMETER',/,
     /       '      TRUSTI - TRUST REGION RADIUS OF INTEGER ',
     /                                                    'VARIABLES',/,
     /       '      TRUSTB - TRUST REGION RADIUS OF BINARY ',
     /                                                    'VARIABLES',/,
     /       '      KKT   - MAXIMUM NORM OF KKT RESIDUAL',//,
     / '  IT       F           MCV      SIGMA     TRUSTI',
     /               '    TRUSTB     KKT',/,
     / ' -------------------------',
     / '---------------------------------------')
 1020 FORMAT('    OUTPUT IN THE FOLLOWING ORDER:',/,
     /       '       IT     - ITERATION NUMBER',/,
     /       '       F      - OBJECTIVE FUNCTION VALUE',/,
     /       '       MCV    - MAXIMUM CONSTRAINT VIOLATION',/,
     /       '       SIGMA  - PENALTY PARAMETER',/,
     /       '       TRUSTC - TRUST REGION RADIUS OF CONTINUOUS ',
     /                                                    'VARIABLES',/,
     /       '       TRUSTI - TRUST REGION RADIUS OF INTEGER ',
     /                                                    'VARIABLES',/,
     /       '       TRUSTB - TRUST REGION RADIUS OF BINARY ',
     /                                                    'VARIABLES',/,
     /       '       KKT    - MAXIMUM NORM OF KKT RESIDUAL',//,
     / '  IT       F           MCV      SIGMA     TRUSTC',
     /               '    TRUSTI    TRUSTB     KKT',/,
     / ' ---------------------------------------',
     / '---------------------------------------')
 1100 FORMAT(/, /, '   --- FINAL CONVERGENCE ANALYSIS ---', /, /,
     /       '      OBJECTIVE FUNCTION VALUE:        F(X)  =', D16.8, /,
     /       '      APPROXIMATION OF SOLUTION:       X     =',/,
     /                                                    (9X,4D16.8))
 1110 FORMAT('      APPROXIMATION OF MULTIPLIERS:    U     =',/,
     /                                                    (9X,4D16.8))
 1120 FORMAT('      CONSTRAINT FUNCTION VALUES:      G(X)  =',/,
     /                                                    (9X,4D16.8))
 1130 FORMAT('      DISTANCE FROM LOWER BOUND:       XL-X  =',/,
     /                                                    (9X,4D16.8))
 1140 FORMAT('      DISTANCE FROM UPPER BOUND:       XU-X  =',/,
     /                                                    (9X,4D16.8))
 1150 FORMAT('      NUMBER OF FUNCTION CALLS:        NFUNC =',I5)
 1151 FORMAT('       - CALLS WITHIN TR METHOD:       NF_TR =',I5,
     /     /,'       - CALLS FOR INT-DERIVATIVES:    NF_2D =',I5)
 1160 FORMAT('      NUMBER OF GRADIENT CALLS:        NGRAD =',I5)
 1170 FORMAT('      NUMBER OF CALLS OF QP SOLVER:    NQL   =',I5)
 1171 FORMAT('       - SECOND ORDER CORRECTIONS:     NQL2  =',I5)
 1172 FORMAT('      NUMBER OF B&B-NODES:             NODES =',I8)
 1180 FORMAT('      TERMINATION REASON:              IFAIL =',I5,/,/)
C***********************************************************************
      END
C***********************************************************************
C END SUBROUTINE MISQP
C***********************************************************************
C
      SUBROUTINE MISQP1(M,ME,MME,MMAX,N,N1,NMAX,MNNQP,X,F,G,DF,DG,
     /  SIGMA,DELTA,TRUSTC,TRUSTI,TRUSTB,U,XL,XU,NCONT,NINT,NBIN,IND,B,
     /  ACC,ACCINT,ACCQP,MAXIT,MAXPEN,MAXUND,NODES,IPRINT,
     /  IOUT,IFAIL,NFUNC,NGRAD,ITER,NQL,NQL2,NUNDO,NPENUP,D,DL,DU,
     /  DQL1,BD,DBD,DFDDBD,
     /  DLA,DLAOLD,DMAXC,DMAXI,DMAXB,ETA,FOLD,GOLD,GQL2,KKT,PNEW1,POLD,
     /  PRED,R,R1,RESNEW,RESNE1,RESOLD,UOLD,UQL1,XOLD,EPSM,
     /  SIGMA1,DELTA1,DBDFAC,FACSIG,FACSI1,DIVDEL,DIVDE1,
     /  TRCFAC,TRCFA1,TRCFA2,TRIFAC,TRIFA1,TRIFA2,DIVTRC,DIVTRI,
     /  DEC1,DEC2,DEC3,DEC4,DEC5,DEC6,DEC7,DEC8,DEC9,DEC10,
     /  SEC,W,LW,IWAR,LIWAR,LOGW,LLOGW,FITER,RESITER,PEN,RESVIO,
     /  RESTART,CURVE,SCALE,RSC,XREST,DREST,SCUB,BESRES,INTBIN,
     /  BADD,RELCOU,NONMON,RESOPT,IOPTNS)
C***********************************************************************
C
C SUBROUTINE MISQP1
C
C BASIC SUBROUTINE TO SOLVE NONLINEAR PROGRAMMING PROBLEMS
C
C***********************************************************************
      IMPLICIT NONE
      INTEGER I,J,M,ME,MME,MMEN1,MNNQP,MMAX,N,N1,NBFGS,NMAX,NCONT,
     /  NINT,NBIN,IND,IFAIL,IFAIL1,IPRINT,IOUT,MAXIT,MAXPEN,
     /  MAXUND,ITER,NFUNC,NGRAD,NUNDO,NPENUP,NQL,NQL2,
     /  LW,IWAR,LIWAR,LLOGW,RSC,RELCOU,NULL,INTBIN,NODES,NONMON,
     /  IOPTNS(20),QLPRNT
      DIMENSION IND(INTBIN),IWAR(LIWAR)
      DOUBLE PRECISION X,XL,XU,F,G,DF,DG,U,B,ACC,ACCQP,ACCINT,
     /  D,DQL1,DL,DU,DELTA,DELTA1,DLA,DLAOLD,FQP,FOLD,GOLD,GQL2,KKT,
     /  PNEW,PNEW1,POLD,PRED,R,R1,RESNEW,RESNE1,RESOLD,SIGMA,SIGMA1,
     /  UOLD,UQL1,XOLD,EPSM,FQPREL,
     /  BD,DBD,DFDDBD,DBDFAC,ETA,DMAXC,DMAXI,TRUSTC,TRUSTI,TRUSTB,
     /  FACSIG,FACSI1,DIVDEL,DIVDE1,TRCFAC,TRCFA1,TRCFA2,TRIFAC,TRIFA1,
     /  TRIFA2,DIVTRC,DIVTRI,DMAXB,
     /  DEC1,DEC2,DEC3,DEC4,DEC5,DEC6,DEC7,DEC8,DEC9,DEC10,
     /  DBD1,DBDI,EDEL,EDELI,SUM,THETA,THETA1,TEMP,ZERO,ONE,INFTY,W,
     /  DABS,DMIN1,DMAX1,DNINT,DSQRT,DINT,
     /  FITER,RESITER,PEN,RESVIO,XREST,DREST,SCUB,BESRES,
     /  BADD
      DIMENSION X(N),XL(N),XU(N),G(MME),DF(N1),DG(MMAX,N1),U(MNNQP),
     /  B(NMAX,N1),BD(N1),D(N1),DQL1(N1),DL(N1),DU(N1),DLA(N1),
     /  DLAOLD(N1),ETA(N1),GOLD(MME),GQL2(MME),UOLD(MNNQP),UQL1(MNNQP),
     /  XOLD(N),W(LW),XREST(N),DREST(N),BADD(INTBIN),FITER(NONMON),
     /  RESITER(NONMON)
      LOGICAL   SEC,LOGW(LLOGW),RESTART,CURVE,SCALE,RESOPT
      INTRINSIC DABS,DMAX1,DMIN1,DNINT,DINT,DSQRT
      EXTERNAL  MIQLB4
C
C***********************************************************************
C 
      QLPRNT = 1
      ZERO  = 0.0D+0
      ONE   = 1.0D+0
      INFTY = 1.0D+30
      MMEN1 = MME+N1
	NULL  = 0
C
      IF (IFAIL.EQ.-3) GOTO 800
      IF (IFAIL.NE.-1) THEN
         IF (M.EQ.0) GOTO 20
         DO I=1,MME
            DG(I,N1) = ONE
         END DO
         DO I=1,ME
            DO J=1,N
               DG(I+M,J) = -DG(I,J)          
            END DO
            G(I+M) = -G(I)
         END DO
C
C GOTO BFGS UPDATE
C
   20    IF (IFAIL.EQ.-2) GOTO 850
      END IF
	PNEW  = PEN
	RESNEW= RESVIO
      IF (IFAIL.EQ.-1) THEN
         IF (SEC) GOTO 500
         GOTO 350
      END IF
C***********************************************************************
      IF (NCONT.EQ.0) TRUSTC=ZERO
      IF (NINT.EQ.0)  TRUSTI=ZERO
      IF (NBIN.EQ.0)  TRUSTB=ZERO

      FOLD=F
	RESOLD=RESNEW

      DO I=1,NONMON
	   FITER(I) = FOLD
	   RESITER(I) = RESOLD
	END DO
C
C IF IFAIL=0 SET START VALUES
C
      SEC    = .FALSE.
      NQL    = 0
      ITER   = 0
      NUNDO  = 0
      NPENUP = 0
      NQL2   = 0
      NODES  = 0
      R      = ZERO
      R1     = ZERO
      SIGMA1 = SIGMA
      DELTA1 = DELTA
      DL(N1) = ZERO
      DU(N1) = INFTY

	RESTART= .FALSE.
	CURVE  = .FALSE.
C***********************************************************************
  100 CONTINUE
      DO I=1,MNNQP
         UOLD(I) =  U(I)
      END DO
      KKT = ZERO
      DO I=1,N
         XOLD(I) = X(I)
         DLAOLD(I) = DF(I)
         DO J = 1,MME
            DLAOLD(I) = DLAOLD(I) - UOLD(J)*DG(J,I)
         END DO
         DLAOLD(I) = DLAOLD(I) - UOLD(MME+I) + UOLD(MMEN1+I)
c         IF  (N.LE.NCONT)  KKT = DMAX1(DABS(DLAOLD(I)),KKT)
         KKT = DMAX1(DABS(DLAOLD(I)),KKT)
      END DO

C
C NONMONOTONE BOUND
C
      IF (NONMON.EQ.0) GOTO 105    
      DO I=NONMON,2,-1
         FITER(I)=FITER(I-1)
	   RESITER(I)=RESITER(I-1)
	END DO
      FITER(1) = FOLD
	RESITER(1) = RESOLD
C
  105 RESOLD = RESNEW
      KKT = KKT + RESOLD
      FOLD = F
      DO I=1,M
        GOLD(I) = G(I)
      END DO
  150 CONTINUE
      SIGMA = SIGMA1
      DELTA = DELTA1
      POLD  = F + SIGMA*RESOLD
C
C CONSTRAINT VIOLATION PENALTY
C
      DF(N1) = SIGMA
      ITER = ITER+1
      IF (IPRINT.EQ.2) THEN
         IF (INTBIN.EQ.0) THEN
            WRITE(IOUT,900) ITER,F,RESOLD,SIGMA,TRUSTC,KKT
         ELSE IF (INTBIN.EQ.N) THEN
C
C AENDERN TRUSTB AUCH!!
C      
            WRITE(IOUT,900) ITER,F,RESOLD,SIGMA,TRUSTI,TRUSTB,KKT
         ELSE
            WRITE(IOUT,900) ITER,F,RESOLD,SIGMA,TRUSTC,TRUSTI,
     /                      TRUSTB,KKT
         END IF
      END IF
      IF (IPRINT.GE.3) THEN
         WRITE(IOUT,960) ITER,F,(X(I),I=1,N)
C
C OUTPUT OF ALL MNNQP MULTIPLIERS
C
         WRITE(IOUT,970) (U(I),I=1,MNNQP)
         IF (M.EQ.0) GOTO 160
         WRITE(IOUT,980) (G(I),I=1,M)
         WRITE(IOUT,990) RESOLD
  160    IF (INTBIN.EQ.0) THEN
            WRITE(IOUT,1000) TRUSTC
         ELSE IF (INTBIN.EQ.N) THEN
            IF (NBIN.EQ.N) THEN
               WRITE(IOUT,1000) TRUSTB
	      ELSE IF (NINT.EQ.N) THEN
               WRITE(IOUT,1000) TRUSTI
	      ELSE
               WRITE(IOUT,1002) TRUSTI
               WRITE(IOUT,1003) TRUSTB
            END IF
         ELSE
            IF (NBIN.EQ.0) THEN
               WRITE(IOUT,1001) TRUSTC
               WRITE(IOUT,1002) TRUSTI
	      ELSE IF (NINT.EQ.0) THEN
               WRITE(IOUT,1001) TRUSTC
               WRITE(IOUT,1003) TRUSTB
	      ELSE
               WRITE(IOUT,1001) TRUSTC
               WRITE(IOUT,1002) TRUSTI
               WRITE(IOUT,1003) TRUSTB
            END IF
         END IF
         WRITE(IOUT,1010) KKT
      END IF
C***********************************************************************
C OPIMALITY CHECK (KKT CONDITION) ONLY IF INTBIN=0
C
      IF (INTBIN.NE.0) GOTO 175
      IF (KKT.LT.ACC) GOTO 275
C***********************************************************************
  175 IF (ITER.GT.MAXIT) THEN
        IF (IPRINT.GT.0) WRITE(IOUT,930) MAXIT
        IFAIL = 1
        RETURN
      END IF

      RELCOU=0

  176 IF (DMAX1(TRUSTC,TRUSTI,TRUSTB).LE.ACCQP) THEN
        IFAIL=2
        GOTO 270
	END IF
C
      IF (NCONT.EQ.0) GOTO 180
      TEMP = -TRUSTC
      DO I=1,NCONT
	   IF (SCALE) THEN
            DL(I) = - X(I)
	   ELSE
            DL(I) = XL(I) - X(I)
	   END IF
         IF (DL(I).LT.TEMP) DL(I) = TEMP
	   IF (SCALE) THEN
            DU(I) = SCUB - X(I)
	   ELSE
            DU(I) = XU(I) - X(I)
	   END IF
         IF (DU(I).GT.TRUSTC) DU(I) = TRUSTC
      END DO
  180 IF (NINT.EQ.0) GOTO 182
      TEMP = -TRUSTI
      DO I=NCONT+1,NCONT+NINT
         DL(I) = XL(I) - X(I)
         IF (DL(I).LT.TEMP) DL(I) = TEMP
         DU(I) = XU(I) - X(I)
         IF (DU(I) .GT. TRUSTI) DU(I) = TRUSTI
      END DO
  182 IF (NBIN.EQ.0) GOTO 185
      TEMP = -TRUSTB
      DO I=NCONT+NINT+1,N
         DL(I) = XL(I) - X(I)
         IF (DL(I).LT.TEMP) DL(I) = TEMP
         DU(I) = XU(I) - X(I)
         IF (DU(I) .GT. TRUSTB) DU(I) = TRUSTB
      END DO
C***********************************************************************
C SOLVE QP SUBPROBLEM
C***********************************************************************
  185 DO I=1,N
         D(I) = ZERO
      END DO
      D(N1) = RESOLD
      NQL = NQL + 1
C  200 CONTINUE
      IFAIL1 = 0
C      write(*,*)'n is misqp',n
C      WRITE(IOUT,*)'MIQL CALL AT 1200'

      CALL MIQLB4(MME,NULL,MMAX,N1,NMAX,MNNQP,INTBIN,IND,B,DF,DG,G,DL,
     /          DU,D,U,FQP,ACCINT,ACCQP,IOPTNS,IOUT,IFAIL1,
     /          QLPRNT,W,LW,IWAR,LIWAR,LOGW,LLOGW)
C        write(iout,*)"nach miql1"
      NODES = NODES + IWAR(1)
	FQPREL=W(1)
C	write(iout,*)'ifail1:',ifail1
c	WRITE(IOUT,*)'FQPREL',FQPREL
      IF (IFAIL1.NE.0) THEN
	   IF (INTBIN.GT.0) GOTO 270
         IFAIL = 100 + IFAIL1  
         IF (IPRINT.NE.0) THEN
            WRITE(IOUT,*)'IFAIL(MIQP)=',IFAIL1
            WRITE(IOUT,920)
	   ENDIF
         RETURN
      END IF 
      DO I=NCONT+1,N
         D(I) = DNINT(D(I))
      END DO
c	write(iout,*)'!!!!!!!!!!nach QP1!!!!!!!!!!!!!',INTBIN,RELCOU

C     KEINE ZUSAETZLICHEN QPS 
c      goto 2000

	IF (INTBIN.EQ.0) GOTO 2000
c      IF (SIGMA*RESOLD-FQP.LE.ACC+EPSM) THEN
C	  WRITE(IOUT,*)'SIGMA,RESOLD,FQP,ACC,EPSM',SIGMA,
C     /	  RESOLD,FQP,ACC,EPSM,ACCQP,B(N1,N1),D(N1)

     
      IF (SIGMA*RESOLD-FQP.LE.ACC+0.5D0*B(N1,N1)*D(N1)**2.AND.
     /   (SIGMA*RESOLD-FQPREL.GT.ACCQP)) THEN
         RELCOU=RELCOU+1
	   IF(RELCOU.GT.INTBIN)GOTO 2000       
         J=0
         TEMP=-INFTY
c	write(iout,*)'!!!!!!!!!!!!!!nach QP2!!!!!!!!!!!!!!!!!!!!!!!'
         DO I=NCONT+1,N


	      IF (BADD(I-NCONT).LT.B(I,I)) THEN
	         IF (BADD(I-NCONT)-B(I,I).GT.TEMP) THEN
                  TEMP=BADD(I-NCONT)-B(I,I)
	            J=I
	         END IF
	      END IF
	   END DO
         IF (J.EQ.0) GOTO 2000
	   TEMP=BADD(J-NCONT)/B(J,J)
	   IF (TEMP.LE.EPSM) GOTO 2000
	   DO I=1,N
	      DO J=I,N
               B(I,J)=B(I,J)*TEMP
	         B(J,I)=B(I,J)
	      END DO
	   END DO 
C	   
         B(N1,N1)=B(N1,N1)*TEMP
C

c             DO J=1,N
c	WRITE(IOUT,*)'*************B:',(B(J,I),I=1,N)
c        ENDDO

         IF (NINT.EQ.0) THEN
      	    IF(TRUSTB.GT.ZERO) GOTO 176
	   ELSE IF (NBIN.EQ.0) THEN
	        IF(TRUSTI.GT.ZERO) GOTO 176
	   ELSE
	        IF(DMAX1(TRUSTI,TRUSTB).GT.ZERO) GOTO 176
	   END IF
	END IF
c	write(iout,*)'!!!!!!!!!!!!!!nach QP3!!!!!!!!!!!!!!!!!!!!!!!'

C
C DETERMINE SUBPROBLEM VALUE
C AND B*D AND D(T)*B*D
C
 2000 DBD = ZERO
      DFDDBD = ZERO
      DO I=1,N
         SUM = ZERO
         DO J=1,N
            SUM = SUM + D(J)*B(J,I)
         END DO
         DBD = DBD + SUM*D(I)
         BD(I) = SUM
         DFDDBD = DFDDBD + DF(I)*D(I)
      END DO
C
C   CALCULATION FOR OPTIMALITY TEST
C
      SUM=DABS(DFDDBD)
      DFDDBD = DFDDBD + DBD*0.5 
C
C PREDICTED REDUCTION
C
C  201 CONTINUE
      PRED = SIGMA*RESOLD  - DFDDBD - SIGMA*D(N1)

c	write(iout,*)'!!!!!!!!!!!!!!pred red!!!!!!!!!!!!!!!!!!!!!!!'

C
      IF (IPRINT.EQ.3) THEN 
         WRITE(IOUT,1020) PRED
         IF ( M.NE.0) WRITE(IOUT,1040) SIGMA,DELTA
      ELSE IF (IPRINT.GE.4) THEN      
         WRITE(IOUT,1020) PRED
         IF (M.NE.0) WRITE(IOUT,1040) SIGMA,DELTA
         WRITE(IOUT,1050) (D(I),I=1,N)
         WRITE(IOUT,970) (U(I),I=1,MNNQP)
      END IF
C***********************************************************************
C
C   TEST FOR OPTIMALITY
C
C  265 CONTINUE  
      IF (PRED.GT.ACC.OR.RESOLD.GT.ACC) GOTO 300
C***********************************************************************
C TEST PASSED
C
C CHECK IF CURRENT LAGRANGE MULTIPLIERS BETTER
C 
  270 CONTINUE
      IF (INTBIN.EQ.0.OR.(.NOT. RESOPT)) GOTO 272
      IF (RESTART.AND.RSC.EQ.1) GOTO 272
      IF (NINT.NE.0.AND.TRUSTI.LT.ONE) THEN
         TRUSTI=ONE
         TEMP = -TRUSTI
         DO I=NCONT+1,NCONT+NINT
            DL(I) = XL(I) - X(I)
            IF (DL(I).LT.TEMP) DL(I) = TEMP
            IF (DL(I).GT.ZERO) DL(I) = ZERO 
            DU(I) = XU(I) - X(I)
            IF (DU(I) .GT. TRUSTI) DU(I) = TRUSTI
            IF (DU(I).LT.ZERO) DU(I) = ZERO 
         END DO
      END IF
	IF (NBIN.NE.0.AND.TRUSTB.LT.ONE) THEN
		TRUSTB=ONE
         TEMP = -TRUSTB
         DO I=NCONT+NINT+1,N
            DL(I) = XL(I) - X(I)
            IF (DL(I).LT.TEMP) DL(I) = TEMP
            IF (DL(I).GT.ZERO) DL(I) = ZERO 
            DU(I) = XU(I) - X(I)
            IF (DU(I) .GT. TRUSTB) DU(I) = TRUSTB
            IF (DU(I).LT.ZERO) DU(I) = ZERO 
         END DO
	END IF
	DO I=1,N
	   D(I)=ZERO
	END DO
      D(N1) = RESOLD
      NQL = NQL + 1
      IFAIL1 = 0

C      WRITE(IOUT,*)'MIQL CALL AT 1300'

      CALL MIQLB4(MME,NULL,MMAX,N1,NMAX,MNNQP,0,IND,B,DF,DG,G,DL,DU,D,
     /          U,FQP,ACCINT,ACCQP,IOPTNS,IOUT,IFAIL1,QLPRNT,W,
     /          LW,IWAR,LIWAR,LOGW,LLOGW)
C        write(iout,*)"nach miql2"
	NODES = NODES + IWAR(1)
      FQPREL=W(1)
      IF (IFAIL1.NE.0) THEN
         GOTO 272
      END IF

      DO I=1,N
         IF (D(I).LT.DL(I)) D(I) = DL(I)
         IF (D(I).GT.DU(I)) D(I) = DU(I)         
      END DO

      DMAXI = ZERO
      DO I=NCONT+1,N
        DMAXI = DMAX1(DMAXI,DABS(D(I)))
      END DO
      IF (DMAXI.LT.EPSM) GOTO 272
C
C DETERMINE SUBPROBLEM VALUE
C AND B*D AND D(T)*B*D
C
      DBD = ZERO
      DFDDBD = ZERO
      DO I=1,N
         SUM = ZERO
         DO J=1,N
            SUM = SUM + D(J)*B(J,I)
         END DO
         DBD = DBD + SUM*D(I)
         DFDDBD = DFDDBD + DF(I)*D(I)
      END DO
      DFDDBD = DFDDBD + DBD*0.5 
C
C PREDICTED REDUCTION
C
      PRED = SIGMA*RESOLD  - DFDDBD - SIGMA*D(N1)
      IF (PRED.LT.ACC) GOTO 272

      DO I=NCONT+1,N
	   IF (DABS(D(I)).GT.EPSM.AND.DABS(D(I)).LT.0.5D0) THEN
            IF (D(I).LT.-EPSM) THEN
               D(I) = DNINT(D(I)-0.5D0)
	      ELSE 
               D(I) = DNINT(D(I)+0.5D0)
            END IF
	   ELSE
            D(I) = DNINT(D(I))
	   END IF
      END DO
	IF (RESTART) THEN
         TEMP=ZERO
	   DO I=NCONT+1,N
            TEMP=DMAX1(DABS(XREST(I)-X(I)),TEMP)
	   END DO
	   IF (TEMP.LE.EPSM) THEN
	      TEMP=ZERO
	      DO I=NCONT+1,N
               TEMP=DMAX1(DABS(DREST(I)-D(I)),TEMP)
	      END DO
            IF (TEMP.LE.EPSM) GOTO 272
         END IF
	END IF
      DO I=1,N
         XREST(I)=X(I)
	   DREST(I)=D(I)
	END DO
	IF (IPRINT.GT.0) WRITE (IOUT,1090)
	NPENUP=0
	RESTART=.TRUE.
	CURVE=.TRUE.
	RSC=1
      GOTO 330
C
C ENDE ZUSATZ!!!!!!!!!
C
  272 TEMP = KKT
      KKT  = ZERO
      DO I=1,N
         DLAOLD(I) = DF(I)
         DO J=1,MME
            DLAOLD(I) = DLAOLD(I) - U(J)*DG(J,I)
         END DO
         DLAOLD(I) = DLAOLD(I) - U(MME+I) + U(MMEN1+I)
         KKT = DMAX1(DABS(DLAOLD(I)),KKT)
      END DO
      KKT = KKT + RESOLD
      IF (KKT.GE.TEMP) THEN
C         KKT = TEMP
         GOTO 275
      END IF
C
C VIELLEICHT KANN MAN IMMER DIE NEUEN NEHMEN!!!!
C
      DO I=1,MNNQP
         UOLD(I) = U(I)
      END DO
      IF (IPRINT.EQ.3) THEN
         WRITE(IOUT,1080) (U(I),I=1,MNNQP)
         WRITE(IOUT,1081) KKT
      END IF
C
  275 CONTINUE
c      IF (IPRINT.NE.2) GOTO 280
c      IF (INTBIN.EQ..0) THEN
c         WRITE(IOUT,900) ITER,F,RESOLD,SIGMA,TRUSTC,KKT
c      ELSE IF (NCONT.EQ.0) THEN
c         WRITE(IOUT,900) ITER,F,RESOLD,SIGMA,TRUSTI,KKT
c      ELSE
c         WRITE(IOUT,900) ITER,F,RESOLD,SIGMA,TRUSTC,TRUSTI,KKT
c      END IF
C  280 IF (RESOLD.GT.ACC) THEN

      IF (BESRES.GT.ACC) THEN
         IFAIL = 4
         IF (IPRINT.GT.0) WRITE(IOUT,951) 
      ELSE
         IFAIL = 0
      END IF
c      IF (NINT.NE.0.AND.DABS(TRUSTI).LT.EPSM) IFAIL = 5
c      IF (NBIN.NE.0.AND.DABS(TRUSTB).LT.EPSM) IFAIL = 5
      RETURN
C***********************************************************************
C
C PENALTY-PARAMETER UPDATE
C
  300 CONTINUE
c      IF (IPRINT.NE.2) GOTO 310
c      IF (INTBIN.EQ.0) THEN
c         WRITE(IOUT,900) ITER,F,RESOLD,SIGMA,TRUSTC,KKT
c      ELSE IF (INTBIN.EQ.N) THEN
C
C AENDERN TRUSTB AUCH!!
C      
c         WRITE(IOUT,900) ITER,F,RESOLD,SIGMA,TRUSTI,KKT
c      ELSE
c         WRITE(IOUT,900) ITER,F,RESOLD,SIGMA,TRUSTC,TRUSTI,KKT
c      END IF
C  310 CONTINUE
      IF (M.NE.0) GOTO 320
      SIGMA1 = SIGMA
      DELTA1 = DELTA
  320 IF ((RESOLD-D(N1)).LT.ACC.AND.D(N1).GT.ACC) THEN
         NPENUP= NPENUP+1
         SIGMA = FACSIG * SIGMA
         DELTA = DELTA / DIVDEL
         DF(N1)= SIGMA
         POLD  = F + SIGMA*RESOLD
      END IF
      IF (INTBIN.GT.0) THEN
        IF (NCONT.EQ.0) THEN
	      IF (NBIN.EQ.0) THEN
               TEMP = DMIN1(TRUSTI,RESOLD)
	      ELSE IF (NINT.EQ.0) THEN
               TEMP = DMIN1(TRUSTB,RESOLD)
	      ELSE
               TEMP = DMIN1(DMAX1(TRUSTI,TRUSTB),RESOLD)	      
            END IF
        ELSE
	    IF (NBIN.EQ.0) THEN
        TEMP = DMIN1(DMAX1(TRUSTI,TRUSTC),RESOLD)
	    ELSE IF (NINT.EQ.0) THEN
        TEMP = DMIN1(DMAX1(TRUSTB,TRUSTC),RESOLD)
	    ELSE
        TEMP = DMIN1(DMAX1(TRUSTC,DMAX1(TRUSTI,TRUSTB)),RESOLD)	      
            ENDIF
        END IF
      ELSE
         TEMP = DMIN1(TRUSTC,RESOLD)
      END IF
      IF (PRED.LT.(DELTA*SIGMA*TEMP)) THEN
         NPENUP = NPENUP+1
         SIGMA1 = FACSI1 * SIGMA
         DELTA1 = DELTA / DIVDE1
      ELSE
         SIGMA1 = SIGMA
         DELTA1 = DELTA
      END IF
C
      IF (NPENUP.GT.MAXPEN) THEN
         IF (IPRINT.GT.0) WRITE(IOUT,950) MAXPEN
         IFAIL = 3
	   IF (INTBIN.GT.0) THEN
            NPENUP=0
            GOTO 270
	   END IF
         RETURN
      END IF
  330 IFAIL = -1
C
C PRED=0 START NEXT ITERATION (PENALTY UPDATED)
C GOTO: STEP NOT SUCCESSFUL
C
      IF (DABS(PRED).LT.EPSM.AND. (.NOT. RESTART)) THEN
         GOTO 750
      END IF
      DO I=1,N
         X(I) = X(I) + D(I)
      END DO
C      DO I=1,M
C        GOLD(I)=G(I)
C      ENDDO
C      FOLD=F
      NFUNC = NFUNC+1
      RETURN
  350 CONTINUE
C      DO I=1,M
C        IF(ABS(G(I)).GT. INFTY)G(I)=GOLD(I)
C      ENDDO
C      IF(ABS(F).GT. INFTY)F=FOLD
      IF (CURVE) THEN
	   CURVE=.FALSE.
	   GOTO 800
	END IF
C
C CALCULATE R
C
      IF (NONMON.EQ.0.OR.
     /   (INTBIN.EQ.0.AND.M.EQ.0)) GOTO 351

	TEMP=FITER(1)+SIGMA*RESITER(1)
	DO I=2,NONMON
         TEMP=DMAX1(TEMP,FITER(I)+SIGMA*RESITER(I))
	END DO
C
      TEMP=DMAX1(POLD,TEMP)
      R=(TEMP-PNEW)/PRED
C
C INCREASE ONLY ALLOWED IF STEP WILL BE AVERAGE OR BETTER
C
      IF (R.GT.DEC2) POLD=TEMP
  351 R = (POLD - PNEW)/PRED
      IF (IPRINT.GE.4) THEN
         WRITE(IOUT,1060) R
      END IF
C
C GOOD STEP 1
C
      IF (R.GT.DEC1) GOTO 600
C  370 CONTINUE
      IF (M.EQ.0) THEN
         IF (R.LT.DEC2) GOTO 550
         GOTO 700
      END IF     
C***********************************************************************
C SECOND ORDER CORRECTION
C***********************************************************************
      DO I=1,N
         DQL1(I) = D(I)
      END DO
      DO I=1,MNNQP
         UQL1(I) = U(I)
      END DO
      DO I=1,M
         GQL2(I) = G(I)
         DO J=1,N
            GQL2(I) = GQL2(I)-DG(I,J)*D(J)
         END DO
      END DO
      DO I=1,ME
         GQL2(I+M) = - GQL2(I)
      END DO
C  375 CONTINUE
      D(N1) = RESNEW
      NQL = NQL + 1
      NQL2 =  NQL2 + 1
C  400 CONTINUE
      IFAIL1 = 0

C      WRITE(IOUT,*)'MIQL CALL AT 1600'
    
      CALL MIQLB4(MME,NULL,MMAX,N1,NMAX,MNNQP,INTBIN,IND,B,DF,DG,GQL2,
     /          DL,DU,D,U,FQP,ACCINT,ACCQP,IOPTNS,IOUT,
     /          IFAIL1,QLPRNT,W,LW,IWAR,LIWAR,LOGW,LLOGW)
C        write(iout,*)"nach miql3"
      NODES = NODES + IWAR(1)
	FQPREL=W(1)
      IF (IFAIL1.NE.0) THEN
	   IF (INTBIN.GT.0) GOTO 270
         IFAIL = 100 + IFAIL1
         DO I=1,N
            X(I) = XOLD(I)
         END DO
         IF (IPRINT.NE.0)   THEN
            WRITE(IOUT,*)'IFAIL(MIQP)=',IFAIL1
            WRITE(IOUT,920) 
         ENDIF   
         RETURN
      END IF
C 4000 CONTINUE
      DO I=NCONT+1,N
         D(I) = DNINT(D(I))
      END DO
C
      TEMP = DFDDBD
      DBD = ZERO
      DFDDBD = ZERO
      DO I=1,N
        SUM = ZERO
        DO J=1,N
          SUM = SUM + D(J)*B(J,I)
        END DO
        DBD = DBD + SUM*D(I)
        BD(I) = SUM
        DFDDBD = DFDDBD + DF(I)*D(I)
      END DO
      DFDDBD = DFDDBD + DBD*0.5D0
      R1 = R + (TEMP + SIGMA*RESNEW - DFDDBD - SIGMA*D(N1))/PRED
C  401 CONTINUE
      IF (IPRINT.GE.4) THEN
C        WRITE(IOUT,1030) 
        IF (M.NE.0) WRITE(IOUT,1040) SIGMA,DELTA
        WRITE(IOUT,1050) (D(I),I=1,N)
        WRITE(IOUT,970) (U(I),I=1,MNNQP)
        WRITE(IOUT,1070) R1
      END IF
C
C BAD STEP 1
C      
      IF (R.LT.DEC2) GOTO 450
C
C AVERAGE STEP 1
C
      IF (R1.GE.DEC3.AND.R1.LE.DEC4) THEN
C
C VERY GOOD PREDICTION FOR STEP 2
C
        IF (NCONT.GT.0) TRUSTC = TRCFAC*TRUSTC
        IF (NINT.GT.0) TRUSTI = DMAX1(ONE,TRIFAC*TRUSTI)
	  IF (NBIN.GT.0) TRUSTB = ONE
      END IF
      DO I=1,N
        D(I) = DQL1(I)
      END DO
      DO I=1,MNNQP
        U(I) = UQL1(I)
      END DO
      GOTO 700
C
C BAD STEP 1
C      
  450 IF (NBIN.GT.0) THEN
         DMAXB = ZERO
         DO I=NCONT+NINT+1,N
            DMAXB = DMAX1(DMAXB,DABS(D(I)))
         END DO
         IF (DMAXB.LE.ONE+EPSM.AND.DMAXB.GE.ONE-EPSM) R1=DMAX1(R1,DEC5)
      END IF
      IF (R1.LT.DEC5) THEN
C
C PREDICTION STEP 2 NOT GOOD ENOUGH => CHECK STEP 1
C
        DO I=1,N
          D(I) = DQL1(I)
        END DO
        DO I=1,MNNQP
          U(I) = UQL1(I)
        END DO
        GOTO 550
      END IF
C
C GET NEW FUNCTION VALUE AND CHECK STEP 2
C
C  451 CONTINUE
      RESNE1= RESNEW
      PNEW1 = PNEW
C
C CHECK IF CURRENT POINT MUST BE STORED
C   
      IF (R.GT.DEC10) THEN
C
C STEP WILL BE ACCEPTED BUT PERHAPS SECOND STEP NOT BETTER
C
        FOLD = F
        DO I=1,M
          GOLD(I) = G(I)
        END DO
      END IF
      DO I=1,N
        X(I) = XOLD(I)+D(I)
      END DO
      IFAIL = -1
      SEC = .TRUE.
      NFUNC = NFUNC+1
      RETURN
  500 CONTINUE
      SEC = .FALSE.
      IF (PNEW.GT.PNEW1) THEN
C
C STEP 1 BETTER => CHECK STEP 1
C
        RESNEW = RESNE1
        IF (R.GT.DEC10) THEN
          F = FOLD
          DO I=1,M
            G(I) = GOLD(I)
          END DO
        END IF
        DO I=1,N
          D(I) = DQL1(I)
          X(I) = XOLD(I)+D(I)
        END DO
        DO I=1,MNNQP
          U(I) = UQL1(I)
        END DO
        GOTO 550
      END IF
      R = (POLD-PNEW)/PRED
C
      IF (IPRINT.GE.4) THEN
         WRITE(IOUT,1060) R
      END IF
C
C STEP 2 GOOD => EXPAND TRUST
C
      IF (R.GE.DEC6) GOTO 600
C
C STEP 2 AVERAGE
C
      IF (R.GE.DEC7) GOTO 700
C
C STEP 2 BAD => REDUCE TRUST
C
  550 CONTINUE
C
C TRUST REGION REDUCTION
C
      IF (NCONT.EQ.0) GOTO 555
      DMAXC = ZERO
      DO I=1,NCONT
        DMAXC = DMAX1(DMAXC,DABS(D(I)))
      END DO
      IF (DMAXC.GT.EPSM) TRUSTC = DMAXC/DIVTRC
  555 IF (NINT.EQ.0) GOTO 558
      DMAXI = ZERO
      DO I=NCONT+1,NCONT+NINT
        DMAXI = DMAX1(DMAXI,DABS(D(I)))
      END DO
      TRUSTI = DINT(DMAXI/DIVTRI)    
  558 IF (NBIN.EQ.0) GOTO 560
	TRUSTB=ZERO
  560 GOTO 700
C
C EXPAND TRUST REGION
C
  600 IF (NCONT.EQ.0) GOTO 650
      DMAXC = ZERO
      DO I=1,NCONT
        DMAXC = DMAX1(DMAXC,DABS(D(I)))
      END DO
      IF (DMAXC.GE.TRUSTC-EPSM) THEN
        IF (R.GT.DEC8) THEN 
          TRUSTC = TRCFA1*TRUSTC
        ELSE
          TRUSTC = TRCFA2*TRUSTC
        END IF
      END IF
  650 IF (NINT.EQ.0) GOTO 675
      DMAXI = ZERO
      DO I=NCONT+1,N
        DMAXI = DMAX1(DMAXI,DABS(D(I)))
      END DO
      IF (DMAXI.GE.TRUSTI-EPSM) THEN
         IF (R.GT.DEC9) THEN
            TRUSTI = DMAX1(ONE,TRIFA1*TRUSTI)
         ELSE
            TRUSTI = DMAX1(ONE,TRIFA2*TRUSTI)
         END IF
      END IF
  675 IF (NBIN.EQ.0) GOTO 700
      TRUSTB=ONE
C
C CHECK IF STEP ACCEPTED
C
  700 IF (R.GT.DEC10) GOTO 800
C
C STEP NOT SUCCESSFUL
C
      F = FOLD
      DO I=1,M
         G(I) = GOLD(I)
      END DO
      DO I=1,N
         X(I) = XOLD(I)
      END DO
      DO I=1,MNNQP
         U(I) =  UOLD(I)
      END DO
  750 NUNDO = NUNDO+1
      IF (NUNDO.EQ.MAXUND) THEN
         IF (IPRINT.GT.0) WRITE(IOUT,940) MAXUND
         IFAIL = 2
	   IF (INTBIN.GT.0) THEN
            NUNDO=0
            GOTO 270
	   END IF
         RETURN
      END IF
      GOTO 150
C
C SUCCESSFUL STEP
C
  800 IFAIL = -2 
      NGRAD = NGRAD+1
C
C MULTIPLIERS CORRECTION
C     
      DO I=NCONT+1,N
         IF (D(I).LT.DU(I).AND.D(I).GT.DL(I)) THEN
c	      U(MME+I)=ZERO
c	      U(MMEN1+I)=ZERO
	   ELSE IF (D(I).LE.DL(I)+EPSM) THEN
            U(MME+I)=DMAX1(U(MME+I),U(MMEN1+I))
            U(MMEN1+I)=ZERO
	   ELSE IF (D(I).GE.DU(I)-EPSM) THEN
            U(MMEN1+I)=DMAX1(U(MME+I),U(MMEN1+I))
            U(MME+I)=ZERO
	   END IF
	END DO     
      RETURN
  850 CONTINUE
C
C SET UNDO-COUNTER NUNDO=0
C
      NUNDO = 0
C
C PERFORM NEXT ITERATION
C
C      IF (NCONT.EQ.0.AND.MODE.LT.4) GOTO 100
C
C BFGS UPDATE
C
C      IF (NINT.EQ.0.OR.MODE.GE.4) GOTO 870
      DBD = ZERO
      DO I=1,N
         SUM = ZERO
         DO J=1,N
            SUM = SUM + D(J)*B(J,I)
         END DO
         DBD = DBD + SUM*D(I)
         BD(I) = SUM
      END DO
      IF (DBD.LE.EPSM) GOTO 100
C  870 CONTINUE
      NBFGS = N
c      IF (MODE.LE.3) THEN
c         NBFGS = NCONT
c      ELSE
c         NBFGS = N
c      END IF
      DO I=1,N
         DLA(I) = DF(I)
         DO J=1,MME
            DLA(I) = DLA(I) - UOLD(J)*DG(J,I)
         END DO
         DLA(I) = DLA(I) - UOLD(MME+I) + UOLD(MMEN1+I)
      END DO
C
C UPDATE HESSIAN OF LAGRANGIAN
C
C  872 CONTINUE
      EDEL = ZERO
c      WRITE(IOUT,*)'DLA',(DLA(J),J=1,N)
c      WRITE(IOUT,*)'DLAOLD',(DLAOLD(J),J=1,N)

      DO I=1,NBFGS
         ETA(I) = DLA(I) - DLAOLD(I)
         EDEL = EDEL + D(I)*ETA(I)
      END DO

c      WRITE(IOUT,*)'ETA',(ETA(J),J=1,N)

c      WRITE(IOUT,*)'EDEL',EDEL

      DBD1 = DBDFAC*DBD
      IF (EDEL.GE.DBD1) GOTO 875
      THETA = (DBD - DBD1)/(DBD - EDEL)
      THETA1 = ONE - THETA
      DO I=1,NBFGS
         ETA(I) = THETA*ETA(I) + THETA1*BD(I)
      END DO

c      WRITE(IOUT,*)'ETA',(ETA(J),J=1,N)

      EDEL = DBD1
  875 CONTINUE
      IF (DBD.LT.ZERO) THEN
         DBD =  DABS(DBD)
      END IF
      IF (EDEL.LT.ZERO) THEN
         EDEL = DABS(EDEL)
      END IF
      DBDI = DSQRT(ONE/DBD)
      EDELI = DSQRT(ONE/EDEL)
      DO I=1,NBFGS
         BD(I) = DBDI*BD(I)
         ETA(I) = EDELI*ETA(I)
      END DO

c      WRITE(IOUT,*)'ETA',(ETA(J),J=1,N)
c      WRITE(IOUT,*)'BD',(BD(I),I=1,N)

      DO I=1,NBFGS
         DO J=I,NBFGS
            B(I,J) = B(I,J) + ETA(I)*ETA(J) - BD(I)*BD(J)
            B(J,I) = B(I,J)
         END DO
      END DO  

C             DO J=1,N
C	WRITE(IOUT,*)'*************B:',(B(J,I),I=1,N)
C        ENDDO

C
C PERFORM NEXT ITERATION
C
      GOTO 100
C***********************************************************************
C FORMAT DEFINITIONS
C***********************************************************************

  900 FORMAT(I5,1X,D13.6,6(2X,D8.2))
  920 FORMAT(/,' *** ERROR: FAILURE MESSAGE OF MIQP SOLVER',//)
  930 FORMAT(/,' *** ERROR 1: MORE THAN MAXIT ITERATIONS (',I5,')',//)
  940 FORMAT(/,' *** ERROR 2: MORE THAN MAXUND NON-SUCCESSFUL STEPS (',
     /                              I5,')',//)
  950 FORMAT(/,' *** ERROR 3: MORE THAN MAXPEN PENALTY UPDATES (',
     /                              I5,')',//)
  951 FORMAT(/,' *** ERROR 4: TERMINATION AT INFEASIBLE ITERATE',//)
  960 FORMAT(//2X,'ITERATION',I5,//,
     /       6X,'OBJECTIVE FUNCTION VALUE:       F(X)  =',D16.8,/,
     /       6X,'VARIABLE VALUES:              X     =',/,(8X,4D16.8))
  970 FORMAT(6X,'MULTIPLIER VALUES:            U     =',/,(8X,4D16.8))
  980 FORMAT(6X,'CONSTRAINT FUNCTION VALUES:   G(X)  =',/,(8X,4D16.8))
  990 FORMAT(6X,'MAXIMUM CONSTRAINT VIOLATION: MCV   =',D12.4)
 1000 FORMAT(6X,'TRUST REGION RADIUS:          TRST  =',D12.4)
 1001 FORMAT(6X,'REAL TRUST REGION RADIUS:     TRSTC =',D12.4)
 1002 FORMAT(6X,'INTEGER TRUST REGION RADIUS:  TRSTI =',D12.4)
 1003 FORMAT(6X,'BINARY TRUST REGION RADIUS:   TRSTB =',D12.4)
 1010 FORMAT(6X,'KKT RESIDUAL:                 KKT   =',D12.4) 
 1020 FORMAT(6X,'PREDICTED REDUCTION:          PRED  =',D12.4)
 1040 FORMAT(6X,'PENALTY PARAMETER:            SIGMA =',D12.4,/,
     /       6X,'SCALING PARAMETER:            DELTA =',D12.4)
 1050 FORMAT(6X,'SEARCH STEP:                  D     =',/,(8X,4D16.8))
 1060 FORMAT(6X,'RATIO ACTUAL/PREDICTED:       R     =',D12.4)
 1070 FORMAT(6X,'SECOND ORDER CORRECTION:      SOC   =',D12.4)
 1080 FORMAT(6X,'IMPROVED MULTIPLER VALUE      U     =',/,(8X,4D16.8))
 1081 FORMAT(6X,'IMPROVED KKT RESIDUAL:        KKT   =',D12.4) 
 1090 FORMAT(2X,//,'*** RESTART ***') 
C***********************************************************************
C 9999 CONTINUE
      RETURN
      END
C***********************************************************************
C END SUBROUTINE MISQP1
C***********************************************************************
C
      SUBROUTINE MISQP2(M,ME,MME,MMAX,N,N1,NMAX,X,F,G,DF,DG,
     /  XL,XU,B,EPSM,INDEX,GX,FX,GEPS1,FEPS1,GEPS2,FEPS2,LDINT,FIRST,
     /  SEC,HITL,HITR,ACC,START,NCONT,U,LAG,LAG1,LAG2,BADD,NINT,DLA)
C
C***********************************************************************
C
C SUBROUTINE MISQP2
C
C DETERMINES DERIVATIVES AND HESSIAN-APPROXIMATION FOR INTEGER VARIABLES
C
C***********************************************************************
C
      IMPLICIT NONE
      INTEGER I,J,M,ME,MME,MMAX,N,N1,NMAX,INDEX,NCONT,NINT
      DOUBLE PRECISION X,XL,XU,F,G,DF,DG,B,EPSM,FX,GX,
     /                 FEPS1,GEPS1,FEPS2,GEPS2,ONE,TWO,
     /                 DABS,ACC,LAG,LAG1,LAG2,U,BADD,TEMP,DLA
      DIMENSION X(N),XL(N),XU(N),G(M),DF(N1),DG(MMAX,N),U(MME+N1+N1),
     /          B(NMAX,N),GX(M),GEPS1(M),GEPS2(M),BADD(N-NCONT),DLA(N)
      LOGICAL LDINT,FIRST,HITR,HITL,SEC,START
      INTRINSIC DABS
C
C     ZERO = 0.0D+0
      ONE = 1.0D+0
      TWO = 2.0D+0
C
      IF (FIRST) THEN
         FIRST = .FALSE.
         SEC   = .FALSE.
         HITR  = .TRUE.
         HITL  = .TRUE.

	   LAG = F 
         DO I=1,ME
            LAG = LAG - (U(I)-U(I+M))*G(I)
         END DO
         DO I=ME+1,M
            LAG = LAG - U(I)*G(I)
         END DO
         DO I=1,N
            LAG = LAG - U(I+MME)*(X(I)-XL(I))
            LAG = LAG + U(I+MME+N1)*(XU(I)-X(I))
         END DO
C   5 CONTINUE
         DO I=1,M
            GX(I) = G(I)
         END DO
         FX = F
      END IF
C
      IF (SEC) THEN
         IF (HITL) GOTO 200
         GOTO 100
      END IF
      IF (.NOT.HITL) GOTO 200
      IF (.NOT.HITR) GOTO 100
C
   10 CONTINUE
      HITR = .TRUE.
      HITL = .TRUE.
      X(INDEX) = X(INDEX) + ONE
      IF (X(INDEX).LE.XU(INDEX)) THEN 
        HITR = .FALSE.
        RETURN
      END IF
      GOTO 150
  100 FEPS1 = F 
      LAG1  = F
      DO J=1,N
        LAG1 = LAG1 - U(J+MME)*(X(J)-XL(J))
        LAG1 = LAG1 + U(J+MME+N1)*(XU(J)-X(J))
      END DO
      IF (M.EQ.0) GOTO 145
      DO J=1,ME
        LAG1 = LAG1 - (U(J)-U(J+M))*G(J)
        GEPS1(J) = G(J)
      END DO
      DO J=ME+1,M
        LAG1 = LAG1 - U(J)*G(J)
        GEPS1(J) = G(J)
      END DO
  145 IF (SEC) GOTO 400
  150 X(INDEX) = X(INDEX) - TWO
      IF (X(INDEX).GE.XL(INDEX)) THEN
        HITL = .FALSE.
        RETURN
      END IF
      GOTO 250
  200 FEPS2 = F 
      LAG2  = F
      DO J=1,N
        LAG2 = LAG2 - U(J+MME)*(X(J)-XL(J))
        LAG2 = LAG2 + U(J+MME+N1)*(XU(J)-X(J))
      END DO
      IF (M.EQ.0) GOTO 245
      DO J=1,ME
        LAG2 = LAG2 - (U(J)-U(J+M))*G(J)
        GEPS2(J) = G(J)
      END DO
      DO J=ME+1,M
        LAG2 = LAG2 - U(J)*G(J)
        GEPS2(J) = G(J)
      END DO
  245 IF (SEC) GOTO 300
  250 X(INDEX) = X(INDEX) + ONE
      IF ((.NOT.HITR).AND.(.NOT.HITL)) THEN
         DF(INDEX) = (FEPS1 - FEPS2)*0.5D0
         DO J=1,ME
	      IF (DABS(GX(J)).LE.ACC+EPSM) THEN
               DG(J,INDEX) = (GEPS1(J) - GEPS2(J))*0.5D0
	      ELSE
	         IF (DABS(GEPS1(J)).LE.ACC+EPSM) THEN
                  DG(J,INDEX) = GEPS1(J) - GX(J)
	         ELSE IF (DABS(GEPS2(J)).LE.ACC+EPSM) THEN
                  DG(J,INDEX) = GX(J) - GEPS2(J)
	         ELSE
                  DG(J,INDEX) = (GEPS1(J) - GEPS2(J))*0.5D0
               END IF
	      END IF
         END DO
         DO J=ME+1,M
            DG(J,INDEX) = (GEPS1(J) - GEPS2(J))*0.5D0
         END DO
C
C
         DLA(INDEX)=DF(INDEX)
         DO J=1,ME
            DLA(INDEX) = DLA(INDEX) 
     /        - U(J)*DG(J,INDEX)+U(J+M)*DG(J,INDEX)
         END DO
         DO J=ME+1,M
            DLA(INDEX) = DLA(INDEX) 
     /        - U(J)*DG(J,INDEX)
         END DO
         DLA(INDEX) = DLA(INDEX) 
     /      - U(MME+INDEX) + U(MME+N1+INDEX)
C
C
         TEMP=LAG1-(LAG+DLA(INDEX))
         BADD(INDEX-NCONT)=LAG2-(LAG-DLA(INDEX))
         BADD(INDEX-NCONT)=(BADD(INDEX-NCONT)+TEMP)
c         BADD(INDEX-NCONT)=BADD(INDEX-NCONT)*2.0D0
c         BADD(INDEX-NCONT) = LAG1-2.0D0*LAG+LAG2
         IF (BADD(INDEX-NCONT).LE.ACC) BADD(INDEX-NCONT)=ACC
         GOTO 550
      ELSE
         IF (HITL) THEN
            DF(INDEX) = FEPS1-FX
            DO J=1,M
               DG(J,INDEX) = GEPS1(J) - GX(J)
            END DO
C
C
            DLA(INDEX)=DF(INDEX)
            DO J=1,ME
               DLA(INDEX) = DLA(INDEX) 
     /            - U(J)*DG(J,INDEX)+U(J+M)*DG(J,INDEX)
            END DO
            DO J=ME+1,M
               DLA(INDEX) = DLA(INDEX) 
     /            - U(J)*DG(J,INDEX)
            END DO
            DLA(INDEX) = DLA(INDEX) 
     /         - U(MME+INDEX) + U(MME+N1+INDEX)
C
C
            X(INDEX) = X(INDEX) + TWO

            IF  (X(INDEX).LE.XU(INDEX)) THEN 
               BADD(INDEX-NCONT)=LAG1-(LAG+DLA(INDEX))
               BADD(INDEX-NCONT)=BADD(INDEX-NCONT)*2.0D0
               IF (BADD(INDEX-NCONT).LE.ACC) BADD(INDEX-NCONT)=ACC
               X(INDEX) = X(INDEX) - TWO
               GOTO 550
C
C not necessary
C
               SEC = .TRUE.
               RETURN
            END IF
C
C 0/1 - PROBLEM
C
            X(INDEX) = X(INDEX) - TWO

	      BADD(INDEX-NCONT)=LAG1-(LAG+DLA(INDEX))
            BADD(INDEX-NCONT)=BADD(INDEX-NCONT)*2.0D0
c            BADD(INDEX-NCONT) = DMAX1(BADD(INDEX-NCONT),ACC)
            IF (BADD(INDEX-NCONT).LE.ACC) BADD(INDEX-NCONT)=ACC
            GOTO 550
         ELSE
            DF(INDEX) = FX-FEPS2
            DO J=1,M
               DG(J,INDEX)  =  GX(J) - GEPS2(J)
            END DO
C
C
            DLA(INDEX)=DF(INDEX)
            DO J=1,ME
               DLA(INDEX) = DLA(INDEX) 
     /            - U(J)*DG(J,INDEX)+U(J+M)*DG(J,INDEX)
            END DO
            DO J=ME+1,M
               DLA(INDEX) = DLA(INDEX) 
     /           - U(J)*DG(J,INDEX)
            END DO
            DLA(INDEX) = DLA(INDEX) 
     /         - U(MME+INDEX) + U(MME+N1+INDEX)
C
C
            X(INDEX) = X(INDEX) - TWO

            IF (X(INDEX).GE.XL(INDEX)) THEN 
	         BADD(INDEX-NCONT)=LAG2-(LAG-DLA(INDEX))
               BADD(INDEX-NCONT)=BADD(INDEX-NCONT)*2.0D0

               IF (BADD(INDEX-NCONT).LE.ACC) BADD(INDEX-NCONT)=ACC
               X(INDEX) = X(INDEX) + TWO
               GOTO 550
C
C not necessary
C
               SEC = .TRUE.
               RETURN
            END IF
C
C 0/1 - PROBLEM
C
            X(INDEX) = X(INDEX) + TWO
C
	      BADD(INDEX-NCONT)=LAG2-(LAG-DLA(INDEX))
            BADD(INDEX-NCONT)=BADD(INDEX-NCONT)*2.0D0
            IF (BADD(INDEX-NCONT).LE.ACC) BADD(INDEX-NCONT)=ACC
            GOTO 550
         END IF
      END IF 
  300 CONTINUE

      SEC = .FALSE.
      X(INDEX) = X(INDEX) - TWO
c      BADD(INDEX-NCONT) = LAG2-2.0D0*LAG1+LAG
      BADD(INDEX-NCONT)=LAG1-(LAG+DLA(INDEX))
      TEMP=LAG2-(LAG+2.0D0*DLA(INDEX))
      BADD(INDEX-NCONT)=DMIN1(BADD(INDEX-NCONT),TEMP)
c      BADD(INDEX-NCONT)=BADD(INDEX-NCONT)*2.0D0
      IF (BADD(INDEX-NCONT).LE.ACC) BADD(INDEX-NCONT)=ACC
      GOTO 550
  400 CONTINUE

      SEC = .FALSE.
      X(INDEX) = X(INDEX) + TWO
c      BADD(INDEX-NCONT) = LAG-2.0D0*LAG2+LAG1
	BADD(INDEX-NCONT)=LAG2-(LAG-DLA(INDEX))
      TEMP=LAG1-(LAG-2.0D0*DLA(INDEX))
      BADD(INDEX-NCONT)=DMIN1(BADD(INDEX-NCONT),TEMP)
c      BADD(INDEX-NCONT)=BADD(INDEX-NCONT)*2.0D0
      IF (BADD(INDEX-NCONT).LE.ACC) BADD(INDEX-NCONT)=ACC
  550 INDEX = INDEX + 1

      IF (INDEX.GT.N) THEN
	   IF (START) THEN
	      DO I=NCONT+NINT+1,N
               B(I,I)=BADD(I-NCONT)
	      END DO
            START=.FALSE.
	   END IF
         LDINT = .FALSE.
C 
C SET OLD VALUES FOR F AND G
C
         F = FX
         DO I=1,M
           G(I) = GX(I)
         END DO
         RETURN
      END IF
C
C NEXT INTEGER VARIABLE
C
      GOTO 10 
      END
C***********************************************************************
C END SUBROUTINE MISQP2
C***********************************************************************

