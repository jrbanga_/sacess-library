C
C************************************************************************
C
C   FILE NAME:  CHOLKY.FOR
C
C************************************************************************
C
C
C
C               C H O L E S K Y    D E C O M P O S I T I O N
C
C
C
C   Problem:
C   -------
C
C   The code factorizes a positive definite matrix C by an triangular decompo-
C   sition, C = U^T*U, where U is an upper triagonal matrix. In case of a 
C   semi-definite matrix, the diagonal entries of C are increased by small 
C   constants until a factorization is possible.
C
C
C
C   Usage:
C   -----
C
C                CALL CHOLKY(N,NMAX,C,U,ACC,IFAIL,WAR,LWAR)
C
C   Definition of the parameters:
C
C   N :          Number of rows and columns of C.
C   NMAX :       Row dimension of C. NMAX must be at least one and greater
C                or equal to N.
C   C(NMAX,N):   Positive definite matrix which is to be decomposed. Note that 
C                only the upper triangular part of C including diagonal is read.
C   U(NMAX,N):   On return, U contains the upper triangular factor of C in the   
C                upper triangular part including diagonal. The lower triangular
C                part of U without diagonal contains the lower part of matrix C.
C   ACC :        Accuracy for updating diagonal entries of C in case of a 
C                semi-definte matrix (e.g. 1.0D-12).
C   IFAIL :      Termination reason, i.e. 
C                -1 : Length of working array WAR too small.
C                 0 : The decomposition is successfully computed.
C                >1 : The diagonal is updated IFAIL times.
C   WAR(LWAR) :  Real working array of length LWAR. 
C   LWAR :       Length of WAR, must be at least N*(N+3)/2 + 2*N + 1.
C
C
C
C   Author:      K. Schittkowski
C   -------      Department of Computer Science
C                University of Bayreuth
C                D-95440 Bayreuth
C                Germany
C
C
C
C   Version:    1.0 (April, 2005)  - first implementation
C   --------
C            
C
C***********************************************************************
C
C
      SUBROUTINE       CHOLKY(N,NMAX,C,U,ACC,IFAIL,WAR,LWAR)
      IMPLICIT         NONE 
      INTEGER          N,NMAX,IFAIL,LWAR
      DOUBLE PRECISION C(NMAX,N),U(NMAX,N),WAR(LWAR),ACC
      INTEGER          I,ID,IWD,II,J,IR,IRB,IWR,IRA,IWW,IDX,K
      DOUBLE PRECISION DIAG,DIAGR,GA,GB,TEMP,SUMX,SUM,ZERO,ONE,UFL

C
C	INITIALIZATION OF VARIABLES THAT MAY BE UNINITIALIZED OTHERWISE
C
	TEMP = 0.0D0

      IWR   = 1 
      IWD   = IWR + (N*(N+3))/2
      IWW   = IWD + N
      IF (LWAR.LT.IWW+N) THEN
         IFAIL = -1
         RETURN
      ENDIF  
      IFAIL = 0
      DIAGR = 2.0D0
      ZERO  = 0.0D0
      ONE   = 1.0D0
      UFL   = 1.0D-30
      DIAG  = ZERO

C  Determine Cholesky factors of C

      DO I = 1,N
         ID = IWD + I
         WAR(ID) = C(I,I)
         DIAG = DMAX1(DIAG,ACC - WAR(ID))
         IF (I.LT.N) THEN
            II=I + 1
            DO  J=II,N
               GA = -DMIN1(WAR(ID),C(J,J))
               GB = DABS(WAR(ID) - C(J,J)) + DABS(C(I,J))
               IF (GB.GT.ZERO) GA = GA + C(I,J)**2/GB
               DIAG = DMAX1(DIAG,GA)
            ENDDO
         ENDIF
      ENDDO
      IF (DIAG.LE.ZERO) GOTO 90
   70 DIAG = DIAGR*DIAG
      DO I=1,N
         ID = IWD + I
         C(I,I) = DIAG + WAR(ID)
cc         WAR(IWW+I) = DIAG + WAR(ID)
      ENDDO
   90 IR = IWR
      DO J=1,N
         IRA = IWR
         IRB = IR + 1
         DO I=1,J
            TEMP = C(I,J)
cc            IF (I.EQ.J) TEMP = WAR(IWW+I)
            IF (I.GT.1) THEN
               DO K=IRB,IR
                  IRA = IRA + 1
                  TEMP = TEMP - WAR(K)*WAR(IRA)
               ENDDO
            ENDIF 
            IR = IR + 1
            IRA = IRA + 1
            IF (I.LT.J) WAR(IR) = TEMP/WAR(IRA)
         ENDDO
         IF (TEMP.LT.ACC) GOTO 140
         WAR(IR) = DSQRT(TEMP)
      ENDDO
      GOTO 170
  140 WAR(J) = ONE
      IFAIL = IFAIL + 1 
      SUMX = ONE
      K = J
  150 SUM = ZERO
      IRA = IR-1
      DO I=K,J
         SUM = SUM - WAR(IRA)*WAR(I)
         IRA = IRA + I
      ENDDO
      IR = IR - K
      K = K - 1
   
      IF (WAR(IR).LT.UFL) THEN
         WAR(K) = SUM/UFL
      ELSE
         WAR(K) = SUM/WAR(IR)
      ENDIF
      SUMX = SUMX + WAR(K)**2
      IF (K.GE.2) GOTO 150
      DIAG = DIAG + ACC - TEMP/SUMX
      GOTO 70


C Store Cholesky factors 

      IR = IWR
      DO I = 1,N
         DO J = 1,I
            IR = IR + 1
cc            IF (J.LT.I) THEN 
               WAR(IR) = C(J,I)
cc            ELSE
cc               WAR(IR) = WAR(IWW+I)
cc            ENDIF   
         ENDDO
      ENDDO
  170 CONTINUE
      IDX = IWR + 1
      DO I=1,N
         DO J=1,I
C            WAR(NMAX*(I-1)+J) = WAR(IDX)           
            U(J,I) = WAR(IDX)
            IDX = IDX + 1
         ENDDO
         DO J=I+1,N
C            WAR(NMAX*(I-1)+J) = C(J,I)
C            U(J,I) = C(J,I)
            U(J,I) = C(I,J)
         ENDDO
      ENDDO

      RETURN
      END
C
C
C
      SUBROUTINE TRILIN(N,NMAX,U,B,X,W)
C
C
C   Solve system of equations U^T*U*X = B where U is an upper triangular matrix
C
C
      IMPLICIT         NONE
      INTEGER          N,NMAX,I,J
      DOUBLE PRECISION U(NMAX,N),B(N),X(N),W(N),SUM
C
      W(1) = B(1)/U(1,1)
      DO I=2,N
         SUM = 0.0D0
         DO J=1,I-1
            SUM = SUM + U(J,I)*W(J)
         ENDDO
         W(I) = (B(I) - SUM)/U(I,I)
      ENDDO
      X(N) = W(N)/U(N,N)
      DO I=N-1,1,-1
         SUM = 0.0D0
         DO J=I+1,N
            SUM = SUM + U(I,J)*X(J)
         ENDDO
         X(I) = (W(I) - SUM)/U(I,I)
      ENDDO            
C         
      RETURN
      END
