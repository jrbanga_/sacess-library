      SUBROUTINE ZMEXFUN(NLHS,PL,NRHS,PR)
*
*     MAIN GATEWAY (F90 POINTER)
*
      INTEGER          C
      PARAMETER       (C=255)
      CHARACTER * (C)  Q
      INTEGER          NL    , NR   
*
      INTEGER          NLHS  , NRHS 
      INTEGER          PL(*) , PR(*)
*
      INTERFACE
        FUNCTION ZALREAL(N)
          INTEGER,POINTER :: ZALREAL
          INTEGER N
        END FUNCTION ZALREAL
      END INTERFACE
      INTEGER          ZGETSCA
*
*     BUFFER POINTER AND SIZE
*
      INTEGER          BSIZE
      INTEGER,POINTER :: B
      COMMON  /BUFFER/ B
      SAVE    /BUFFER/
*
*     UTILITY FUNCTIONS
*
      INTEGER          ZIDSET
*
      EXTERNAL         ZGETSCA,ZGETSIZ,ZIDSET
*
*     POINTERS TO ARRAYS
*
      INTEGER,POINTER::A6,A8,A9,A10,A11,A12,A13,A14,A25,A27,A29
*
*     INTEGERS USED TO DIMENSION ARRAYS
*
      INTEGER          I1,I2,I3,I26,I28,I30
*
      PARAMETER       (Q = 'misqp' , NL =13 , NR =30)
*
      INTEGER M
      PARAMETER (M=2048)
      CHARACTER*(M) NAMES
      CHARACTER*(30) PROCNM
      COMMON /ARGLIST/ PROCNM,NAMES
      SAVE /ARGLIST/
      EXTERNAL ZTEST
*
      NAMES='m,me,n,nint,nbin,x,f,g,df,dg,u,xl,xu,b,acc,accqp,maxit,
     +        maxpen,maxund,resopt,nonmon,iprint,mode,ifail,rw,lrw,iw,
     +        liw,lw,llw'
      PROCNM='6misqp'
*
      CALL ZTEST(Q,NLHS,NRHS,NL,NR)
*
*     GET VALUES OF ALL INTEGER DIMENSIONS
*
      I1     = ZGETSCA (PR(1))
      I2     = ZGETSCA (PR(2))
      I3     = ZGETSCA (PR(3))
      I26    = ZGETSCA (PR(26))
      I28    = ZGETSCA (PR(28))
      I30    = ZGETSCA (PR(30))
*
*     ALLOCATE SPACE FOR ALL ARRAY ARGUMENTS
*
      A6     =>ZALREAL (I3+1)
      A8     =>ZALREAL (I1+I2+1)
      A9     =>ZALREAL (I3+1)
      A10    =>ZALREAL ((I1+I2+1)*(I3+1))
      A11    =>ZALREAL (I1+I3+I3)
      A12    =>ZALREAL (I3+1)
      A13    =>ZALREAL (I3+1)
      A14    =>ZALREAL ((I3+1)*(I3+1))
      A25    =>ZALREAL (I26)
      A27    =>ZALREAL (I28)
      A29    =>ZALREAL (I30)
*
*     ALLOCATE SPACE FOR BUFFER
*
      BSIZE  = MAX   (1,(I3+1),(I1+I2+1),(I3+1),(I1+I2+1)*(I3+1),
     +                (I1+I3+I3),(I3+1),(I3+1),(I3+1)*(I3+1),(I26),
     +                (I28),(I30))
      B      =>ZALREAL (BSIZE)
*
      CALL FCNMISQP(NLHS,PL,NRHS,PR,A6,A8,A9,A10,A11,A12,A13,
     +                 A14,A25,A27,A29,B)
*
      RETURN
      END
 
      SUBROUTINE FCNMISQP(NLHS,PL,NRHS,PR,A6,A8,A9,A10,A11,A12,A13,
     +                  A14,A25,A27,A29,B)
*
      INTEGER          P, C
      PARAMETER       (P = 0, C = 255)
*
*     NUMBER OF LHS AND RHS ARGUMENTS ( NL AND NR )
*
      INTEGER          NL , NR
      CHARACTER *(C)   Q
*
      PARAMETER       (Q = 'misqp' , NL =13 , NR =30)
*
*     INPUT / OUTPUT INTERFACE POINTERS
*
      INTEGER          NLHS  , NRHS
      INTEGER          PL(*) , PR(*)
*
*     UTILITY PROCEDURES
*
      EXTERNAL ZOUTI,ZOUTC,ZOUTZ,ZARG1,ZIND,ZINS,
     +         ZOUTR,ZOUTL,ZOUTX,ZINI,ZINC,ZINX,
     +         ZOUTD,ZOUTS,ZARG0,ZINR,ZINL,ZINZ,
     +         ZCHECK,MISQP
*
*     REAL AND  IMAGINARY  POINTERS TO LHS ARGUMENTS
*
      INTEGER          RL(NL),IL(NL)
      DATA             RL,IL /NL*0,NL*0/
*     REAL AND  IMAGINARY  POINTERS TO RHS ARGUMENTS
*
      INTEGER          RR(NR),IR(NR)
*
*     LHS DATA TYPE
*
      INTEGER          FLAG(NL)
*
*     DIMENSIONS      (X,Y)  AND D = X*Y
*
      INTEGER          DL(NL),XL(NL),YL(NL)
      INTEGER          DR(NR),XR(NR),YR(NR),X(NR),Y(NR)
*
*     VARIABLES USED IN DATA TYPE CONVERSION
*
      INTEGER         B
*
*
      COMMON  /PQSIM/ A1,A2,A3,A26,A28,A30
*
*     LOCAL FORTRAN VARIABLES
*
      INTEGER               A1     
      INTEGER               A2     
      INTEGER               A3     
      INTEGER               A4     
      INTEGER               A5     
      INTEGER               A17    
      INTEGER               A18    
      INTEGER               A19    
      INTEGER               A21    
      INTEGER               A22    
      INTEGER               A23    
      INTEGER               A24    
      INTEGER               A26    
      INTEGER               A28    
      INTEGER               A30    
      DOUBLE PRECISION      A7     
      DOUBLE PRECISION      A15    
      DOUBLE PRECISION      A16    
      LOGICAL               A20    
      INTEGER               A27    (*)
      DOUBLE PRECISION      A6     (*)
      DOUBLE PRECISION      A8     (*)
      DOUBLE PRECISION      A9     (*)
      DOUBLE PRECISION      A10    (*)
      DOUBLE PRECISION      A11    (*)
      DOUBLE PRECISION      A12    (*)
      DOUBLE PRECISION      A13    (*)
      DOUBLE PRECISION      A14    (*)
      DOUBLE PRECISION      A25    (*)
      LOGICAL               A29    (*)
*
*     DATA TYPE FOR LHS ARGUMENTS ( RE= 0 ,  IM = 1 )
*
      SAVE FLAG
      DATA FLAG       /0,0,0,0,0,0,0,0,0,0,0,0,0/
*
*     GET ARGUMENT DIMENSIONS AND POINTERS (RHS)
*
      CALL ZARG0(Q,P,PR,RR,IR,X,Y,DR,NR)
*
*     COPY MATLAB RHS (DIMENSION) INTEGERS TO FORTRAN
*                 ===
*
      CALL ZOUTI ( Q, P, 1    , A1   , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 2    , A2   , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 3    , A3   , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 26   , A26  , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 28   , A28  , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 30   , A30  , DR, RR, IR, B )
*
*     GET SPECIFIED DIMENSION OF RHS ARGUMENTS
*
      CALL MISQPR(XR,YR,DR,NR)
*
*     CHECK DIMENSION OF RHS ARGUMENTS
*
      CALL ZCHECK(Q,P,X,Y,XR,YR,DR,RR,NR)
*
*     COPY MATLAB RHS (NON-DIMENSION) ARGUMENTS TO FORTRAN
*                 ===
*
      CALL ZOUTI ( Q, P, 4    , A4   , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 5    , A5   , DR, RR, IR, B )
      CALL ZOUTD ( Q, P, 6    , A6   , DR, RR, IR)
      CALL ZOUTD ( Q, P, 7    , A7   , DR, RR, IR)
      CALL ZOUTD ( Q, P, 8    , A8   , DR, RR, IR)
      CALL ZOUTD ( Q, P, 9    , A9   , DR, RR, IR)
      CALL ZOUTD ( Q, P, 10   , A10  , DR, RR, IR)
      CALL ZOUTD ( Q, P, 11   , A11  , DR, RR, IR)
      CALL ZOUTD ( Q, P, 12   , A12  , DR, RR, IR)
      CALL ZOUTD ( Q, P, 13   , A13  , DR, RR, IR)
      CALL ZOUTD ( Q, P, 14   , A14  , DR, RR, IR)
      CALL ZOUTD ( Q, P, 15   , A15  , DR, RR, IR)
      CALL ZOUTD ( Q, P, 16   , A16  , DR, RR, IR)
      CALL ZOUTI ( Q, P, 17   , A17  , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 18   , A18  , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 19   , A19  , DR, RR, IR, B )
      CALL ZOUTL ( Q, P, 20   , A20  , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 21   , A21  , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 22   , A22  , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 23   , A23  , DR, RR, IR, B )
      CALL ZOUTI ( Q, P, 24   , A24  , DR, RR, IR, B )
      CALL ZOUTD ( Q, P, 25   , A25  , DR, RR, IR)
      CALL ZOUTI ( Q, P, 27   , A27  , DR, RR, IR, B )
      CALL ZOUTL ( Q, P, 29   , A29  , DR, RR, IR, B )
*
*     GET DIMENSION OF LHS ARGUMENTS
*
      CALL MISQPL(XL,YL,DL,X,Y,NL)
*
*     ALLOCATE SPACE AND GET POINTERS FOR LHS ARGUMENTS
*
      CALL ZARG1(PL,RL,IL,XL,YL,FLAG,NL)
*
*     CALL EXTERNAL FORTRAN PROCEDURE
*
      CALL   MISQP(A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,
     +                 A15,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,
     +                 A27,A28,A29,A30)
*
*     COPY FORTRAN OUTPUT ARRAYS TO MATLAB ARRAYS
*                  ======
      CALL ZIND ( A6    , 1   , DL, RL)
      CALL ZIND ( A7    , 2   , DL, RL)
      CALL ZIND ( A8    , 3   , DL, RL)
      CALL ZIND ( A9    , 4   , DL, RL)
      CALL ZIND ( A10   , 5   , DL, RL)
      CALL ZIND ( A11   , 6   , DL, RL)
      CALL ZIND ( A12   , 7   , DL, RL)
      CALL ZIND ( A13   , 8   , DL, RL)
      CALL ZIND ( A14   , 9   , DL, RL)
      CALL ZINI ( A24   , 10  , DL, RL, B )
      CALL ZIND ( A25   , 11  , DL, RL)
      CALL ZINI ( A27   , 12  , DL, RL, B )
      CALL ZINL ( A29   , 13  , DL, RL, B )
*
      RETURN
      END
 
      SUBROUTINE MISQPL(X,Y,Z,U,V,W)
*
*     THIS SUBROUTINE RETURNS THE DIMENSIONS OF THE
*     LHS VARIABLES USING THE INTEGER SCALARS WHICH
*     ARE PASSED THROUGH THE COMMON BLOCK
*
      INTEGER          Q,W,X(*),Y(*),Z(*),U(*),V(*)
*
      INTEGER I1,I2,I3,I26,I28,I30
      COMMON  /PQSIM/ I1,I2,I3,I26,I28,I30
*
      INTEGER  ZIDSET
      EXTERNAL ZIDSET
*
*
      X(1)     = I3+1
      Y(1)     = 1
*
      X(2)     = 1
      Y(2)     = 1
*
      X(3)     = I1+I2+1
      Y(3)     = 1
*
      X(4)     = I3+1
      Y(4)     = 1
*
      X(5)     = I1+I2+1
      Y(5)     = I3+1
*
      X(6)     = I1+I3+I3
      Y(6)     = 1
*
      X(7)     = I3+1
      Y(7)     = 1
*
      X(8)     = I3+1
      Y(8)     = 1
*
      X(9)     = I3+1
      Y(9)     = I3+1
*
      X(10)    = 1
      Y(10)    = 1
*
      X(11)    = I26
      Y(11)    = 1
*
      X(12)    = I28
      Y(12)    = 1
*
      X(13)    = I30
      Y(13)    = 1
*
      DO 10 Q = 1 , W
         Z(Q) = X(Q) * Y(Q)
   10 CONTINUE
 
      RETURN
      END
 
      SUBROUTINE MISQPR(X,Y,Z,W)
*
*     THIS SUBROUTINE RETURNS THE DIMENSIONS OF THE
*     RHS VARIABLES USING THE INTEGER SCALARS WHICH
*     ARE PASSED THROUGH THE COMMON BLOCK
*
*
      INTEGER          Q,W,X(*),Y(*),Z(*)
*
      INTEGER I1,I2,I3,I26,I28,I30
      COMMON  /PQSIM/ I1,I2,I3,I26,I28,I30
*
      X(1)     = 1
      Y(1)     = 1
*
      X(2)     = 1
      Y(2)     = 1
*
      X(3)     = 1
      Y(3)     = 1
*
      X(4)     = 1
      Y(4)     = 1
*
      X(5)     = 1
      Y(5)     = 1
*
      X(6)     = I3+1
      Y(6)     = 1
*
      X(7)     = 1
      Y(7)     = 1
*
      X(8)     = I1+I2+1
      Y(8)     = 1
*
      X(9)     = I3+1
      Y(9)     = 1
*
      X(10)    = I1+I2+1
      Y(10)    = I3+1
*
      X(11)    = I1+I3+I3
      Y(11)    = 1
*
      X(12)    = I3+1
      Y(12)    = 1
*
      X(13)    = I3+1
      Y(13)    = 1
*
      X(14)    = I3+1
      Y(14)    = I3+1
*
      X(15)    = 1
      Y(15)    = 1
*
      X(16)    = 1
      Y(16)    = 1
*
      X(17)    = 1
      Y(17)    = 1
*
      X(18)    = 1
      Y(18)    = 1
*
      X(19)    = 1
      Y(19)    = 1
*
      X(20)    = 1
      Y(20)    = 1
*
      X(21)    = 1
      Y(21)    = 1
*
      X(22)    = 1
      Y(22)    = 1
*
      X(23)    = 1
      Y(23)    = 1
*
      X(24)    = 1
      Y(24)    = 1
*
      X(25)    = I26
      Y(25)    = 1
*
      X(26)    = 1
      Y(26)    = 1
*
      X(27)    = I28
      Y(27)    = 1
*
      X(28)    = 1
      Y(28)    = 1
*
      X(29)    = I30
      Y(29)    = 1
*
      X(30)    = 1
      Y(30)    = 1
*
      DO 10 Q = 1 , W
         Z(Q) = X(Q) * Y(Q)
   10 CONTINUE
*
      RETURN
      END
 
